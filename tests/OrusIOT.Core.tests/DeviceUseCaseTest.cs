using Moq;
using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Entities;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.UseCases.DeviceUseCases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace OrusIOT.Core.tests
{
    public class DeviceUseCaseTest
    {
        [Fact]
        public async Task Get_Device_By_Id_Async()
        {
            // Arrange
            var mockDeviceRepository = new Mock<IDeviceRepository>();
            mockDeviceRepository
                .Setup(repo => repo.GetDeviceById(It.IsAny<GetDeviceByIdRequest>()))
                .Returns(Task.FromResult(new GetDeviceByIdResponse("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9",
                                                                   "create device test",
                                                                   "F8082F31661F",
                                                                   true)));

            var useCase = new GetDeviceByIdUseCase(mockDeviceRepository.Object);
            var mockOutput = new Mock<IOutputPort<GetDeviceByIdResponse>>();
            mockOutput.Setup(outputPort => outputPort.Handle(It.IsAny<GetDeviceByIdResponse>()));

            // Act
            var response = await useCase.HandleAsync(new GetDeviceByIdRequest("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9"),
                                                                              mockOutput.Object);

            // Assert
            Assert.True(response);
        }

        [Fact]
        public async Task Get_All_Devices_Async()
        {
            // Arrange
            var mockDeviceRepository = new Mock<IDeviceRepository>();
            var devices = new List<Device>();
            devices.Add(new Device("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9", "create device test", "F8082F31661F"));
            mockDeviceRepository
                .Setup(repo => repo.GetDevices(It.IsAny<GetDeviceRequest>()))
                .Returns(Task.FromResult(new GetDeviceResponse(devices, 10, 0, true)));

            var useCase = new GetAllDevicesUseCase(mockDeviceRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<GetDeviceResponse>>();
            mockOutputPort.Setup(outputPort => outputPort.Handle(It.IsAny<GetDeviceResponse>()));

            // Act
            var response = await useCase.HandleAsync(new GetDeviceRequest("9cab5c26-6e15-4b5a-92cc-9e43ed2f3ab3",
                                                                          10,
                                                                          0,
                                                                          0),
                                                     mockOutputPort.Object);

            // Assert
            Assert.True(response);
        }

        [Fact]
        public async Task Create_Device_Async()
        {
            // Arrange
            var mockDeviceRepository = new Mock<IDeviceRepository>();
            mockDeviceRepository
                .Setup(repo => repo.CreateDevice(It.IsAny<CreateDeviceRequest>()))
                .Returns(Task.FromResult(new CreateDeviceResponse(Guid.NewGuid().ToString(), "create device test", "F8082F31661F", true)));

            var useCase = new CreateDeviceUseCase(mockDeviceRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<CreateDeviceResponse>>();
            mockOutputPort.Setup(outputPort => outputPort.Handle(It.IsAny<CreateDeviceResponse>()));

            // Act
            var response = await useCase.HandleAsync(new CreateDeviceRequest("create device test", "F8082F31661F"), mockOutputPort.Object);

            // Assert
            Assert.True(response);
        }

        [Fact]
        public async Task Update_Device_Async()
        {
            // Arrange
            var mockDeviceRepository = new Mock<IDeviceRepository>();
            mockDeviceRepository
                .Setup(repo => repo.UpdateDevice(It.IsAny<UpdateDeviceRequest>()))
                .Returns(Task.FromResult(new UpdateDeviceResponse("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9",
                                                                  "create device test updated",
                                                                  "F8082F31661F",
                                                                  true)));

            var useCase = new UpdateDeviceUseCase(mockDeviceRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<UpdateDeviceResponse>>();
            mockOutputPort.Setup(outpupPort => outpupPort.Handle(It.IsAny<UpdateDeviceResponse>()));

            // Act
            var response = await useCase.HandleAsync(new UpdateDeviceRequest("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9",
                                                                             "create device test",
                                                                             "F8082F31661F"),
                                                     mockOutputPort.Object);

            // Assert
            Assert.True(response);
        }

        [Fact]
        public async Task Delete_Device_Async()
        {
            // Arrange
            var mockDeviceRepository = new Mock<IDeviceRepository>();
            mockDeviceRepository
                .Setup(repo => repo.DeleteDevice(It.IsAny<string>()))
                .Returns(Task.FromResult(new DeleteDeviceResponse("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9", true)));

            var useCase = new DeleteDeviceUseCase(mockDeviceRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<DeleteDeviceResponse>>();
            mockOutputPort.Setup(outputPort => outputPort.Handle(It.IsAny<DeleteDeviceResponse>()));

            // Act
            var response = await useCase.HandleAsync(new DeleteDeviceRequest("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9"),
                                                     mockOutputPort.Object);

            // Assert
            Assert.True(response);
        }
    }
}
