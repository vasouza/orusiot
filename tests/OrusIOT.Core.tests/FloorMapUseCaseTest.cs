﻿using Moq;
using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Entities;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.UseCases.FloorMapUseCases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace OrusIOT.Core.tests
{
    public class FloorMapUseCaseTest
    {
        [Fact]
        public async Task Get_FloorMap_By_Id_Async()
        {
            // Arrange
            var mockFloorMapRepository = new Mock<IFloorMapRepository>();
            mockFloorMapRepository
                .Setup(repo => repo.GetFloorMapById(It.IsAny<GetFloorMapByIdRequest>()))
                .Returns(Task.FromResult(new GetFloorMapByIdResponse("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9",
                                                                     "create device test",
                                                                     "{}",
                                                                     "0ced32af-eb74-4859-91c2-ae6765270156",
                                                                     "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAQ4CAYAAADo08FDAAAgAElEQVR4Xuy9i5IjyY" +
                                                                     "4lBkYEycx69OveOzM2K+2uSf//STLJpJ3Vzszq3rndXZVJMiIoOweAO8Ijgk4ms6pfSeu2rEzGw59wAAc42DT" +
                                                                     "/6X87y12f8a67RZrF+zfWqmEY+H2z0spxo+9f+94fPm5ENptN+v+8ubPZK7fjPfGT2jWe5Xw+y2bUjvhl9ww+3" +
                                                                     "tW2rXypvrCdm8sDtfa9z984juy3DHmdxHv43dvnbQS+0Ahwz4V9X/77OPSCNSpYh5uNdF0nTdOkPbW2Pn2fdxuVX" +
                                                                     "7gOsorPEt2X+N9/j93zZ1ImSd6/cV/4/sG1sQ3lfrt37zeySc/Hs/EutrlRWVn7tMX29V3u7UrPMLmH0fKxErwHc9M0H" +
                                                                     "Ctci/snfW6W2+DPb+49fiodRJswr1gT+IzDwPZyjWDNrLTPH3vNGF5qQlU+NjpnXbOV0dZ6325Etlt5+PN3svvuGz" +
                                                                     "luNzK2nQydzul5g1XXirS2dsMQe3tLqVz2I/1u5y+eqJ9Of8zmBU/Us+As2Ce97hPbP6PoG8+bUdtoasGmsXXh+0waW" +
                                                                     "5fLeoOPl++fNLb2XJzH2nZt4GjPffF8bbzftZ2y9v1yP8r2LI0/eqDzqVfHtWLbLa3b8u3e7+b8hTdQbX+tfF",
                                                                     "0716ff03-1699-4664-b0fa-c80623a5b368",
                                                                     true)));

            var useCase = new GetFloorMapByIdUseCase(mockFloorMapRepository.Object);
            var mockOutput = new Mock<IOutputPort<GetFloorMapByIdResponse>>();
            mockOutput.Setup(outputPort => outputPort.Handle(It.IsAny<GetFloorMapByIdResponse>()));

            // Act
            var response = await useCase.HandleAsync(new GetFloorMapByIdRequest("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9"),
                                                                                mockOutput.Object);

            // Assert
            Assert.True(response);
        }

        [Fact]
        public async Task Get_List_FloorMaps_Async()
        {
            // Arrange
            var mockFloorMapRepository = new Mock<IFloorMapRepository>();
            var floorMaps = new List<FloorMap>();
            floorMaps.Add(new FloorMap("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9",
                                       "create floor map test",
                                       "{}",
                                       "0ced32af-eb74-4859-91c2-ae6765270156",
                                       "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAQ4CAYAAADo08FDAAAgAElEQVR4Xuy9i5IjyY" +
                                       "4lBkYEycx69OveOzM2K+2uSf//STLJpJ3Vzszq3rndXZVJMiIoOweAO8Ijgk4ms6pfSeu2rEzGw59wAAc42DT" +
                                       "/6X87y12f8a67RZrF+zfWqmEY+H2z0spxo+9f+94fPm5ENptN+v+8ubPZK7fjPfGT2jWe5Xw+y2bUjvhl9ww+3" +
                                       "tW2rXypvrCdm8sDtfa9z984juy3DHmdxHv43dvnbQS+0Ahwz4V9X/77OPSCNSpYh5uNdF0nTdOkPbW2Pn2fdxuVX" +
                                       "7gOsorPEt2X+N9/j93zZ1ImSd6/cV/4/sG1sQ3lfrt37zeySc/Hs/EutrlRWVn7tMX29V3u7UrPMLmH0fKxErwHc9M0H" +
                                       "Ctci/snfW6W2+DPb+49fiodRJswr1gT+IzDwPZyjWDNrLTPH3vNGF5qQlU+NjpnXbOV0dZ6325Etlt5+PN3svvuGz" +
                                       "luNzK2nQydzul5g1XXirS2dsMQe3tLqVz2I/1u5y+eqJ9Of8zmBU/Us+As2Ce97hPbP6PoG8+bUdtoasGmsXXh+0waW" +
                                       "5fLeoOPl++fNLb2XJzH2nZt4GjPffF8bbzftZ2y9v1yP8r2LI0/eqDzqVfHtWLbLa3b8u3e7+b8hTdQbX+tfF",
                                       "0716ff03-1699-4664-b0fa-c80623a5b368"));
            mockFloorMapRepository
                .Setup(repo => repo.GetListFloorMaps(It.IsAny<GetListFloorMapRequest>()))
                .Returns(Task.FromResult(new GetListFloorMapResponse(floorMaps, 10, 0, true)));

            var useCase = new GetListFloorMapUseCase(mockFloorMapRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<GetListFloorMapResponse>>();
            mockOutputPort.Setup(outputPort => outputPort.Handle(It.IsAny<GetListFloorMapResponse>()));

            // Act
            var response = await useCase.HandleAsync(new GetListFloorMapRequest("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9", 10, 0, 0), mockOutputPort.Object);

            // Assert
            Assert.True(response);
        }

        [Fact]
        public async Task Create_Device_Async()
        {
            // Arrange
            var mockFloorMapRepository = new Mock<IFloorMapRepository>();
            mockFloorMapRepository
                .Setup(repo => repo.CreateFloorMap(It.IsAny<CreateFloorMapRequest>()))
                .Returns(Task.FromResult(new CreateFloorMapResponse(Guid.NewGuid().ToString(),
                                                                    "create floor map test",
                                                                    "{}",
                                                                    "0ced32af-eb74-4859-91c2-ae6765270156",
                                                                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAQ4CAYAAADo08FDAAAgAElEQVR4Xuy9i5IjyY" +
                                                                   "4lBkYEycx69OveOzM2K+2uSf//STLJpJ3Vzszq3rndXZVJMiIoOweAO8Ijgk4ms6pfSeu2rEzGw59wAAc42DT" +
                                                                   "/6X87y12f8a67RZrF+zfWqmEY+H2z0spxo+9f+94fPm5ENptN+v+8ubPZK7fjPfGT2jWe5Xw+y2bUjvhl9ww+3" +
                                                                   "tW2rXypvrCdm8sDtfa9z984juy3DHmdxHv43dvnbQS+0Ahwz4V9X/77OPSCNSpYh5uNdF0nTdOkPbW2Pn2fdxuVX" +
                                                                   "7gOsorPEt2X+N9/j93zZ1ImSd6/cV/4/sG1sQ3lfrt37zeySc/Hs/EutrlRWVn7tMX29V3u7UrPMLmH0fKxErwHc9M0H" +
                                                                   "Ctci/snfW6W2+DPb+49fiodRJswr1gT+IzDwPZyjWDNrLTPH3vNGF5qQlU+NjpnXbOV0dZ6325Etlt5+PN3svvuGz" +
                                                                   "luNzK2nQydzul5g1XXirS2dsMQe3tLqVz2I/1u5y+eqJ9Of8zmBU/Us+As2Ce97hPbP6PoG8+bUdtoasGmsXXh+0waW" +
                                                                   "5fLeoOPl++fNLb2XJzH2nZt4GjPffF8bbzftZ2y9v1yP8r2LI0/eqDzqVfHtWLbLa3b8u3e7+b8hTdQbX+tfF",
                                                                   "0716ff03-1699-4664-b0fa-c80623a5b368",
                                                                                                true)));

            var useCase = new CreateFloorMapUseCase(mockFloorMapRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<CreateFloorMapResponse>>();
            mockOutputPort.Setup(outputPort => outputPort.Handle(It.IsAny<CreateFloorMapResponse>()));

            // Act
            var response = await useCase.HandleAsync(new CreateFloorMapRequest("create floor map test",
                                                                               "{}",
                                                                               "0ced32af-eb74-4859-91c2-ae6765270156",
                                                                               "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAQ4CAYAAADo08FDAAAgAElEQVR4Xuy9i5IjyY" +
                                                                               "4lBkYEycx69OveOzM2K+2uSf//STLJpJ3Vzszq3rndXZVJMiIoOweAO8Ijgk4ms6pfSeu2rEzGw59wAAc42DT" +
                                                                               "/6X87y12f8a67RZrF+zfWqmEY+H2z0spxo+9f+94fPm5ENptN+v+8ubPZK7fjPfGT2jWe5Xw+y2bUjvhl9ww+3" +
                                                                               "tW2rXypvrCdm8sDtfa9z984juy3DHmdxHv43dvnbQS+0Ahwz4V9X/77OPSCNSpYh5uNdF0nTdOkPbW2Pn2fdxuVX" +
                                                                               "7gOsorPEt2X+N9/j93zZ1ImSd6/cV/4/sG1sQ3lfrt37zeySc/Hs/EutrlRWVn7tMX29V3u7UrPMLmH0fKxErwHc9M0H" +
                                                                               "Ctci/snfW6W2+DPb+49fiodRJswr1gT+IzDwPZyjWDNrLTPH3vNGF5qQlU+NjpnXbOV0dZ6325Etlt5+PN3svvuGz" +
                                                                               "luNzK2nQydzul5g1XXirS2dsMQe3tLqVz2I/1u5y+eqJ9Of8zmBU/Us+As2Ce97hPbP6PoG8+bUdtoasGmsXXh+0waW" +
                                                                               "5fLeoOPl++fNLb2XJzH2nZt4GjPffF8bbzftZ2y9v1yP8r2LI0/eqDzqVfHtWLbLa3b8u3e7+b8hTdQbX+tfF",
                                                                               "0716ff03-1699-4664-b0fa-c80623a5b368"),
                                                                                             mockOutputPort.Object);

            // Assert
            Assert.True(response);
        }

        [Fact]
        public async Task Update_Device_Async()
        {
            // Arrange
            var mockFloorMapRepository = new Mock<IFloorMapRepository>();
            mockFloorMapRepository
                .Setup(repo => repo.UpdateFloorMap(It.IsAny<UpdateFloorMapRequest>()))
                .Returns(Task.FromResult(new UpdateFloorMapResponse("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9",
                                                                    "create device test updated",
                                                                    "{}",
                                                                    "0ced32af-eb74-4859-91c2-ae6765270156",
                                                                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAQ4CAYAAADo08FDAAAgAElEQVR4Xuy9i5IjyY" +
                                                                   "4lBkYEycx69OveOzM2K+2uSf//STLJpJ3Vzszq3rndXZVJMiIoOweAO8Ijgk4ms6pfSeu2rEzGw59wAAc42DT" +
                                                                   "/6X87y12f8a67RZrF+zfWqmEY+H2z0spxo+9f+94fPm5ENptN+v+8ubPZK7fjPfGT2jWe5Xw+y2bUjvhl9ww+3" +
                                                                   "tW2rXypvrCdm8sDtfa9z984juy3DHmdxHv43dvnbQS+0Ahwz4V9X/77OPSCNSpYh5uNdF0nTdOkPbW2Pn2fdxuVX" +
                                                                   "7gOsorPEt2X+N9/j93zZ1ImSd6/cV/4/sG1sQ3lfrt37zeySc/Hs/EutrlRWVn7tMX29V3u7UrPMLmH0fKxErwHc9M0H" +
                                                                   "Ctci/snfW6W2+DPb+49fiodRJswr1gT+IzDwPZyjWDNrLTPH3vNGF5qQlU+NjpnXbOV0dZ6325Etlt5+PN3svvuGz" +
                                                                   "luNzK2nQydzul5g1XXirS2dsMQe3tLqVz2I/1u5y+eqJ9Of8zmBU/Us+As2Ce97hPbP6PoG8+bUdtoasGmsXXh+0waW" +
                                                                   "5fLeoOPl++fNLb2XJzH2nZt4GjPffF8bbzftZ2y9v1yP8r2LI0/eqDzqVfHtWLbLa3b8u3e7+b8hTdQbX+tfF",
                                                                   "0716ff03-1699-4664-b0fa-c80623a5b368",
                                                                                                true)));

            var useCase = new UpdateFloorMapUseCase(mockFloorMapRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<UpdateFloorMapResponse>>();
            mockOutputPort.Setup(outpupPort => outpupPort.Handle(It.IsAny<UpdateFloorMapResponse>()));

            // Act
            var response = await useCase.HandleAsync(new UpdateFloorMapRequest("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9",
                                                                               "update floor map test",
                                                                               "{}",
                                                                               "0ced32af-eb74-4859-91c2-ae6765270156",
                                                                               "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAQ4CAYAAADo08FDAAAgAElEQVR4Xuy9i5IjyY" +
                                                                               "4lBkYEycx69OveOzM2K+2uSf//STLJpJ3Vzszq3rndXZVJMiIoOweAO8Ijgk4ms6pfSeu2rEzGw59wAAc42DT" +
                                                                               "/6X87y12f8a67RZrF+zfWqmEY+H2z0spxo+9f+94fPm5ENptN+v+8ubPZK7fjPfGT2jWe5Xw+y2bUjvhl9ww+3" +
                                                                               "tW2rXypvrCdm8sDtfa9z984juy3DHmdxHv43dvnbQS+0Ahwz4V9X/77OPSCNSpYh5uNdF0nTdOkPbW2Pn2fdxuVX" +
                                                                               "7gOsorPEt2X+N9/j93zZ1ImSd6/cV/4/sG1sQ3lfrt37zeySc/Hs/EutrlRWVn7tMX29V3u7UrPMLmH0fKxErwHc9M0H" +
                                                                               "Ctci/snfW6W2+DPb+49fiodRJswr1gT+IzDwPZyjWDNrLTPH3vNGF5qQlU+NjpnXbOV0dZ6325Etlt5+PN3svvuGz" +
                                                                               "luNzK2nQydzul5g1XXirS2dsMQe3tLqVz2I/1u5y+eqJ9Of8zmBU/Us+As2Ce97hPbP6PoG8+bUdtoasGmsXXh+0waW" +
                                                                               "5fLeoOPl++fNLb2XJzH2nZt4GjPffF8bbzftZ2y9v1yP8r2LI0/eqDzqVfHtWLbLa3b8u3e7+b8hTdQbX+tfF",
                                                                               "0716ff03-1699-4664-b0fa-c80623a5b368"),
                                                     mockOutputPort.Object);

            // Assert
            Assert.True(response);
        }

        [Fact]
        public async Task Delete_Device_Async()
        {
            // Arrange
            var mockFloorMapRepository = new Mock<IFloorMapRepository>();
            mockFloorMapRepository
                .Setup(repo => repo.DeleteFloorMap(It.IsAny<string>()))
                .Returns(Task.FromResult(new DeleteFloorMapResponse("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9", true)));

            var useCase = new DeleteFloorMapUseCase(mockFloorMapRepository.Object);
            var mockOutputPort = new Mock<IOutputPort<DeleteFloorMapResponse>>();
            mockOutputPort.Setup(outputPort => outputPort.Handle(It.IsAny<DeleteFloorMapResponse>()));

            // Act
            var response = await useCase.HandleAsync(new DeleteFloorMapRequest("c72267c4-4a80-40ae-8bd7-e6cce9f2b5c9"),
                                                     mockOutputPort.Object);

            // Assert
            Assert.True(response);
        }
    }
}
