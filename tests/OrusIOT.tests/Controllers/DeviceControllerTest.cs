﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using OrusIOT.Controllers;
using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.UseCases.DeviceUseCases;
using OrusIOT.Presenter.Device;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace OrusIOT.tests.Controllers
{
    public class DeviceControllerTest
    {
        [Fact]
        public async Task Post_Returns_OK_When_Device_Use_Case_Succeeds_Async()
        {
            // Arrange
            var mockDeviceRepository = new Mock<IDeviceRepository>();
            mockDeviceRepository
                .Setup(repo => repo.CreateDevice(It.IsAny<CreateDeviceRequest>()))
                .Returns(Task.FromResult(new CreateDeviceResponse("fca526ac-d561-4d4a-89ea-5634a9b0d144",
                                                                  "device test",
                                                                  "0a:39:51:5b:94:93",
                                                                  true,
                                                                  string.Empty)));

            // Fake
            var outputPort = new CreateDevicePresenter();
            var createDeviceUseCase = new CreateDeviceUseCase(mockDeviceRepository.Object);
            var controller = new DeviceController(null, null, null, null, createDeviceUseCase, outputPort, null, null, null, null);

            // Act
            var result = await controller.CreateDeviceAsync(new Model.CreateDeviceRequest()
            {
                Name = "device test",
                MacAddress = "0a:39:51:5b:94:93"
            });

            // Assert
            var statusCode = ((ContentResult)result).StatusCode;
            Assert.True(statusCode.HasValue && statusCode.Value == (int)HttpStatusCode.OK);
        }
    }
}
