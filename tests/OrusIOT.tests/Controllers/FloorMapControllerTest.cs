﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using OrusIOT.Controllers;
using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.UseCases.FloorMapUseCases;
using OrusIOT.Presenter.FloorMap;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace OrusIOT.tests.Controllers
{
    public class FloorMapControllerTest
    {
        [Fact]
        public async Task Post_Returns_OK_When_Floor_Map_Use_Case_Succeeds_Async()
        {
            // Arrange
            var mockFloorMapRepository = new Mock<IFloorMapRepository>();
            mockFloorMapRepository
                .Setup(repo => repo.CreateFloorMap(It.IsAny<CreateFloorMapRequest>()))
                .Returns(Task.FromResult(new CreateFloorMapResponse("706c4e5b-aef1-4b92-a58d-5064d2e9d67b",
                                                                    "floor map test",
                                                                    "{}",
                                                                    "0716ff03-1699-4664-b0fa-c80623a5b368",
                                                                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAQ4CAYAAADo08FDAAAgAElEQVR4Xuy9i5IjyY" +
                                                                    "4lBkYEycx69OveOzM2K+2uSf//STLJpJ3Vzszq3rndXZVJMiIoOweAO8Ijgk4ms6pfSeu2rEzGw59wAAc42DT" +
                                                                    "/6X87y12f8a67RZrF+zfWqmEY+H2z0spxo+9f+94fPm5ENptN+v+8ubPZK7fjPfGT2jWe5Xw+y2bUjvhl9ww+3" +
                                                                    "tW2rXypvrCdm8sDtfa9z984juy3DHmdxHv43dvnbQS+0Ahwz4V9X/77OPSCNSpYh5uNdF0nTdOkPbW2Pn2fdxuVX" +
                                                                    "7gOsorPEt2X+N9/j93zZ1ImSd6/cV/4/sG1sQ3lfrt37zeySc/Hs/EutrlRWVn7tMX29V3u7UrPMLmH0fKxErwHc9M0H" +
                                                                    "Ctci/snfW6W2+DPb+49fiodRJswr1gT+IzDwPZyjWDNrLTPH3vNGF5qQlU+NjpnXbOV0dZ6325Etlt5+PN3svvuGz" +
                                                                    "luNzK2nQydzul5g1XXirS2dsMQe3tLqVz2I/1u5y+eqJ9Of8zmBU/Us+As2Ce97hPbP6PoG8+bUdtoasGmsXXh+0waW" +
                                                                    "5fLeoOPl++fNLb2XJzH2nZt4GjPffF8bbzftZ2y9v1yP8r2LI0/eqDzqVfHtWLbLa3b8u3e7+b8hTdQbX+tfF",
                                                                    "0716ff03-1699-4664-b0fa-c80623a5b368",
                                                                    true,
                                                                    string.Empty)));

            // Fake
            var outputPort = new CreateFloorMapPresenter();
            var useCase = new CreateFloorMapUseCase(mockFloorMapRepository.Object);
            var controller = new FloorMapController(null, null, null, null, useCase, outputPort, null, null, null, null);

            // Act
            var result = await controller.CreateFloorMapAsync(new Model.CreateFloorMapModelRequest()
            {
                Name = "floor map test",
                FloorMapZones = "{}",
                AreaId = "0716ff03-1699-4664-b0fa-c80623a5b368"
            });

            // Assert
            var statusCode = ((ContentResult)result).StatusCode;
            Assert.True(statusCode.HasValue && statusCode.Value == (int)HttpStatusCode.OK);
        }
    }
}
