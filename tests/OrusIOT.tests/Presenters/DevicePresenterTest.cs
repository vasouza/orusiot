﻿using Newtonsoft.Json;
using OrusIOT.Presenter.Device;
using System.Net;
using Xunit;

namespace OrusIOT.tests.Presenters
{
    public class DevicePresenterTest
    {
        [Fact]
        public void Returns_Ok_Status_Code_When_Create_Use_Case_Succeeds()
        {
            // Arrange
            var presenter = new CreateDevicePresenter();

            // Act
            presenter.Handle(new Core.Dto.Device.CreateDeviceResponse("fca526ac-d561-4d4a-89ea-5634a9b0d144",
                                                                      "device test",
                                                                      "0a:39:51:5b:94:93",
                                                                      true,
                                                                      string.Empty));

            // Assert
            Assert.Equal((int)HttpStatusCode.OK, presenter.ContentResult.StatusCode);
        }

        [Fact]
        public void Returns_Valid_Device_When_Create_Use_Case_Succeeds()
        {
            // Arrange
            var presenter = new CreateDevicePresenter();

            // Act
            presenter.Handle(new Core.Dto.Device.CreateDeviceResponse("fca526ac-d561-4d4a-89ea-5634a9b0d144",
                                                                      "device test",
                                                                      "0a:39:51:5b:94:93",
                                                                      true,
                                                                      string.Empty));

            // Assert
            dynamic data = JsonConvert.DeserializeObject(presenter.ContentResult.Content);
            Assert.True(data.Success.Value);
            Assert.Equal("fca526ac-d561-4d4a-89ea-5634a9b0d144", data.DeviceId.Value);
            Assert.Equal("device test", data.Name.Value);
            Assert.Equal("0a:39:51:5b:94:93", data.MacAddress.Value);
        }
    }
}
