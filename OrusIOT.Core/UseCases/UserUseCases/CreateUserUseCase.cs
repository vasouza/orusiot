﻿using OrusIOT.Core.Dto.UserDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.User;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.UserUseCases
{
    public class CreateUserUseCase : ICreateUserUseCase
    {
        private readonly IUserRepository _UserRepository;

        public CreateUserUseCase(IUserRepository UserRepository)
        {
            _UserRepository = UserRepository;
        }

        public async Task<bool> HandleAsync(CreateUserRequest message, IOutputPort<CreateUserResponse> outputPort)
        {
            var response = await _UserRepository.CreateUser(message);

            outputPort.Handle(response.Success ? new CreateUserResponse(response.Login, response.Address, response.Name, response.Email, response.Phone,
                                                                        response.IsActive, response.DepartmentId, response.CompanyId, true) :
                                                 new CreateUserResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
