﻿using OrusIOT.Core.Dto.UserDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases;
using OrusIOT.Core.Interfaces.UseCases.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.UserUseCases
{
    public class GetUserByIdUseCase : IGetUserByIdUseCase
    {
        private readonly IUserRepository _orusIotRepository;

        public GetUserByIdUseCase(IUserRepository orusIotRepository)
        {
            _orusIotRepository = orusIotRepository;
        }

        public async Task<bool> HandleAsync(GetUserByIdRequest message, IOutputPort<GetUserByIdResponse> outputPort)
        {
            var response = await _orusIotRepository.GetUserById(message);

            outputPort.Handle(response.Success ? new GetUserByIdResponse(response.User, true, response.Message) :
                                                 new GetUserByIdResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
