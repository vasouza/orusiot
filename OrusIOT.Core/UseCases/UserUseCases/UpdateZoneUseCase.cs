﻿using OrusIOT.Core.Dto.UserDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.UserUseCases
{
    public class UpdateUserUseCase : IUpdateUserUseCase
    {
        private readonly IUserRepository _UserRepository;

        public UpdateUserUseCase(IUserRepository UserRepository)
        {
            _UserRepository = UserRepository;
        }

        public async Task<bool> HandleAsync(UpdateUserRequest message, IOutputPort<UpdateUserResponse> outputPort)
        {
            var response = await _UserRepository.UpdateUser(message);

            outputPort.Handle(response.Success ? new UpdateUserResponse(response.User, true, response.Message)  :
                                                 new UpdateUserResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
