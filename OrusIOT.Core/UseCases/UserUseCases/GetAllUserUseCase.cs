﻿using OrusIOT.Core.Dto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases;
using OrusIOT.Core.Interfaces.UseCases.User;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.UserUseCases
{
    public class GetAllUserUseCase : IGetAllUserUseCase
    {
        private readonly IUserRepository _orusIotRepository;

        public GetAllUserUseCase(IUserRepository orusIotRepository)
        {
            _orusIotRepository = orusIotRepository;
        }

        public async Task<bool> HandleAsync(GetListUserRequest message, IOutputPort<GetListUserResponse> outputPort)
        {
            var response = await _orusIotRepository.GetUsers(message);

            outputPort.Handle(response.Success ? new GetListUserResponse(response.Users, response.Quantity, response.Page, true, response.Message) :
                                                 new GetListUserResponse(response.Errors));
            return response.Success;
        }
    }
}
