﻿using OrusIOT.Core.Dto.UserDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.UserUseCases
{
    public class DeleteUserUseCase : IDeleteUserUseCase
    {
        private readonly IUserRepository _UserRepository;

        public DeleteUserUseCase(IUserRepository UserRepository)
        {
            _UserRepository = UserRepository;
        }

        public async Task<bool> HandleAsync(DeleteUserRequest message, IOutputPort<DeleteUserResponse> outputPort)
        {
            var response = await _UserRepository.DeleteUserById(message);

            outputPort.Handle(response.Success ? new DeleteUserResponse(response.UserId, true, response.Message) :
                                                 new DeleteUserResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
