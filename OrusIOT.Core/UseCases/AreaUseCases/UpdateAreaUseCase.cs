﻿using OrusIOT.Core.Dto.AreaDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Area;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.AreaUseCases
{
    public class UpdateAreaUseCase : IUpdateAreaUseCase
    {
        private readonly IAreaRepository _areaRepository;

        public UpdateAreaUseCase(IAreaRepository areaRepository)
        {
            _areaRepository = areaRepository;
        }

        public async Task<bool> HandleAsync(UpdateAreaRequest message, IOutputPort<UpdateAreaResponse> outputPort)
        {
            var response = await _areaRepository.UpdateArea(message);

            outputPort.Handle(response.Success ? new UpdateAreaResponse(response.AreaId, response.Name, response.Address, response.Floor,
                                                                        response.City, response.Country, response.ZipCode, response.LocaleId,
                                                                        response.CompanyId, response.IsActive, true) :
                                                 new UpdateAreaResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
