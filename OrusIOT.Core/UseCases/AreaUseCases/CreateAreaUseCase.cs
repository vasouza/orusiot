﻿using OrusIOT.Core.Dto.AreaDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Area;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.AreaUseCases
{
    public class CreateAreaUseCase : ICreateAreaUseCase
    {
        private readonly IAreaRepository _areaRepository;

        public CreateAreaUseCase(IAreaRepository areaRepository)
        {
            _areaRepository = areaRepository;
        }

        public async Task<bool> HandleAsync(CreateAreaRequest message, IOutputPort<CreateAreaResponse> outputPort)
        {
            var response = await _areaRepository.CreateArea(message);

            outputPort.Handle(response.Success ? new CreateAreaResponse(response.AreaId, response.Name, response.Address, response.Floor,
                                                                        response.City, response.Country, response.ZipCode, response.LocaleId,
                                                                        response.CompanyId, response.IsActive, true) :
                                                 new CreateAreaResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
