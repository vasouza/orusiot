﻿using OrusIOT.Core.Dto.AreaDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Area;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.AreaUseCases
{
    public class DeleteAreaUseCase : IDeleteAreaUseCase
    {
        private readonly IAreaRepository _areaRepository;

        public DeleteAreaUseCase(IAreaRepository areaRepository)
        {
            _areaRepository = areaRepository;
        }

        public async Task<bool> HandleAsync(DeleteAreaRequest message, IOutputPort<DeleteAreaResponse> outputPort)
        {
            var response = await _areaRepository.DeleteAreaById(message);

            outputPort.Handle(response.Success ? new DeleteAreaResponse(response.AreaId, true) :
                                                 new DeleteAreaResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
