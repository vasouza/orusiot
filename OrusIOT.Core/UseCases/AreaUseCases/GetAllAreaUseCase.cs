﻿using OrusIOT.Core.Dto.AreaDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Area;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.AreaUseCases
{
    public class GetAllAreaUseCase : IGetAllAreaUseCase
    {
        private readonly IAreaRepository _areaRepository;

        public GetAllAreaUseCase(IAreaRepository areaRepository)
        {
            _areaRepository = areaRepository;
        }
        public async Task<bool> HandleAsync(GetListAreaRequest message, IOutputPort<GetListAreaResponse> outputPort)
        {
            var response = await _areaRepository.GetListAreas(message);

            outputPort.Handle(response.Success ? new GetListAreaResponse(response.Areas, response.Quantity, response.Page, true) :
                                                 new GetListAreaResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
