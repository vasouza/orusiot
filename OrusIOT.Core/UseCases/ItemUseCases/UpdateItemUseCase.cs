﻿using OrusIOT.Core.Dto.ItemDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Item;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ItemUseCases
{
    public class UpdateItemUseCase : IUpdateItemUseCase
    {
        private readonly IItemRepository _ItemRepository;

        public UpdateItemUseCase(IItemRepository ItemRepository)
        {
            _ItemRepository = ItemRepository;
        }

        public async Task<bool> HandleAsync(UpdateItemRequest message, IOutputPort<UpdateItemResponse> outputPort)
        {
            var response = await _ItemRepository.UpdateItem(message);

            outputPort.Handle(response.Success ? new UpdateItemResponse(response.ItemId, response.Name, response.Code, response.SerialNumber,
                                                                        response.IsActive, response.CustodyId, response.LocaleId, response.CompanyId, true, response.Message) :
                                                 new UpdateItemResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
