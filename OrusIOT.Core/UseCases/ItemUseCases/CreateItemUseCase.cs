﻿using OrusIOT.Core.Dto.ItemDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Item;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ItemUseCases
{
    public class CreateItemUseCase : ICreateItemUseCase
    {
        private readonly IItemRepository _ItemRepository;

        public CreateItemUseCase(IItemRepository ItemRepository)
        {
            _ItemRepository = ItemRepository;
        }

        public async Task<bool> HandleAsync(CreateItemRequest message, IOutputPort<CreateItemResponse> outputPort)
        {
            var response = await _ItemRepository.CreateItem(message);

            outputPort.Handle(response.Success ? new CreateItemResponse(response.ItemId, response.Name, response.Code, response.SerialNumber,
                                                                        response.IsActive, response.CustodyId, response.LocaleId, response.CompanyId, true) :
                                                 new CreateItemResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
