﻿using OrusIOT.Core.Dto.ItemDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Item;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ItemUseCases
{
    public class GetItemByIdUseCase : IGetItemByIdUseCase
    {
        private readonly IItemRepository _ItemRepository;

        public GetItemByIdUseCase(IItemRepository ItemRepository)
        {
            _ItemRepository = ItemRepository;
        }

        public async Task<bool> HandleAsync(GetItemByIdRequest message, IOutputPort<GetItemByIdResponse> outputPort)
        {
            var response = await _ItemRepository.GetItemById(message);

            outputPort.Handle(response.Success ? new GetItemByIdResponse(response.ItemId, response.Name, response.Code, response.SerialNumber,
                                                                        response.IsActive, response.CustodyId, response.LocaleId, response.CompanyId, true, response.Message) :
                                                 new GetItemByIdResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
