﻿using OrusIOT.Core.Dto.ItemDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Item;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ItemUseCases
{
    public class DeleteItemUseCase : IDeleteItemUseCase
    {
        private readonly IItemRepository _ItemRepository;

        public DeleteItemUseCase(IItemRepository ItemRepository)
        {
            _ItemRepository = ItemRepository;
        }

        public async Task<bool> HandleAsync(DeleteItemRequest message, IOutputPort<DeleteItemResponse> outputPort)
        {
            var response = await _ItemRepository.DeleteItemById(message);

            outputPort.Handle(response.Success ? new DeleteItemResponse(response.ItemId, true, response.Message) :
                                                 new DeleteItemResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
