﻿using OrusIOT.Core.Dto.ItemDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Item;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ItemUseCases
{
    public class GetAllItemUseCase : IGetAllItemUseCase
    {
        private readonly IItemRepository _ItemRepository;

        public GetAllItemUseCase(IItemRepository ItemRepository)
        {
            _ItemRepository = ItemRepository;
        }

        public async Task<bool> HandleAsync(GetListItemRequest message, IOutputPort<GetListItemResponse> outputPort)
        {
            var response = await _ItemRepository.GetListItems(message);

            outputPort.Handle(response.Success ? new GetListItemResponse(response.Items, response.Quantity, response.Page, true, response.Message) :
                                                 new GetListItemResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
