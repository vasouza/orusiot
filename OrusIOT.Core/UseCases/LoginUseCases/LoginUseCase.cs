﻿using Microsoft.IdentityModel.Tokens;
using OrusIOT.Core.Dto.LoginDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Login;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.LoginUseCases
{
    public class LoginUseCase : ILoginUseCase
    {
        private readonly ILoginRepository _loginRepository;

        public LoginUseCase(ILoginRepository loginRepository)
        {
            _loginRepository = loginRepository;
        }

        public async Task<bool> HandleAsync(LoginRequest message, IOutputPort<LoginResponse> outputPort)
        {
            var response = await _loginRepository.FindUser(message);

            if (response.Success && response.Authenticated)
            {
                ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(message.Login, "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, message.Login)
                    }
                );

                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao +
                    TimeSpan.FromSeconds(message.TokenConfigurations.Seconds);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = message.TokenConfigurations.Issuer,
                    Audience = message.TokenConfigurations.Audience,
                    SigningCredentials = message.SigningConfigurations.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });

                var token = handler.WriteToken(securityToken);

                outputPort.Handle(response.Success ? new LoginResponse(response.Authenticated,
                                                                       dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                                                                       dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                                                                       token,
                                                                       true,
                                                                       response.Message) :
                                                     new LoginResponse(response.Errors, false, response.Message));
            }
            else
            {
                outputPort.Handle(response.Success ? new LoginResponse(false,
                                                                       null,
                                                                       null,
                                                                       null,
                                                                       true,
                                                                       response.Message) :
                                                     new LoginResponse(response.Errors, false, response.Message));
            }
            
            return response.Success;
        }
    }
}
