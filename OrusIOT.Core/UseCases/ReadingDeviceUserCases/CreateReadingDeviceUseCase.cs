﻿using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.ReadingDevice;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ReadingDeviceUseCases
{
    public class CreateReadingDeviceUseCase : ICreateReadingDeviceUseCase
    {
        private readonly IReadingDeviceRepository _readingDeviceRepository;

        public CreateReadingDeviceUseCase(IReadingDeviceRepository readingDeviceRepository)
        {
            _readingDeviceRepository = readingDeviceRepository;
        }

        public async Task<bool> HandleAsync(CreateReadingDeviceRequest message, IOutputPort<CreateReadingDeviceResponse> outputPort)
        {
            var response = await _readingDeviceRepository.CreateReadingDevice(message);

            outputPort.Handle(response.Success ? new CreateReadingDeviceResponse(response.ReadingDeviceId,
                                                                                 response.Key,
                                                                                 response.Name,
                                                                                 response.Manufacturer,
                                                                                 response.Model,
                                                                                 response.SerialNumber,
                                                                                 response.IsActive,
                                                                                 response.AssociatedUserId,
                                                                                 response.SharedUserId,
                                                                                 response.MacAddress,
                                                                                 response.Latitude,
                                                                                 response.Longitude,
                                                                                 response.CompanyId,
                                                                                 true) :
                                                 new CreateReadingDeviceResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
