﻿using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.ReadingDevice;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ReadingDeviceUseCases
{
    public class GetAllReadingDeviceUseCase : IGetAllReadingDeviceUseCase
    {
        private readonly IReadingDeviceRepository _readingDeviceRepository;

        public GetAllReadingDeviceUseCase(IReadingDeviceRepository readingDeviceRepository)
        {
            _readingDeviceRepository = readingDeviceRepository;
        }

        public async Task<bool> HandleAsync(GetListReadingDeviceRequest message, IOutputPort<GetListReadingDeviceResponse> outputPort)
        {
            var response = await _readingDeviceRepository.GetListReadingDevices(message);

            outputPort.Handle(response.Success ? new GetListReadingDeviceResponse(response.ReadingDevices, response.Quantity, response.Page, true, response.Message) :
                                                 new GetListReadingDeviceResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
