﻿using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.ReadingDevice;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ReadingDeviceUseCases
{
    public class GetReadingDeviceByIdUseCase : IGetReadingDeviceByIdUseCase
    {
        private readonly IReadingDeviceRepository _readingDeviceRepository;

        public GetReadingDeviceByIdUseCase(IReadingDeviceRepository readingDeviceRepository)
        {
            _readingDeviceRepository = readingDeviceRepository;
        }

        public async Task<bool> HandleAsync(GetReadingDeviceByIdRequest message, IOutputPort<GetReadingDeviceByIdResponse> outputPort)
        {
            var response = await _readingDeviceRepository.GetReadingDeviceById(message);

            outputPort.Handle(response.Success ? new GetReadingDeviceByIdResponse(response.ReadingDeviceId,
                                                                                 response.Key,
                                                                                 response.Name,
                                                                                 response.Manufacturer,
                                                                                 response.Model,
                                                                                 response.SerialNumber,
                                                                                 response.IsActive,
                                                                                 response.AssociatedUserId,
                                                                                 response.SharedUserId,
                                                                                 response.MacAddress,
                                                                                 response.Latitude,
                                                                                 response.Longitude,
                                                                                 response.CompanyId,
                                                                                 true, response.Message) :
                                                 new GetReadingDeviceByIdResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
