﻿using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.ReadingDevice;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ReadingDeviceUseCases
{
    public class DeleteReadingDeviceUseCase : IDeleteReadingDeviceUseCase
    {
        private readonly IReadingDeviceRepository _readingDeviceRepository;

        public DeleteReadingDeviceUseCase(IReadingDeviceRepository readingDeviceRepository)
        {
            _readingDeviceRepository = readingDeviceRepository;
        }

        public async Task<bool> HandleAsync(DeleteReadingDeviceRequest message, IOutputPort<DeleteReadingDeviceResponse> outputPort)
        {
            var response = await _readingDeviceRepository.DeleteReadingDeviceById(message);

            outputPort.Handle(response.Success ? new DeleteReadingDeviceResponse(response.ReadingDeviceId, true, response.Message) :
                                                 new DeleteReadingDeviceResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
