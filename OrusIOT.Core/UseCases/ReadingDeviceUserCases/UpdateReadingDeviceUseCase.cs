﻿using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.ReadingDevice;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ReadingDeviceUseCases
{
    public class UpdateReadingDeviceUseCase : IUpdateReadingDeviceUseCase
    {
        private readonly IReadingDeviceRepository _readingDeviceRepository;

        public UpdateReadingDeviceUseCase(IReadingDeviceRepository readingDeviceRepository)
        {
            _readingDeviceRepository = readingDeviceRepository;
        }

        public async Task<bool> HandleAsync(UpdateReadingDeviceRequest message, IOutputPort<UpdateReadingDeviceResponse> outputPort)
        {
            var response = await _readingDeviceRepository.UpdateReadingDevice(message);

            outputPort.Handle(response.Success ? new UpdateReadingDeviceResponse(response.ReadingDeviceId,
                                                                                 response.Key,
                                                                                 response.Name,
                                                                                 response.Manufacturer,
                                                                                 response.Model,
                                                                                 response.SerialNumber,
                                                                                 response.IsActive,
                                                                                 response.AssociatedUserId,
                                                                                 response.SharedUserId,
                                                                                 response.MacAddress,
                                                                                 response.Latitude,
                                                                                 response.Longitude,
                                                                                 response.CompanyId,
                                                                                 true, response.Message) :
                                                 new UpdateReadingDeviceResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
