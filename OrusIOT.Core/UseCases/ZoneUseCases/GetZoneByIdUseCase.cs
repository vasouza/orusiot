﻿using OrusIOT.Core.Dto.ZoneDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Zone;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ZoneUseCases
{
    public class GetZoneByIdUseCase : IGetZoneByIdUseCase
    {
        private readonly IZoneRepository _zoneRepository;

        public GetZoneByIdUseCase(IZoneRepository zoneRepository)
        {
            _zoneRepository = zoneRepository;
        }

        public async Task<bool> HandleAsync(GetZoneByIdRequest message, IOutputPort<GetZoneByIdResponse> outputPort)
        {
            var response = await _zoneRepository.GetZoneById(message);

            outputPort.Handle(response.Success ? new GetZoneByIdResponse(response.ZoneId, response.Name, response.AreaId, response.IsActive, true, response.Message) :
                                                 new GetZoneByIdResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
