﻿using OrusIOT.Core.Dto.ZoneDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Zone;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ZoneUseCases
{
    public class CreateZoneUseCase : ICreateZoneUseCase
    {
        private readonly IZoneRepository _zoneRepository;

        public CreateZoneUseCase(IZoneRepository zoneRepository)
        {
            _zoneRepository = zoneRepository;
        }

        public async Task<bool> HandleAsync(CreateZoneRequest message, IOutputPort<CreateZoneResponse> outputPort)
        {
            var response = await _zoneRepository.CreateZone(message);

            outputPort.Handle(response.Success ? new CreateZoneResponse(response.ZoneId, response.Name, response.AreaId, response.IsActive, true) :
                                                 new CreateZoneResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
