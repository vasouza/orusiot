﻿using OrusIOT.Core.Dto.ZoneDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Zone;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ZoneUseCases
{
    public class UpdateZoneUseCase : IUpdateZoneUseCase
    {
        private readonly IZoneRepository _zoneRepository;

        public UpdateZoneUseCase(IZoneRepository zoneRepository)
        {
            _zoneRepository = zoneRepository;
        }

        public async Task<bool> HandleAsync(UpdateZoneRequest message, IOutputPort<UpdateZoneResponse> outputPort)
        {
            var response = await _zoneRepository.UpdateZone(message);

            outputPort.Handle(response.Success ? new UpdateZoneResponse(response.ZoneId, response.Name, response.AreaId, response.IsActive, true, response.Message) :
                                                 new UpdateZoneResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
