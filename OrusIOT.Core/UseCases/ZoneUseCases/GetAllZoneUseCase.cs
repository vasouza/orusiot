﻿using OrusIOT.Core.Dto.ZoneDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Zone;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ZoneUseCases
{
    public class GetAllZoneUseCase : IGetAllZoneUseCase
    {
        private readonly IZoneRepository _zoneRepository;

        public GetAllZoneUseCase(IZoneRepository zoneRepository)
        {
            _zoneRepository = zoneRepository;
        }

        public async Task<bool> HandleAsync(GetListZoneRequest message, IOutputPort<GetListZoneResponse> outputPort)
        {
            var response = await _zoneRepository.GetListZones(message);

            outputPort.Handle(response.Success ? new GetListZoneResponse(response.Zones, response.Quantity, response.Page, true, response.Message) :
                                                 new GetListZoneResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
