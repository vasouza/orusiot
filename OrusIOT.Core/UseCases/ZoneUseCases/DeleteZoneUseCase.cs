﻿using OrusIOT.Core.Dto.ZoneDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Zone;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.ZoneUseCases
{
    public class DeleteZoneUseCase : IDeleteZoneUseCase
    {
        private readonly IZoneRepository _zoneRepository;

        public DeleteZoneUseCase(IZoneRepository zoneRepository)
        {
            _zoneRepository = zoneRepository;
        }

        public async Task<bool> HandleAsync(DeleteZoneRequest message, IOutputPort<DeleteZoneResponse> outputPort)
        {
            var response = await _zoneRepository.DeleteZoneById(message);

            outputPort.Handle(response.Success ? new DeleteZoneResponse(response.ZoneId, true, response.Message) :
                                                 new DeleteZoneResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
