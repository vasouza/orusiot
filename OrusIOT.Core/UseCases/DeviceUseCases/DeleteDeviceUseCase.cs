﻿using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Device;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.DeviceUseCases
{
    public class DeleteDeviceUseCase : IDeleteDeviceUseCase
    {
        private readonly IDeviceRepository _deviceRepository;

        public DeleteDeviceUseCase(IDeviceRepository deviceRepository)
        {
            _deviceRepository = deviceRepository;
        }

        public async Task<bool> HandleAsync(DeleteDeviceRequest message, IOutputPort<DeleteDeviceResponse> outputPort)
        {
            var response = await _deviceRepository.DeleteDevice(message.DeviceId);

            outputPort.Handle(response.Success ?
                              new DeleteDeviceResponse(response.DeviceId, true, response.Message) :
                              new DeleteDeviceResponse(response.Errors, response.Message, false));

            return response.Success;
        }
    }
}
