﻿using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Device;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.DeviceUseCases
{
    public class GetDeviceByIdUseCase : IGetDeviceByIdUseCase
    {
        private readonly IDeviceRepository _deviceRepository;

        public GetDeviceByIdUseCase(IDeviceRepository deviceRepository)
        {
            _deviceRepository = deviceRepository;
        }

        public async Task<bool> HandleAsync(GetDeviceByIdRequest message, IOutputPort<GetDeviceByIdResponse> outputPort)
        {
            var response = await _deviceRepository.GetDeviceById(message);
            outputPort.Handle(response.Success ?
                              new GetDeviceByIdResponse(response.DeviceId, response.Name, response.MacAddress, true) :
                              new GetDeviceByIdResponse(response.Errors, false, response.Message));

            return response.Success;
        }
    }
}
