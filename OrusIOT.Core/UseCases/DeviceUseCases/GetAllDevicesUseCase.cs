﻿using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Device;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.DeviceUseCases
{
    public class GetAllDevicesUseCase : IGetAllDevicesUseCase
    {
        private readonly IDeviceRepository _deviceRepository;

        public GetAllDevicesUseCase(IDeviceRepository deviceRepository)
        {
            _deviceRepository = deviceRepository;
        }

        public async Task<bool> HandleAsync(GetDeviceRequest message, IOutputPort<GetDeviceResponse> outputPort)
        {
            var response = await _deviceRepository.GetDevices(message);
            outputPort.Handle(response.Success ?
                new GetDeviceResponse(response.Devices, response.Quantity, response.Page, response.Success, response.Message) :
                new GetDeviceResponse(response.Errors));

            return response.Success;
        }
    }
}
