﻿using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Device;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.DeviceUseCases
{
    public class CreateDeviceUseCase : ICreateDeviceUseCase
    {
        private readonly IDeviceRepository _deviceRepository;

        public CreateDeviceUseCase(IDeviceRepository deviceRepository)
        {
            _deviceRepository = deviceRepository;
        }

        public async Task<bool> HandleAsync(CreateDeviceRequest message, IOutputPort<CreateDeviceResponse> outputPort)
        {
            var response = await _deviceRepository.CreateDevice(message);

            outputPort.Handle(response.Success ? new CreateDeviceResponse(response.DeviceId, response.Name, response.MacAddress, true, response.Message) :
                                                 new CreateDeviceResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
