﻿using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Device;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.DeviceUseCases
{
    public class UpdateDeviceUseCase : IUpdateDeviceUseCase
    {
        private readonly IDeviceRepository _deviceRepository;

        public UpdateDeviceUseCase(IDeviceRepository deviceRepository)
        {
            _deviceRepository = deviceRepository;
        }

        public async Task<bool> HandleAsync(UpdateDeviceRequest message, IOutputPort<UpdateDeviceResponse> outputPort)
        {
            var response = await _deviceRepository.UpdateDevice(message);

            outputPort.Handle(response.Success ?
                              new UpdateDeviceResponse(response.DeviceId, response.Name, response.MacAddress, true, response.Message) :
                              new UpdateDeviceResponse(response.Errors, false, response.Message));

            return response.Success;
        }
    }
}
