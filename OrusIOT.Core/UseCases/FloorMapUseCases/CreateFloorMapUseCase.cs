﻿using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.FloorMap;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.FloorMapUseCases
{
    public class CreateFloorMapUseCase : ICreateFloorMapUseCase
    {
        private readonly IFloorMapRepository _floorMapRepository;

        public CreateFloorMapUseCase(IFloorMapRepository floorMapRepository)
        {
            _floorMapRepository = floorMapRepository;
        }

        public async Task<bool> HandleAsync(CreateFloorMapRequest message, IOutputPort<CreateFloorMapResponse> outputPort)
        {
            var response = await _floorMapRepository.CreateFloorMap(message);

            outputPort.Handle(response.Success ? new CreateFloorMapResponse(response.FloorMapId,
                                                                            response.Name,
                                                                            response.FloorMapZones,
                                                                            response.AreaId,
                                                                            response.Image,
                                                                            response.CompanyId,
                                                                            true,
                                                                            response.Message) :
                                                 new CreateFloorMapResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
