﻿using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.FloorMap;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.FloorMapUseCases
{
    public class UpdateFloorMapUseCase : IUpdateFloorMapUseCase
    {
        private readonly IFloorMapRepository _floorMapRepository;

        public UpdateFloorMapUseCase(IFloorMapRepository floorMapRepository)
        {
            _floorMapRepository = floorMapRepository;
        }

        public async Task<bool> HandleAsync(UpdateFloorMapRequest message, IOutputPort<UpdateFloorMapResponse> outputPort)
        {
            var response = await _floorMapRepository.UpdateFloorMap(message);

            outputPort.Handle(response.Success ?
                              new UpdateFloorMapResponse(response.FloorMapId,
                                                         response.Name,
                                                         response.FloorMapZones,
                                                         response.AreaId,
                                                         response.Image,
                                                         response.CompanyId,
                                                         true,
                                                         response.Message) :
                              new UpdateFloorMapResponse(response.Errors, false, response.Message));

            return response.Success;
        }
    }
}
