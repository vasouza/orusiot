﻿using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.FloorMap;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.FloorMapUseCases
{
    public class GetListFloorMapUseCase : IGetListFloorMapUseCase
    {
        private readonly IFloorMapRepository _floorMapRepository;

        public GetListFloorMapUseCase(IFloorMapRepository floorMapRepository)
        {
            _floorMapRepository = floorMapRepository;
        }

        public async Task<bool> HandleAsync(GetListFloorMapRequest message, IOutputPort<GetListFloorMapResponse> outputPort)
        {
            var response = await _floorMapRepository.GetListFloorMaps(message);

            outputPort.Handle(response.Success ?
                new GetListFloorMapResponse(response.FloorMaps, response.Quantity, response.Page, response.Success, response.Message) :
                new GetListFloorMapResponse(response.Errors));

            return response.Success;
        }
    }
}
