﻿using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.FloorMap;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.FloorMapUseCases
{
    public class DeleteFloorMapUseCase : IDeleteFloorMapUseCase
    {
        private readonly IFloorMapRepository _floorMapRepository;

        public DeleteFloorMapUseCase(IFloorMapRepository floorMapRepository)
        {
            _floorMapRepository = floorMapRepository;
        }

        public async Task<bool> HandleAsync(DeleteFloorMapRequest message, IOutputPort<DeleteFloorMapResponse> outputPort)
        {
            var response = await _floorMapRepository.DeleteFloorMap(message.FloorMapId);

            outputPort.Handle(response.Success ?
                              new DeleteFloorMapResponse(response.FloorMapId, true, response.Message) :
                              new DeleteFloorMapResponse(response.Errors, response.Message, false));

            return response.Success;
        }
    }
}
