﻿using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.FloorMap;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.FloorMapUseCases
{
    public class GetFloorMapByIdUseCase : IGetFloorMapByIdUseCase
    {
        private readonly IFloorMapRepository _floorMapRepository;

        public GetFloorMapByIdUseCase(IFloorMapRepository floorMapRepository)
        {
            _floorMapRepository = floorMapRepository;
        }

        public async Task<bool> HandleAsync(GetFloorMapByIdRequest message, IOutputPort<GetFloorMapByIdResponse> outputPort)
        {
            var response = await _floorMapRepository.GetFloorMapById(message);
            outputPort.Handle(response.Success ?
                              new GetFloorMapByIdResponse(response.FloorMapId, response.Name, response.FloorMapZones, response.AreaId, response.Image, response.CompanyId, true) :
                              new GetFloorMapByIdResponse(response.Errors, false, response.Message));

            return response.Success;
        }
    }
}
