﻿using OrusIOT.Core.Dto.CustodyDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.CustodyUseCases
{
    public class CreatedCustodyUseCase : ICreatedCustodyUseCase
    {
        private readonly ICustodyRepository _custodyRepository;

        public CreatedCustodyUseCase(ICustodyRepository custodyRepository)
        {
            _custodyRepository = custodyRepository;
        }

        public async Task<bool> HandleAsync(CreateCustodyRequest message, IOutputPort<CreateCustodyResponse> outputPort)
        {
            var response = await _custodyRepository.CreateCustody(message);

            outputPort.Handle(response.Success ? new CreateCustodyResponse(response.CustodyId, response.Name, response.Email, response.Address, response.Phone, response.CompanyId, true, response.Message) :
                                                 new CreateCustodyResponse(response.Errors, false ,response.Message));
            return response.Success;
        }
    }
}
