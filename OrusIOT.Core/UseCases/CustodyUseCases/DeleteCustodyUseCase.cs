﻿using OrusIOT.Core.Dto.CustodyDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.CustodyUseCases
{
    public class DeleteCustodyUseCase : IDeleteCustodyUseCase
    {

        private readonly ICustodyRepository _custodyRepository;

        public DeleteCustodyUseCase(ICustodyRepository custodyRepository)
        {
            _custodyRepository = custodyRepository;
        }

        public async Task<bool> HandleAsync(DeleteCustodyRequest message, IOutputPort<DeleteCustodyResponse> outputPort)
        {
            var response = await _custodyRepository.DeleteCustodyById(message.CustodyId);

            outputPort.Handle(response.Success ? new DeleteCustodyResponse(response.CustodyId, true, response.Message) :
                                                 new DeleteCustodyResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
