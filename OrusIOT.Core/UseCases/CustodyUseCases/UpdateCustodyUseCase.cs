﻿using OrusIOT.Core.Dto.CustodyDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.CustodyUseCases
{
    public class UpdateCustodyUseCase : IUpdateCustodyUseCase
    {
        private readonly ICustodyRepository _custodyRepository;

        public UpdateCustodyUseCase(ICustodyRepository custodyRepository)
        {
            _custodyRepository = custodyRepository;
        }

        public async Task<bool> HandleAsync(UpdateCustodyRequest message, IOutputPort<UpdateCustodyResponse> outputPort)
        {
            var response = await _custodyRepository.UpdateCustody(message);

            outputPort.Handle(response.Success ? new UpdateCustodyResponse(response.CustodyId, response.Name, response.Email, response.Address, response.Phone, response.CompanyId, true, response.Message) :
                                                 new UpdateCustodyResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
