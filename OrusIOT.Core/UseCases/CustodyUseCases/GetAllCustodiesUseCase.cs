﻿using OrusIOT.Core.Dto.CustodyDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.CustodyUseCases
{
    public class GetAllCustodiesUseCase : IGetAllCustodiesUseCase
    {
        private readonly ICustodyRepository _custodyRepository;

        public GetAllCustodiesUseCase(ICustodyRepository custodyRepository)
        {
            _custodyRepository = custodyRepository;
        }

        public async Task<bool> HandleAsync(GetListCustodiesRequest message, IOutputPort<GetListCustodiesResponse> outputPort)
        {
            var response = await _custodyRepository.GetListCustodies(message);

            outputPort.Handle(response.Success ? new GetListCustodiesResponse(response.Custodies, response.Quantity, response.Page, true, response.Message) :
                                                 new GetListCustodiesResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
