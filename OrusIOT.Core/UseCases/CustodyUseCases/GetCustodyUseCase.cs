﻿using OrusIOT.Core.Dto.CustodyDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.CustodyUseCases
{
    public class GetCustodyUseCase : IGetCustodyUseCase
    {
        private readonly ICustodyRepository _custodyRepository;

        public GetCustodyUseCase(ICustodyRepository custodyRepository)
        {
            _custodyRepository = custodyRepository;
        }

        public async Task<bool> HandleAsync(GetCustodyByIdRequest message, IOutputPort<GetCustodyByIdResponse> outputPort)
        {
            var response = await _custodyRepository.GetCustodyById(message);

            outputPort.Handle(response.Success ? new GetCustodyByIdResponse(response.CustodyId, response.Name, response.Email, response.Address, response.Phone, response.CompanyId, true, response.Message) :
                                                 new GetCustodyByIdResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
