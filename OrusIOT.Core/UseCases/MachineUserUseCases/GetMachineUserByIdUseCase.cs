﻿using OrusIOT.Core.Dto.MachineUserDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.MachineUser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.MachineUserUseCases
{
    public class GetMachineUserByIdUseCase : IGetMachineUserByIdUseCase
    {
        private readonly IMachineUserRepository _machineUserRepository;

        public GetMachineUserByIdUseCase(IMachineUserRepository machineUserRepository)
        {
            _machineUserRepository = machineUserRepository;
        }

        public async Task<bool> HandleAsync(GetMachineUserByIdRequest message, IOutputPort<GetMachineUserByIdResponse> outputPort)
        {
            var response = await _machineUserRepository.GetMachineUserById(message);

            outputPort.Handle(response.Success ? new GetMachineUserByIdResponse(response.MachineUserId, response.Name, response.Login, response.CompanyId, true, response.Message) :
                                                 new GetMachineUserByIdResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
