﻿using OrusIOT.Core.Dto.MachineUserDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.MachineUser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.MachineUserUseCases
{
    public class GetAllMachineUserUseCase : IGetAllMachineUserUseCase
    {
        private readonly IMachineUserRepository _machineUserRepository;

        public GetAllMachineUserUseCase(IMachineUserRepository machineUserRepository)
        {
            _machineUserRepository = machineUserRepository;
        }

        public async Task<bool> HandleAsync(GetListMachineUserRequest message, IOutputPort<GetListMachineUserResponse> outputPort)
        {
            var response = await _machineUserRepository.GetListMachineUsers(message);

            outputPort.Handle(response.Success ? new GetListMachineUserResponse(response.MachineUsers, response.Quantity, response.Page, true, response.Message) :
                                                 new GetListMachineUserResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
