﻿using OrusIOT.Core.Dto.MachineUserDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.MachineUser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.MachineUserUseCases
{
    public class CreateMachineUserUseCase : ICreateMachineUserUseCase
    {
        private readonly IMachineUserRepository _machineUserRepository;

        public CreateMachineUserUseCase(IMachineUserRepository machineUserRepository)
        {
            _machineUserRepository = machineUserRepository;
        }

        public async Task<bool> HandleAsync(CreateMachineUserRequest message, IOutputPort<CreateMachineUserResponse> outputPort)
        {
            var response = await _machineUserRepository.CreateMachineUser(message);

            outputPort.Handle(response.Success ? new CreateMachineUserResponse(response.MachineUserId, response.Name, response.Login, response.CompanyId, true) :
                                                 new CreateMachineUserResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
