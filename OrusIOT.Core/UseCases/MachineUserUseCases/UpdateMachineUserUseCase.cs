﻿using OrusIOT.Core.Dto.MachineUserDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.MachineUser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.MachineUserUseCases
{
    public class UpdateMachineUserUseCase : IUpdateMachineUserUseCase
    {
        private readonly IMachineUserRepository _machineUserRepository;

        public UpdateMachineUserUseCase(IMachineUserRepository machineUserRepository)
        {
            _machineUserRepository = machineUserRepository;
        }


        public async Task<bool> HandleAsync(UpdateMachineUserRequest message, IOutputPort<UpdateMachineUserResponse> outputPort)
        {
            var response = await _machineUserRepository.UpdateMachineUser(message);

            outputPort.Handle(response.Success ? new UpdateMachineUserResponse(response.MachineUserId, response.Name, response.Login, response.CompanyId, true, response.Message) :
                                                 new UpdateMachineUserResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
