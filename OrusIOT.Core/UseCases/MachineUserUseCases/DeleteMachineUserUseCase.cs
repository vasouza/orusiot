﻿using OrusIOT.Core.Dto.MachineUserDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.MachineUser;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.MachineUserUseCases
{
    public class DeleteMachineUserUseCase : IDeleteMachineUserUseCase
    {
        private readonly IMachineUserRepository _machineUserRepository;

        public DeleteMachineUserUseCase(IMachineUserRepository machineUserRepository)
        {
            _machineUserRepository = machineUserRepository;
        }

        public async Task<bool> HandleAsync(DeleteMachineUserRequest message, IOutputPort<DeleteMachineUserResponse> outputPort)
        {
            var response = await _machineUserRepository.DeleteMachineUserById(message);

            outputPort.Handle(response.Success ? new DeleteMachineUserResponse(response.MachineUserId, true, response.Message) :
                                                 new DeleteMachineUserResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
