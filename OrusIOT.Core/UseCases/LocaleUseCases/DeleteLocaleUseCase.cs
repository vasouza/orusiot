﻿using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Locale;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.LocaleUseCases
{
    public class DeleteLocaleUseCase : IDeleteLocaleUseCase
    {
        private readonly ILocaleRepository _localeRepository;

        public DeleteLocaleUseCase(ILocaleRepository localeRepository)
        {
            _localeRepository = localeRepository;
        } 

        public async Task<bool> HandleAsync(DeleteLocaleRequest message, IOutputPort<DeleteLocaleResponse> outputPort)
        {
            var response = await _localeRepository.DeleteLocale(message);

            outputPort.Handle(response.Success ? new DeleteLocaleResponse(response.LocaleId, true, response.Message) :
                                                 new DeleteLocaleResponse(response.Errors, response.Message, false));
            return response.Success;
        }
    }
}
