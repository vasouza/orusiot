﻿using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Locale;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.LocaleUseCases
{
    public class UpdateLocaleUseCase : IUpdateLocaleUseCase
    {
        private readonly ILocaleRepository _localeRepository;

        public UpdateLocaleUseCase(ILocaleRepository localeRepository)
        {
            _localeRepository = localeRepository;
        }

        public async Task<bool> HandleAsync(UpdateLocaleRequest message, IOutputPort<UpdateLocaleResponse> outputPort)
        {
            var response = await _localeRepository.UpdateLocale(message);

            outputPort.Handle(response.Success ? new UpdateLocaleResponse(response.LocaleId, response.Name, response.IsActive, response.CompanyId, true, response.Message) :
                                                 new UpdateLocaleResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
