﻿using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Locale;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.LocaleUseCases
{
    public class GetLocaleByIdUseCase : IGetLocaleByIdUseCase
    {
        private readonly ILocaleRepository _localeRepository;

        public GetLocaleByIdUseCase(ILocaleRepository localeRepository)
        {
            _localeRepository = localeRepository;
        }

        public async Task<bool> HandleAsync(GetLocaleByIdRequest message, IOutputPort<GetLocaleByIdResponse> outputPort)
        {
            var response = await _localeRepository.GetLocaleById(message);

            outputPort.Handle(response.Success ? new GetLocaleByIdResponse(response.LocaleId, response.Name, response.IsActive, response.CompanyId, true) :
                                                 new GetLocaleByIdResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
