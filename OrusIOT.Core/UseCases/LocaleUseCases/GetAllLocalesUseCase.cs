﻿using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Locale;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.LocaleUseCases
{
    public class GetAllLocalesUseCase : IGetAllLocalesUseCase
    {
        private readonly ILocaleRepository _localeRepository;

        public GetAllLocalesUseCase(ILocaleRepository localeRepository)
        {
            _localeRepository = localeRepository;
        }

        public async Task<bool> HandleAsync(GetListLocalesRequest message, IOutputPort<GetListLocalesResponse> outputPort)
        {
            var response = await _localeRepository.GetLocales(message);

            outputPort.Handle(response.Success ? new GetListLocalesResponse(response.Locales, response.Quantity, response.Page, true, response.Message) :
                                                 new GetListLocalesResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
