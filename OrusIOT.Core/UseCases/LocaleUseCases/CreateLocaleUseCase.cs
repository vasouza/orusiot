﻿using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Interfaces;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases.Locale;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.UseCases.LocaleUseCases
{
    public class CreateLocaleUseCase : ICreateLocaleUseCase
    {
        private readonly ILocaleRepository _localeRepository;

        public CreateLocaleUseCase(ILocaleRepository localeRepository)
        {
            _localeRepository = localeRepository;
        }

        public async Task<bool> HandleAsync(CreateLocaleRequest message, IOutputPort<CreateLocaleResponse> outputPort)
        {
            var response = await _localeRepository.CreateLocale(message);

            outputPort.Handle(response.Success ? new CreateLocaleResponse(response.LocaleId, response.Name, response.IsActive, response.CompanyId, true) :
                                                 new CreateLocaleResponse(response.Errors, false, response.Message));
            return response.Success;
        }
    }
}
