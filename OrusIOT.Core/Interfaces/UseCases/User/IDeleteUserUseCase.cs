﻿using OrusIOT.Core.Dto.UserDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Interfaces.UseCases.User
{
    public interface IDeleteUserUseCase :  IUseCaseRequestHandler<DeleteUserRequest, DeleteUserResponse>
    {
    }
}
