﻿using OrusIOT.Core.Dto;

namespace OrusIOT.Core.Interfaces.UseCases.User
{
    public interface IGetAllUserUseCase : IUseCaseRequestHandler<GetListUserRequest, GetListUserResponse>
    {
    }
}
