﻿using OrusIOT.Core.Dto.MachineUserDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Interfaces.UseCases.MachineUser
{
    public interface ICreateMachineUserUseCase : IUseCaseRequestHandler<CreateMachineUserRequest, CreateMachineUserResponse>
    {
    }
}
