﻿using OrusIOT.Core.Dto.MachineUserDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Interfaces.UseCases.MachineUser
{
    public interface IUpdateMachineUserUseCase : IUseCaseRequestHandler<UpdateMachineUserRequest, UpdateMachineUserResponse>
    {
    }
}
