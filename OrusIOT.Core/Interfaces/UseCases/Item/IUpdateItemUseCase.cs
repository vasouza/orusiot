﻿using OrusIOT.Core.Dto.ItemDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Interfaces.UseCases.Item
{
    public interface IUpdateItemUseCase : IUseCaseRequestHandler<UpdateItemRequest, UpdateItemResponse>
    {
    }
}
