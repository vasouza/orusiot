﻿using OrusIOT.Core.Dto.Device;

namespace OrusIOT.Core.Interfaces.UseCases.Device
{
    public interface IGetAllDevicesUseCase : IUseCaseRequestHandler<GetDeviceRequest, GetDeviceResponse>
    {
    }
}
