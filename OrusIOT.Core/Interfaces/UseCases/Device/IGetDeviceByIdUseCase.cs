﻿using OrusIOT.Core.Dto.Device;

namespace OrusIOT.Core.Interfaces.UseCases.Device
{
    public interface IGetDeviceByIdUseCase : IUseCaseRequestHandler<GetDeviceByIdRequest, GetDeviceByIdResponse>
    {
    }
}
