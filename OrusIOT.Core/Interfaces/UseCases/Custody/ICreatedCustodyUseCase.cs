﻿using OrusIOT.Core.Dto.CustodyDto;

namespace OrusIOT.Core.Interfaces.UseCases
{
    public interface ICreatedCustodyUseCase : IUseCaseRequestHandler<CreateCustodyRequest, CreateCustodyResponse>
    {
    }
}
