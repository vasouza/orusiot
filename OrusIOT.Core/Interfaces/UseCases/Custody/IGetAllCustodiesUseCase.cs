﻿using OrusIOT.Core.Dto.CustodyDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Interfaces.UseCases
{
    public interface IGetAllCustodiesUseCase :  IUseCaseRequestHandler<GetListCustodiesRequest, GetListCustodiesResponse>
    {
    }
}
