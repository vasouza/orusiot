﻿using OrusIOT.Core.Dto.CustodyDto;

namespace OrusIOT.Core.Interfaces.UseCases
{
    public interface IDeleteCustodyUseCase : IUseCaseRequestHandler<DeleteCustodyRequest, DeleteCustodyResponse>
    {
    }
}
