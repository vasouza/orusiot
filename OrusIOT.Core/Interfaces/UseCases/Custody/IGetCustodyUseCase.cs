﻿using OrusIOT.Core.Dto.CustodyDto;

namespace OrusIOT.Core.Interfaces.UseCases
{
    public interface IGetCustodyUseCase : IUseCaseRequestHandler<GetCustodyByIdRequest, GetCustodyByIdResponse>
    {
    }
}
