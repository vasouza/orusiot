﻿using OrusIOT.Core.Dto.FloorMap;

namespace OrusIOT.Core.Interfaces.UseCases.FloorMap
{
    public interface IGetFloorMapByIdUseCase : IUseCaseRequestHandler<GetFloorMapByIdRequest, GetFloorMapByIdResponse>
    {
    }
}
