﻿using OrusIOT.Core.Dto.LocaleDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Interfaces.UseCases.Locale
{
    public interface ICreateLocaleUseCase : IUseCaseRequestHandler<CreateLocaleRequest, CreateLocaleResponse>
    {
    }
}
