﻿using OrusIOT.Core.Dto.AreaDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Interfaces.UseCases.Area
{
    public interface IDeleteAreaUseCase : IUseCaseRequestHandler<DeleteAreaRequest, DeleteAreaResponse>
    {
    }
}
