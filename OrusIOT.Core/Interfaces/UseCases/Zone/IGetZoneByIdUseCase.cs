﻿using OrusIOT.Core.Dto.ZoneDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Interfaces.UseCases.Zone
{
    public interface IGetZoneByIdUseCase : IUseCaseRequestHandler<GetZoneByIdRequest, GetZoneByIdResponse>
    {
    }
}
