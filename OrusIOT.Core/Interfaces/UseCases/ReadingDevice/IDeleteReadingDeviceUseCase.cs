﻿using OrusIOT.Core.Dto.ReadingDeviceDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Interfaces.UseCases.ReadingDevice
{
    public interface IDeleteReadingDeviceUseCase :  IUseCaseRequestHandler<DeleteReadingDeviceRequest, DeleteReadingDeviceResponse>
    {
    }
}
