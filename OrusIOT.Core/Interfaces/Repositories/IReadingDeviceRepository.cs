﻿using OrusIOT.Core.Dto.ReadingDeviceDto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface IReadingDeviceRepository
    {
        Task<GetReadingDeviceByIdResponse> GetReadingDeviceById(GetReadingDeviceByIdRequest getReadingDeviceByIdRequest);
        Task<GetListReadingDeviceResponse> GetListReadingDevices(GetListReadingDeviceRequest getListReadingDeviceRequest);
        Task<DeleteReadingDeviceResponse> DeleteReadingDeviceById(DeleteReadingDeviceRequest deleteReadingDeviceRequest);
        Task<CreateReadingDeviceResponse> CreateReadingDevice(CreateReadingDeviceRequest createReadingDeviceRequest);
        Task<UpdateReadingDeviceResponse> UpdateReadingDevice(UpdateReadingDeviceRequest updateReadingDeviceRequest);
    }
}
