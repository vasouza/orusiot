﻿using OrusIOT.Core.Dto.ItemDto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface IItemRepository
    {
        Task<GetItemByIdResponse> GetItemById(GetItemByIdRequest getItemByIdRequest);
        Task<GetListItemResponse> GetListItems(GetListItemRequest getListItemRequest);
        Task<DeleteItemResponse> DeleteItemById(DeleteItemRequest deleteItemRequest);
        Task<CreateItemResponse> CreateItem(CreateItemRequest createItemRequest);
        Task<UpdateItemResponse> UpdateItem(UpdateItemRequest updateItemRequest);
    }
}
