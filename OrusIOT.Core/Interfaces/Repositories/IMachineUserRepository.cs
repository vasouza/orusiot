﻿using OrusIOT.Core.Dto.MachineUserDto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface IMachineUserRepository
    {
        Task<GetMachineUserByIdResponse> GetMachineUserById(GetMachineUserByIdRequest getMachineUserByIdRequest);
        Task<GetListMachineUserResponse> GetListMachineUsers(GetListMachineUserRequest getListMachineUserRequest);
        Task<DeleteMachineUserResponse> DeleteMachineUserById(DeleteMachineUserRequest deleteMachineUserRequest);
        Task<CreateMachineUserResponse> CreateMachineUser(CreateMachineUserRequest createMachineUserRequest);
        Task<UpdateMachineUserResponse> UpdateMachineUser(UpdateMachineUserRequest updateMachineUserRequest);
    }
}
