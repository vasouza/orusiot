﻿using OrusIOT.Core.Dto.CustodyDto;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface ICustodyRepository
    {
        Task<GetCustodyByIdResponse> GetCustodyById(GetCustodyByIdRequest getCustodyRequest);
        Task<GetListCustodiesResponse> GetListCustodies(GetListCustodiesRequest getListCustodiesRequest);
        Task<DeleteCustodyResponse> DeleteCustodyById(string custodyId);
        Task<CreateCustodyResponse> CreateCustody(CreateCustodyRequest createCustodyRequest);
        Task<UpdateCustodyResponse> UpdateCustody(UpdateCustodyRequest updateCustodyRequest);
    }
}
