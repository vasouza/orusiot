﻿using OrusIOT.Core.Dto.FloorMap;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface IFloorMapRepository
    {
        Task<GetListFloorMapResponse> GetListFloorMaps(GetListFloorMapRequest getListFloorMapRequest);
        Task<GetFloorMapByIdResponse> GetFloorMapById(GetFloorMapByIdRequest getFloorMapRequest);
        Task<CreateFloorMapResponse> CreateFloorMap(CreateFloorMapRequest createFloorMapRequest);
        Task<UpdateFloorMapResponse> UpdateFloorMap(UpdateFloorMapRequest updateFloorMapRequest);
        Task<DeleteFloorMapResponse> DeleteFloorMap(string floorMapId);
    }
}
