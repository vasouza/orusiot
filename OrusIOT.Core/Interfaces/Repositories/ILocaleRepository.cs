﻿using OrusIOT.Core.Dto.LocaleDto;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface ILocaleRepository
    {
        Task<GetListLocalesResponse> GetLocales(GetListLocalesRequest request);
        Task<GetLocaleByIdResponse> GetLocaleById(GetLocaleByIdRequest request);
        Task<UpdateLocaleResponse> UpdateLocale(UpdateLocaleRequest request);
        Task<CreateLocaleResponse> CreateLocale(CreateLocaleRequest request);
        Task<DeleteLocaleResponse> DeleteLocale(DeleteLocaleRequest request);
    }
}
