﻿using OrusIOT.Core.Dto.ZoneDto;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface IZoneRepository
    {
        Task<GetZoneByIdResponse> GetZoneById(GetZoneByIdRequest getZoneByIdRequest);
        Task<GetListZoneResponse> GetListZones(GetListZoneRequest getListZoneRequest);
        Task<DeleteZoneResponse> DeleteZoneById(DeleteZoneRequest deleteZoneRequest);
        Task<CreateZoneResponse> CreateZone(CreateZoneRequest createZoneRequest);
        Task<UpdateZoneResponse> UpdateZone(UpdateZoneRequest updateZoneRequest);
    }
}
