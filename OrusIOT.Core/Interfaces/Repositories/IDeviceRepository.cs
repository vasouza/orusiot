﻿using OrusIOT.Core.Dto;
using OrusIOT.Core.Dto.Device;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface IDeviceRepository
    {
        Task<GetDeviceResponse> GetDevices(GetDeviceRequest getDeviceRequest);
        Task<GetDeviceByIdResponse> GetDeviceById(GetDeviceByIdRequest getCustodyRequest);
        Task<CreateDeviceResponse> CreateDevice(CreateDeviceRequest createCustodyRequest);
        Task<UpdateDeviceResponse> UpdateDevice(UpdateDeviceRequest updateCustodyRequest);
        Task<DeleteDeviceResponse> DeleteDevice(string deviceId);
    }
}
