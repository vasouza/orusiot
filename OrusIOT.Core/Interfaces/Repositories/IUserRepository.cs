﻿using OrusIOT.Core.Dto;
using OrusIOT.Core.Dto.UserDto;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface IUserRepository
    {
        Task<GetListUserResponse> GetUsers(GetListUserRequest request);
        Task<GetUserByIdResponse> GetUserById(GetUserByIdRequest request);
        Task<DeleteUserResponse> DeleteUserById(DeleteUserRequest deleteUserRequest);
        Task<CreateUserResponse> CreateUser(CreateUserRequest createUserRequest);
        Task<UpdateUserResponse> UpdateUser(UpdateUserRequest updateUserRequest);

        //Task<RecoverUserPasswordResponse> RecoverPassword(RecoverUserPasswordRequest recoverUserPassword);
        //Task<LoginUserResponse> LoginUser(LoginUserRequest loginUserRequest);
    }
}
