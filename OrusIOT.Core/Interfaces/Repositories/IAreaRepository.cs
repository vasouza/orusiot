﻿using OrusIOT.Core.Dto.AreaDto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface IAreaRepository
    {
        Task<GetAreaByIdResponse> GetAreaById(GetAreaByIdRequest getAreaByIdRequest);
        Task<GetListAreaResponse> GetListAreas(GetListAreaRequest getListAreaRequest);
        Task<DeleteAreaResponse> DeleteAreaById(DeleteAreaRequest deleteAreaRequest);
        Task<CreateAreaResponse> CreateArea(CreateAreaRequest createAreaRequest);
        Task<UpdateAreaResponse> UpdateArea(UpdateAreaRequest updateAreaRequest);
    }
}
