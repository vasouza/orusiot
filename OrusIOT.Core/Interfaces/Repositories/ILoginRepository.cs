﻿using OrusIOT.Core.Dto.LoginDto;
using OrusIOT.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Core.Interfaces.Repositories
{
    public interface ILoginRepository
    {
        Task<LoginResponse> FindUser(LoginRequest login);
    }
}
