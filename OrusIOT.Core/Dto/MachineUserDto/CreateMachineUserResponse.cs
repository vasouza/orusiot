﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.MachineUserDto
{
    public class CreateMachineUserResponse : BaseResponse
    {
        public string MachineUserId { get; }
        public string Name { get; }
        public string Login { get; }
        public string CompanyId { get; }
        public IEnumerable<string> Errors { get; }

        public CreateMachineUserResponse(string machineUserId, string name, string login, string companyId, bool success = false, string message = null) : base(success, message)
        {
            MachineUserId = machineUserId;
            Name = name;
            Login = login;
            CompanyId = companyId;
        }

        public CreateMachineUserResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
