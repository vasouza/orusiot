﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.MachineUserDto
{
    public class DeleteMachineUserResponse : BaseResponse
    {
        public string MachineUserId { get; }
        public IEnumerable<string> Errors { get; }

        public DeleteMachineUserResponse(string machineUserId, bool success = false, string message = null) : base(success, message)
        {
            MachineUserId = machineUserId;
        }

        public DeleteMachineUserResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
