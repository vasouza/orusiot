﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.MachineUserDto
{
    public class UpdateMachineUserRequest : IUseCaseRequest<UpdateMachineUserResponse>
    {
        public string MachineUserId { get; }
        public string Name { get; }
        public string Login { get; }
        public string CompanyId { get; }

        public UpdateMachineUserRequest(string machineUserid, string name, string login, string companyId)
        {
            MachineUserId = machineUserid;
            Name = name;
            Login = login;
            CompanyId = companyId;
        }
    }
}
