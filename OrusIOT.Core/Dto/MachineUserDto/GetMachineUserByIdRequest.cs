﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.MachineUserDto
{
    public  class GetMachineUserByIdRequest : IUseCaseRequest<GetMachineUserByIdResponse>
    {
        public string MachineUserId { get; }

        public GetMachineUserByIdRequest(string machineUserId)
        {
            MachineUserId = machineUserId;
        }
    }
}
