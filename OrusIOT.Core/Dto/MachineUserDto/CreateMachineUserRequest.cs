﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.MachineUserDto
{
    public class CreateMachineUserRequest : IUseCaseRequest<CreateMachineUserResponse>
    {
        public string Name { get; }
        public string Login { get; }
        public string CompanyId { get; }

        public CreateMachineUserRequest(string name, string login, string companyId)
        {
            Name = name;
            Login = login;
            CompanyId = companyId;
        }
    }
}
