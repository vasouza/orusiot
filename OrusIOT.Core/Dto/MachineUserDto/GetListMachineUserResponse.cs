﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.MachineUserDto
{
    public class GetListMachineUserResponse : BaseResponse
    {
        public IEnumerable<Entities.MachineUser> MachineUsers { get; }
        public int Quantity { get; }
        public int Page { get; }
        public IEnumerable<string> Errors { get; }

        public GetListMachineUserResponse(IEnumerable<Entities.MachineUser> machineUsers, int quantity, int page, bool success = false, string message = null) : base(success, message)
        {
            MachineUsers = machineUsers;
            Quantity = quantity;
            Page = page;
        }

        public GetListMachineUserResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
