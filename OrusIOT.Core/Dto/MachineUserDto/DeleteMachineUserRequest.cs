﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.MachineUserDto
{
    public class DeleteMachineUserRequest : IUseCaseRequest<DeleteMachineUserResponse>
    {
        public string MachineUserId { get; }

        public DeleteMachineUserRequest(string machineUserId)
        {
            MachineUserId = machineUserId;
        }
    }
}
