﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ReadingDeviceDto
{
    public class GetReadingDeviceByIdRequest : IUseCaseRequest<GetReadingDeviceByIdResponse>
    {
        public string ReadingDeviceId { get; }

        public GetReadingDeviceByIdRequest(string readingDeviceId)
        {
            ReadingDeviceId = readingDeviceId;
        }
    }
}
