﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ReadingDeviceDto
{
    public class DeleteReadingDeviceRequest : IUseCaseRequest<DeleteReadingDeviceResponse>
    {
        public string ReadingDeviceId { get; }

        public DeleteReadingDeviceRequest(string readingDeviceId)
        {
            ReadingDeviceId = readingDeviceId;
        }
    }
}
