﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ReadingDeviceDto
{
    public class GetListReadingDeviceResponse : BaseResponse
    {
        public IEnumerable<Entities.ReadingDevice> ReadingDevices { get; }
        public int Quantity { get; }
        public int Page { get; }
        public IEnumerable<string> Errors { get; }

        public GetListReadingDeviceResponse(IEnumerable<Entities.ReadingDevice> readingDevices, int quantity, int page, bool success = false, string message = null) : base(success, message)
        {
            ReadingDevices = readingDevices;
            Quantity = quantity;
            Page = page;
        }

        public GetListReadingDeviceResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
