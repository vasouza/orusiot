﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ReadingDeviceDto
{
    public class DeleteReadingDeviceResponse : BaseResponse
    {
        public string ReadingDeviceId { get;  }
        public IEnumerable<string> Errors { get; }

        public DeleteReadingDeviceResponse(string readingDeviceId, bool success = false, string message = null) : base(success, message)
        {
            ReadingDeviceId = readingDeviceId;
        }

        public DeleteReadingDeviceResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
