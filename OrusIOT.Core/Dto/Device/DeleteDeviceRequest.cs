﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.Device
{
    public class DeleteDeviceRequest : IUseCaseRequest<DeleteDeviceResponse>
    {
        public string DeviceId { get; }

        public DeleteDeviceRequest(string deviceId)
        {
            DeviceId = deviceId;
        }
    }
}
