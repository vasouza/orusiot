﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.Device
{
    public class GetDeviceByIdRequest : IUseCaseRequest<GetDeviceByIdResponse>
    {
        public string DeviceId { get; }

        public GetDeviceByIdRequest(string deviceId)
        {
            DeviceId = deviceId;
        }
    }
}
