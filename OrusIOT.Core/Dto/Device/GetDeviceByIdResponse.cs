﻿using System.Collections.Generic;

namespace OrusIOT.Core.Dto.Device
{
    public class GetDeviceByIdResponse : BaseResponse
    {
        public string DeviceId { get; }
        public string Name { get; }
        public string MacAddress { get; }

        public IEnumerable<string> Errors { get; set; }

        public GetDeviceByIdResponse(string deviceId, string name, string macAddress, bool success = false, string message = null) : base(success, message)
        {
            DeviceId = deviceId;
            Name = name;
            MacAddress = macAddress;
        }

        public GetDeviceByIdResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
