﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.Device
{
    public class UpdateDeviceRequest : IUseCaseRequest<UpdateDeviceResponse>
    {
        public string DeviceId { get; }
        public string Name { get; }
        public string MacAddress { get; }

        public UpdateDeviceRequest(string deviceId, string name, string macAddress)
        {
            DeviceId = deviceId;
            Name = name;
            MacAddress = macAddress;
        }
    }
}
