﻿using System.Collections.Generic;

namespace OrusIOT.Core.Dto.Device
{
    public class DeleteDeviceResponse : BaseResponse
    {
        public string DeviceId { get; }
        public IEnumerable<string> Errors { get; }

        public DeleteDeviceResponse(string deviceId, bool success = false, string message = null) : base(success, message)
        {
            DeviceId = deviceId;
        }

        public DeleteDeviceResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
