﻿using System.Collections.Generic;

namespace OrusIOT.Core.Dto.Device
{
    public class GetDeviceResponse : BaseResponse
    {
        public IEnumerable<Entities.Device> Devices { get; set; }
        public int Quantity { get; set; }
        public int Page { get; set; }
        public IEnumerable<string> Errors { get; set; }

        public GetDeviceResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }

        public GetDeviceResponse(IEnumerable<Entities.Device> devices, int quantity, int page, bool success = false, string message = null) : base(success, message)
        {
            Devices = devices;
            Quantity = quantity;
            Page = page;
        }
    }
}
