﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.Device
{
    public class CreateDeviceRequest : IUseCaseRequest<CreateDeviceResponse>
    {
        public string Name { get; }
        public string MacAddress { get; }

        public CreateDeviceRequest(string name, string macAddress)
        {
            Name = name;
            MacAddress = macAddress;
        }
    }
}
