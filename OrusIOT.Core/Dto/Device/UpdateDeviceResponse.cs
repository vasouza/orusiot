﻿using System.Collections.Generic;

namespace OrusIOT.Core.Dto.Device
{
    public class UpdateDeviceResponse : BaseResponse
    {
        public string DeviceId { get; }
        public string Name { get; }
        public string MacAddress { get; }

        public IEnumerable<string> Errors { get; set; }

        public UpdateDeviceResponse(string deviceId, string name, string macAddress, bool success = false, string message = null) : base(success, message)
        {
            DeviceId = deviceId;
            Name = name;
            MacAddress = macAddress;
        }

        public UpdateDeviceResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
