﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.Device
{
    public class GetDeviceRequest : IUseCaseRequest<GetDeviceResponse>
    {
        public string CompanyId { get; }
        public int Quantity { get; }
        public int Skip { get; }
        public int Page { get; }

        public GetDeviceRequest(string companyId, int quantity = 500, int skip = 0, int page = 0)
        {
            CompanyId = companyId;
            Quantity = quantity;
            Skip = skip;
            Page = page;
        }
    }
}
