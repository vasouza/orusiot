﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ZoneDto
{
    public class CreateZoneRequest :  IUseCaseRequest<CreateZoneResponse>
    {
        public string Name { get; }
        public string AreaId { get; }
        public bool IsActive { get; }

        public CreateZoneRequest(string name, string areaId, bool isActive)
        {
            Name = name;
            AreaId = areaId;
            IsActive = isActive;
        }
    }
}
