﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ZoneDto
{
    public class CreateZoneResponse : BaseResponse
    {
        public string ZoneId { get; }
        public string Name { get; }
        public string AreaId { get; }
        public bool IsActive { get;  }
        public IEnumerable<string> Errors { get; }

        public CreateZoneResponse(string zoneId, string name, string areaId, bool isActive, bool success = false, string message = null) : base(success, message)
        {
            ZoneId = zoneId;
            Name = name;
            AreaId = areaId;
            IsActive = isActive;
        }

        public CreateZoneResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
