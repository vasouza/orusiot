﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ZoneDto
{
    public class GetZoneByIdRequest : IUseCaseRequest<GetZoneByIdResponse>
    {
        public string ZoneId { get; }

        public GetZoneByIdRequest(string zoneId)
        {
            ZoneId = zoneId;
        }
    }
}
