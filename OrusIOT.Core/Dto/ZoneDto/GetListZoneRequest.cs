﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ZoneDto
{
    public class GetListZoneRequest : IUseCaseRequest<GetListZoneResponse>
    {
        public string AreaId { get; }
        public int Quantity { get; }
        public int Skip { get; }
        public int Page { get; }

        public GetListZoneRequest(string areaId,  int quantity = 500, int skip = 0, int page = 0)
        {
            AreaId = areaId;
            Quantity = quantity;
            Skip = skip;
            Page = page;
        }
    }
}
