﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ZoneDto
{
    public class DeleteZoneResponse : BaseResponse
    {
        public string ZoneId { get;  }
        public IEnumerable<string> Errors { get; }

        public DeleteZoneResponse(string zoneId, bool success = false, string message = null) : base(success, message)
        {
            ZoneId = zoneId;
        }

        public DeleteZoneResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
