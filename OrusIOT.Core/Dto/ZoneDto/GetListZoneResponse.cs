﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ZoneDto
{
    public class GetListZoneResponse : BaseResponse
    {
        public IEnumerable<Entities.Zone> Zones { get; }
        public int Quantity { get; }
        public int Page { get; }
        public IEnumerable<string> Errors { get; }

        public GetListZoneResponse(IEnumerable<Entities.Zone> zones, int quantity, int page, bool success = false, string message = null) : base(success, message)
        {
            Zones = zones;
            Quantity = quantity;
            Page = page;
        }

        public GetListZoneResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
