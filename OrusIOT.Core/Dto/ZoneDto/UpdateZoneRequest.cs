﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ZoneDto
{
    public class UpdateZoneRequest :  IUseCaseRequest<UpdateZoneResponse>
    {
        public string ZoneId { get; }
        public string Name { get; }
        public string AreaId { get; }
        public bool IsActive { get; }

        public UpdateZoneRequest(string zoneId, string name, string areaId, bool isActive)
        {
            ZoneId = zoneId;
            Name = name;
            AreaId = areaId;
            IsActive = isActive;
        }
    }
}
