﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ZoneDto
{
    public class DeleteZoneRequest : IUseCaseRequest<DeleteZoneResponse>
    {
        public string ZoneId { get; }

        public DeleteZoneRequest(string zoneId)
        {
            ZoneId = zoneId;
        }
    }
}
