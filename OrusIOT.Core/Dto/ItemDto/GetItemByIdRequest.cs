﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ItemDto
{
    public class GetItemByIdRequest : IUseCaseRequest<GetItemByIdResponse>
    {
        public string ItemId { get; }

        public GetItemByIdRequest(string itemId)
        {
            ItemId = itemId;
        }
    }
}
