﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ItemDto
{
    public class UpdateItemRequest :  IUseCaseRequest<UpdateItemResponse>
    {
        public string ItemId { get; }
        public string Name { get; }
        public string Code { get; }
        public string SerialNumber { get; }
        public bool IsActive { get; }
        public string CustodyId { get; }
        public string LocaleId { get; }
        public string CompanyId { get; }

        public UpdateItemRequest(string itemId, string name, string code, string serialNumber, bool isActive, string custodyId, string localeId, string companyId)
        {
            ItemId = itemId;
            Name = name;
            Code = code;
            SerialNumber = serialNumber;
            IsActive = isActive;
            CustodyId = custodyId;
            LocaleId = localeId;
            CompanyId = companyId;
        }
    }
}
