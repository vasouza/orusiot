﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ItemDto
{
    public class GetListItemResponse : BaseResponse
    {
        public IEnumerable<Entities.Item> Items { get; }
        public int Quantity { get; }
        public int Page { get; }
        public IEnumerable<string> Errors { get; }

        public GetListItemResponse(IEnumerable<Entities.Item> items, int quantity, int page, bool success = false, string message = null) : base(success, message)
        {
            Items = items;
            Quantity = quantity;
            Page = page;
        }

        public GetListItemResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
