﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ItemDto
{
    public class DeleteItemResponse : BaseResponse
    {
        public string ItemId { get;  }
        public IEnumerable<string> Errors { get; }

        public DeleteItemResponse(string itemId, bool success = false, string message = null) : base(success, message)
        {
            ItemId = itemId;
        }

        public DeleteItemResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
