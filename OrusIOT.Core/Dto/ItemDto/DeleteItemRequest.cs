﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ItemDto
{
    public class DeleteItemRequest : IUseCaseRequest<DeleteItemResponse>
    {
        public string ItemId { get; }

        public DeleteItemRequest(string itemId)
        {
            ItemId = itemId;
        }
    }
}
