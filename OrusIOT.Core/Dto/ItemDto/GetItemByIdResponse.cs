﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.ItemDto
{
    public class GetItemByIdResponse : BaseResponse
    {
        public string ItemId { get; }
        public string Name { get; }
        public string Code { get;  }
        public string SerialNumber { get; }
        public bool IsActive { get; }
        public string CustodyId { get; }
        public string LocaleId { get; }
        public string CompanyId { get; }

        public IEnumerable<string> Errors { get; }

        public GetItemByIdResponse(string itemId, string name, string code, string serialNumber, bool isActive, string custodyId, string localeId, string companyId, bool success = false, string message = null) : base(success, message)
        {
            ItemId = itemId;
            Name = name;
            Code = code;
            SerialNumber = serialNumber;
            IsActive = isActive;
            CustodyId = custodyId;
            LocaleId = localeId;
            CompanyId = companyId;
        }

        public GetItemByIdResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
