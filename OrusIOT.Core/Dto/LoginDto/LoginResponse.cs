﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.LoginDto
{
    public class LoginResponse : BaseResponse
    {
        public bool Authenticated { get; }
        public string Created { get; }
        public string Expiration { get; }
        public string Token { get; }

        public IEnumerable<string> Errors { get; }

        public LoginResponse(bool authenticated, string created, string expiration, string token, bool success = false, string message = null) : base(success, message)
        {
            Authenticated = authenticated;
            Created = created;
            Expiration = expiration;
            Token = token;
        }

        public LoginResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
