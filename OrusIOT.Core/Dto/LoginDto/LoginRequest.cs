﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.LoginDto
{
    public class LoginRequest : IUseCaseRequest<LoginResponse>
    {
        public string Login { get; }
        public string Password { get; }
        public SigningConfigurations SigningConfigurations { get;  }
        public TokenConfigurations TokenConfigurations { get;  }

        public LoginRequest(string login, string password, SigningConfigurations signingConfigurations, TokenConfigurations tokenConfigurations)
        {
            Login = login;
            Password = password;
            SigningConfigurations = signingConfigurations;
            TokenConfigurations = tokenConfigurations;
        }
    }
}
