﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.FloorMap
{
    public class UpdateFloorMapRequest : IUseCaseRequest<UpdateFloorMapResponse>
    {
        public string FloorMapId { get; }
        public string Name { get; }
        public string FloorMapZones { get; }
        public string AreaId { get; }
        public string  Image { get; }
        public string CompanyId { get; }

        public UpdateFloorMapRequest(string floorMapId, string name, string floorMapZones, string areaId, string image, string companyId)
        {
            FloorMapId = floorMapId;
            Name = name;
            FloorMapZones = floorMapZones;
            AreaId = areaId;
            Image = image;
            CompanyId = companyId;
        }
    }
}
