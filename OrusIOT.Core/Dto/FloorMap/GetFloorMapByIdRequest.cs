﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.FloorMap
{
    public class GetFloorMapByIdRequest : IUseCaseRequest<GetFloorMapByIdResponse>
    {
        public string FloorMapId { get; }

        public GetFloorMapByIdRequest(string floorMapId)
        {
            FloorMapId = floorMapId;
        }
    }
}
