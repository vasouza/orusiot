﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.FloorMap
{
    public class DeleteFloorMapRequest : IUseCaseRequest<DeleteFloorMapResponse>
    {
        public string FloorMapId { get; }

        public DeleteFloorMapRequest(string floorMapId)
        {
            FloorMapId = floorMapId;
        }
    }
}
