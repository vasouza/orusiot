﻿using System.Collections.Generic;

namespace OrusIOT.Core.Dto.FloorMap
{
    public class GetListFloorMapResponse : BaseResponse
    {
        public IEnumerable<Entities.FloorMap> FloorMaps { get; set; }
        public int Quantity { get; set; }
        public int Page { get; set; }
        public IEnumerable<string> Errors { get; set; }

        public GetListFloorMapResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }

        public GetListFloorMapResponse(IEnumerable<Entities.FloorMap> floorMaps,
                                       int quantity,
                                       int page,
                                       bool success = false,
                                       string message = null) : base(success, message)
        {
            FloorMaps = floorMaps;
            Quantity = quantity;
            Page = page;
        }
    }
}
