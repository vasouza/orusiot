﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.FloorMap
{
    public class GetListFloorMapRequest : IUseCaseRequest<GetListFloorMapResponse>
    {
        public int Quantity { get; }
        public int Skip { get; }
        public int Page { get; }
        public string CompanyId { get; }

        public GetListFloorMapRequest(string companyId, int quantity = 500, int skip = 0, int page = 0)
        {
            Quantity = quantity;
            Skip = skip;
            Page = page;
            CompanyId = companyId;
        }
    }
}
