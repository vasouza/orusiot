﻿using System.Collections.Generic;

namespace OrusIOT.Core.Dto.FloorMap
{
    public class DeleteFloorMapResponse : BaseResponse
    {
        public string FloorMapId { get; }
        public IEnumerable<string> Errors { get; }

        public DeleteFloorMapResponse(string floorMapId, bool success = false, string message = null) : base(success, message)
        {
            FloorMapId = floorMapId;
        }

        public DeleteFloorMapResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
