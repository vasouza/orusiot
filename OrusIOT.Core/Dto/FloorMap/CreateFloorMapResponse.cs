﻿using System.Collections.Generic;

namespace OrusIOT.Core.Dto.FloorMap
{
    public class CreateFloorMapResponse : BaseResponse
    {
        public string FloorMapId { get; }
        public string Name { get; }
        public string FloorMapZones { get; }
        public string AreaId { get; }
        public string Image { get; }
        public string CompanyId { get; set; }

        public IEnumerable<string> Errors { get; }

        public CreateFloorMapResponse(string floorMapId,
                                      string name,
                                      string floorMapZones,
                                      string areaId,
                                      string image,
                                      string companyId,
                                      bool success = false,
                                      string message = null) : base(success, message)
        {
            FloorMapId = floorMapId;
            Name = name;
            FloorMapZones = floorMapZones;
            AreaId = areaId;
            Image = image;
            CompanyId = companyId;
        }

        public CreateFloorMapResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
