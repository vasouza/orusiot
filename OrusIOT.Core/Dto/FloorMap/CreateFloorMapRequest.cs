﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.FloorMap
{
    public class CreateFloorMapRequest : IUseCaseRequest<CreateFloorMapResponse>
    {
        public string Name { get; }
        public string FloorMapZones { get; }
        public string AreaId { get; }
        public string Image { get; }
        public string CompanyId { get; set; }

        public CreateFloorMapRequest(string name, string floorMapZones, string areaId, string image, string companyId)
        {
            Name = name;
            FloorMapZones = floorMapZones;
            AreaId = areaId;
            Image = image;
            CompanyId = companyId;
        }
    }
}
