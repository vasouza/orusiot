﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.AreaDto
{
    public class DeleteAreaResponse : BaseResponse
    {
        public string AreaId { get; }
        public IEnumerable<string> Errors { get; }

        public DeleteAreaResponse(string areaId, bool success = false, string message = null) : base(success, message)
        {
            AreaId = areaId;
        }

        public DeleteAreaResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
