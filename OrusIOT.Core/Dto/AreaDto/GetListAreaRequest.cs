﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.AreaDto
{
    public class GetListAreaRequest : IUseCaseRequest<GetListAreaResponse>
    {
        public string CompanyId { get; set; }
        public int Quantity { get; }
        public int Skip { get; }
        public int Page { get; }

        public GetListAreaRequest(string companyId,int quantity = 500, int skip = 0, int page = 0)
        {
            CompanyId = companyId;
            Quantity = quantity;
            Skip = skip;
            Page = page;
        }
    }
}
