﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.AreaDto
{
    public class GetAreaByIdResponse : BaseResponse
    {
        public string AreaId { get; }
        public string Name { get; }
        public string Address { get; }
        public string Floor { get; }
        public string City { get; }
        public string Country { get; }
        public string ZipCode { get; }
        public string LocaleId { get; }
        public string CompanyId { get; }
        public bool IsActive { get; }
        public IEnumerable<string> Errors { get; }

        public GetAreaByIdResponse(string areaId, string name, string address, string floor, string city, string country,
                                  string zipCode, string localeId, string companyId, bool isActive, bool success = false, string message = null) : base(success, message)
        {
            AreaId = areaId;
            Name = name;
            Address = address;
            Floor = floor;
            City = city;
            Country = country;
            ZipCode = zipCode;
            LocaleId = localeId;
            IsActive = isActive;
            CompanyId = companyId;
        }

        public GetAreaByIdResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
