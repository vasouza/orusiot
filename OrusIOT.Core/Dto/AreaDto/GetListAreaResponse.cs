﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.AreaDto
{
    public class GetListAreaResponse : BaseResponse
    {
        public IEnumerable<Entities.Area> Areas { get; }
        public int Quantity { get; }
        public int Page { get; }
        public IEnumerable<string> Errors { get; }

        public GetListAreaResponse(IEnumerable<Entities.Area> areas, int quantity, int page, bool success = false, string message = null) : base(success, message)
        {
            Areas = areas;
            Quantity = quantity;
            Page = page;
        }

        public GetListAreaResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
