﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.AreaDto
{
    public class GetAreaByIdRequest : IUseCaseRequest<GetAreaByIdResponse>
    {
        public string AreaId { get; }

        public GetAreaByIdRequest(string areaId)
        {
            AreaId = areaId;
        }
    }
}
