﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.AreaDto
{
    public class DeleteAreaRequest : IUseCaseRequest<DeleteAreaResponse>
    {
        public string AreaId { get; }

        public DeleteAreaRequest(string areaId)
        {
            AreaId = areaId;
        }
    }
}
