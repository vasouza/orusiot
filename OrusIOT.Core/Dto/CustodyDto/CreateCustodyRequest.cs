﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.CustodyDto
{
    public class CreateCustodyRequest : IUseCaseRequest<CreateCustodyResponse>
    {
        public string Name { get; }
        public string Email { get; }
        public string Phone { get; }
        public string Address { get; }
        public string CompanyId { get; }

        public CreateCustodyRequest(string name, string email, string phone, string address, string companyId)
        {
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            CompanyId = companyId;
        }
    }
}
