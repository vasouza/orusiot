﻿using System.Collections.Generic;

namespace OrusIOT.Core.Dto.CustodyDto
{
    public class DeleteCustodyResponse : BaseResponse
    {
        public string CustodyId { get; }
        public IEnumerable<string> Errors { get; }

        public DeleteCustodyResponse(string custodyId, bool success = false, string message = null) : base(success, message)
        {
            CustodyId = custodyId;
        }

        public DeleteCustodyResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
