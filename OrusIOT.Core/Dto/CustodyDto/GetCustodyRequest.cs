﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.CustodyDto
{
    public class GetCustodyRequest :  IUseCaseRequest<GetCustodyByIdResponse>
    {
        public Guid CustodyId { get; }

        public GetCustodyRequest(Guid custodyId)
        {
            CustodyId = custodyId;
        }
    }
}
