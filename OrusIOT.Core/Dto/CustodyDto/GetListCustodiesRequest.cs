﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.CustodyDto
{
    public class GetListCustodiesRequest : IUseCaseRequest<GetListCustodiesResponse>
    {
        public string CompanyId { get; }
        public int Quantity { get; }
        public int Skip { get; }
        public int Page { get; }

        public GetListCustodiesRequest(string companyId, int quantity = 500, int skip = 0, int page = 0)
        {
            CompanyId = companyId;
            Quantity = quantity;
            Skip = skip;
            Page = page;
        }
    }
}
