﻿using System.Collections.Generic;

namespace OrusIOT.Core.Dto.CustodyDto
{
    public class GetCustodyByIdResponse : BaseResponse
    {
        public string CustodyId { get; }
        public string Name { get; }
        public string Email { get; }
        public string Phone { get; }
        public string Address { get; }
        public string CompanyId { get; }

        public IEnumerable<string> Errors { get; set; }

        public GetCustodyByIdResponse(string custodyId, string name, string email, string address, string phone, string companyId, bool success = false, string message = null) : base(success, message)
        {
            CustodyId = custodyId;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            CompanyId = companyId;
        }

        public GetCustodyByIdResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
