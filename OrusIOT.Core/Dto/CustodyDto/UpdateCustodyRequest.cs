﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.CustodyDto
{
    public class UpdateCustodyRequest : IUseCaseRequest<UpdateCustodyResponse>
    {
        public string CustodyId { get; }
        public string Name { get; }
        public string Email { get; }
        public string Phone { get; }
        public string Address { get; }
        public string CompanyId { get; }

        public UpdateCustodyRequest(string custodyId, string name, string email, string phone, string address, string companyId)
        {
            CustodyId = custodyId;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            CompanyId = companyId;
        }
    }
}
