﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.CustodyDto
{
    public class DeleteCustodyRequest : IUseCaseRequest<DeleteCustodyResponse>
    {
        public string CustodyId { get; }

        public DeleteCustodyRequest(string custodyId)
        {
            CustodyId = custodyId;
        }
    }
}
