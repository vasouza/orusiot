﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto.CustodyDto
{
    public class GetCustodyByIdRequest : IUseCaseRequest<GetCustodyByIdResponse>
    {
        public string CustodyId { get; }

        public GetCustodyByIdRequest(string custodyId)
        {
            CustodyId = custodyId;
        }
    }
}
