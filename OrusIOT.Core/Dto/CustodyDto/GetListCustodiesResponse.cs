﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.CustodyDto
{
    public class GetListCustodiesResponse : BaseResponse
    {
        public IEnumerable<Entities.Custody> Custodies { get; }
        public int Quantity { get; set; }
        public int Page { get; set; }
        public IEnumerable<string> Errors { get; set; }

        public GetListCustodiesResponse(IEnumerable<Entities.Custody> custodies, int quantity, int page, bool success = false, string message = null) : base(success, message)
        {
            Custodies = custodies;
            Quantity = quantity;
            Page = page;
        }

        public GetListCustodiesResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
