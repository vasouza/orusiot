﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class LoginUserRequest : IUseCaseRequest<LoginUserRequest>
    {
        public string Login { get; }
        public string Password { get; }

        public LoginUserRequest(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }
}
