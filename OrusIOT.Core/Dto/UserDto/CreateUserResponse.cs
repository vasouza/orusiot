﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class CreateUserResponse : BaseResponse
    {

        public string Login { get; }
        public string Address { get; }
        public string Name { get; }
        public string Email { get; }
        public string Phone { get; }
        public bool IsActive { get; }
        public string Password { get; }
        public string DepartmentId { get; }
        public string CompanyId { get; }
        public IEnumerable<string> Errors { get; }

        public CreateUserResponse(string login, string address, string name, string email, string phone, bool isActive, string departmentId, string companyId, bool success = false, string message = null) : base(success, message)
        {
            Login = login;
            Address = address;
            Name = name;
            Email = email;
            Phone = phone;
            IsActive = isActive;
            DepartmentId = departmentId;
            CompanyId = companyId;
        }

        public CreateUserResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
