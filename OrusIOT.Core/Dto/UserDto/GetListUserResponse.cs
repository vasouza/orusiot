﻿using OrusIOT.Core.Entities;
using System.Collections.Generic;

namespace OrusIOT.Core.Dto
{
    public class GetListUserResponse : BaseResponse
    {
        public IEnumerable<User> Users { get;  }
        public int Quantity { get; }
        public int Page { get; }
        public IEnumerable<string> Errors { get; }

        public GetListUserResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }

        public GetListUserResponse(IEnumerable<User> users, int quantity, int page ,bool success = false, string message = null) : base(success, message)
        {
            Users = users;
            Quantity = quantity;
            Page = page;
        }
    }
}
