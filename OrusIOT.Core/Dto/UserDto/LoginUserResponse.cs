﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class LoginUserResponse : BaseResponse
    {
        public string Token { get; set; }
        public IEnumerable<string> Errors { get; }

        public LoginUserResponse(string token, bool success = false, string message = null) : base(success, message)
        {
            Token = token;
        }

        public LoginUserResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
