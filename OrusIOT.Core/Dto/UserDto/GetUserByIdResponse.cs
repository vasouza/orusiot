﻿using OrusIOT.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class GetUserByIdResponse : BaseResponse
    {
        public User User { get; }
        public IEnumerable<string> Errors { get; }


        public GetUserByIdResponse(User user, bool success = false, string message = null) : base(success, message)
        {
            User = user;
        } 

        public GetUserByIdResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
