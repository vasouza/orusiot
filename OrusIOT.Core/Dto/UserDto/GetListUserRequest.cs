﻿using OrusIOT.Core.Interfaces;

namespace OrusIOT.Core.Dto
{
    public class GetListUserRequest : IUseCaseRequest<GetListUserResponse>
    {
        public string CompanyId { get; set; }
        public int Quantity { get; }
        public int Skip { get; }
        public int Page { get; }

        public GetListUserRequest(string companyId, int quantity = 500, int skip = 0, int page = 0)
        {
            CompanyId = companyId;
            Quantity = quantity;
            Skip = skip;
            Page = page;
        }
    }
}
