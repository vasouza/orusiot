﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class DeleteUserRequest : IUseCaseRequest<DeleteUserResponse>
    {
        public string UserId { get; }

        public DeleteUserRequest(string userId)
        {
            UserId = userId;
        }
    }
}
