﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class GetUserByIdRequest : IUseCaseRequest<GetUserByIdResponse>
    {
        public string UserId { get; }

        public GetUserByIdRequest(string userId)
        {
            UserId = userId;
        }
    }
}
