﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class RecoverUserPasswordRequest : IUseCaseRequest<RecoverUserPasswordResponse>
    {
        public string Login { get; }
        public string Email { get; }

        public RecoverUserPasswordRequest(string login, string email)
        {
            Login = login;
            Email = email;
        }
    }
}
