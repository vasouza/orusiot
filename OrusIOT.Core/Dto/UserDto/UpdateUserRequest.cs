﻿using OrusIOT.Core.Entities;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class UpdateUserRequest :  IUseCaseRequest<UpdateUserResponse>
    {
        public string UserId { get; }
        public string Login { get; }
        public string Address { get; }
        public string Name { get; }
        public string Email { get; }
        public string Phone { get; }
        public bool IsActive { get; }
        public string Password { get; }
        public string DepartmentId { get; }
        public string CompanyId { get; }

        public UpdateUserRequest(string userId, string login, string address, string name, string email, string phone, bool isActive, string departmentId, string companyId)
        {
            UserId = userId;
            Login = login;
            Address = address;
            Name = name;
            Email = email;
            Phone = phone;
            IsActive = isActive;
            DepartmentId = departmentId;
            CompanyId = companyId;
        }
    }
}
