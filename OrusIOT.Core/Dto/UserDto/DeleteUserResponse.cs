﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class DeleteUserResponse : BaseResponse
    {
        public string UserId { get;  }
        public IEnumerable<string> Errors { get; }

        public DeleteUserResponse(string userId, bool success = false, string message = null) : base(success, message)
        {
            UserId = userId;
        }

        public DeleteUserResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
