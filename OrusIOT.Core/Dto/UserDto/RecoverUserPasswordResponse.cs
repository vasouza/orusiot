﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.UserDto
{
    public class RecoverUserPasswordResponse : BaseResponse
    {
        public string TokenReset { get; set; }
        public IEnumerable<string> Errors { get; }


        public RecoverUserPasswordResponse(string tokenReset, bool success = false, string message = null) : base(success, message)
        {
            TokenReset = tokenReset;
        }

        public RecoverUserPasswordResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
