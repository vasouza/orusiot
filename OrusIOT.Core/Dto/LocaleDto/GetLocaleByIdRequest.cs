﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.LocaleDto
{
    public class GetLocaleByIdRequest : IUseCaseRequest<GetLocaleByIdResponse>
    {
        public string LocaleId { get; set; }

        public GetLocaleByIdRequest(string localeId)
        {
            LocaleId = localeId;
        }
    }
}
