﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.LocaleDto
{
    public class GetLocaleByIdResponse : BaseResponse
    {
        public string LocaleId { get; }
        public string Name { get; }
        public bool IsActive { get; }
        public string CompanyId { get; }
        public IEnumerable<string> Errors { get; }

        public GetLocaleByIdResponse(string localeId, string name, bool isActive, string companyId, bool success = false, string message = null) : base(success, message)
        {
            LocaleId = localeId;
            Name = name;
            IsActive = isActive;
            CompanyId = companyId;
        }

        public GetLocaleByIdResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
