﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.LocaleDto
{
    public class GetListLocalesResponse : BaseResponse
    {
        public IEnumerable<Entities.Locale> Locales { get; }

        public int Quantity { get; }
        public int Page { get; }
        public IEnumerable<string> Errors { get; }

        public GetListLocalesResponse(IEnumerable<Entities.Locale> locales, int quantity, int page, bool success = false, string message = null) : base(success, message)
        {
            Locales = locales;
            Quantity = quantity;
            Page = page;
        }

        public GetListLocalesResponse(IEnumerable<string> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }
    }
}
