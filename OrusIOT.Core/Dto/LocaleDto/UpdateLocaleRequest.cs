﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.LocaleDto
{
    public class UpdateLocaleRequest : IUseCaseRequest<UpdateLocaleResponse>
    {
        public string LocaleId { get; }
        public string Name { get; }
        public bool IsActive { get; }
        public string CompanyId { get; }

        public UpdateLocaleRequest(string localeId, string name, bool isActive, string companyId)
        {
            LocaleId = localeId;
            Name = name;
            IsActive = isActive;
            CompanyId = companyId;
        }
    }
}
