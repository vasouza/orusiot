﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.LocaleDto
{
    public class DeleteLocaleRequest : IUseCaseRequest<DeleteLocaleResponse> 
    {
        public string LocaleId { get; }

        public DeleteLocaleRequest(string localeId)
        {
            LocaleId = localeId;
        }
    }
}
