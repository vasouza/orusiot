﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.LocaleDto
{
    public class DeleteLocaleResponse : BaseResponse
    {
        public string LocaleId { get; }
        public IEnumerable<string> Errors { get; }

        public DeleteLocaleResponse(string localeId, bool success = false, string message = null) : base(success, message)
        {
            LocaleId = localeId;
        }

        public DeleteLocaleResponse(IEnumerable<string> errors, string message = null, bool success = false) : base(success, message)
        {
            Errors = errors;
        }
    }
}
