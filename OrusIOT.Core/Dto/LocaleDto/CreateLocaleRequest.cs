﻿using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Dto.LocaleDto
{
    public class CreateLocaleRequest : IUseCaseRequest<CreateLocaleResponse>
    {
        public string Name { get; }
        public bool IsActive { get; }
        public string CompanyId { get; }

        public CreateLocaleRequest(string name, bool isActive, string companyId)
        {
            Name = name;
            IsActive = isActive;
            CompanyId = companyId;
        }
    }
}
