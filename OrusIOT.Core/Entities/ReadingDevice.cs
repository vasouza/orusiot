﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Entities
{
    public class ReadingDevice
    {
        public string ReadingDeviceId { get;}
        public string Key { get; }
        public string Name { get; }
        public string Manufacturer { get; }
        public string Model { get; }
        public string SerialNumber { get; }
        public bool IsActive { get; }
        public string AssociatedUserId { get; }
        public string SharedUserId { get;  }
        public string MacAddress { get; }
        public int Latitude { get;  }
        public int Longitude { get; }
        public string CompanyId { get; }


        public ReadingDevice(string readingDeviceId, string key, string name, string manufacturer, string model, string serialNumber, bool isActive,
                             string associatedUserId, string sharedUserId, string macAddress, int latitude, int longitude, string companyId)
        {
            ReadingDeviceId = readingDeviceId;
            Key = key;
            Name = name;
            Manufacturer = manufacturer;
            Model = model;
            SerialNumber = serialNumber;
            IsActive = isActive;
            AssociatedUserId = associatedUserId;
            SharedUserId = sharedUserId;
            MacAddress = macAddress;
            Latitude = latitude;
            Longitude = longitude;
            CompanyId = companyId;
        }
    }
}
