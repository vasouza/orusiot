﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Entities
{
    public class Locale
    {
        public string LocaleId { get; }
        public string Name { get; }
        public bool IsActive { get; }
        public string CompanyId { get; }

        public Locale(string localeId, string name, bool isActive, string companyId)
        {
            LocaleId = localeId;
            Name = name;
            IsActive = isActive;
            CompanyId = companyId;
        }
    }
}
