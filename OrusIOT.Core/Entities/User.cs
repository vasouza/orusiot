﻿using System;

namespace OrusIOT.Core.Entities
{
    public class User
    {
        public string UserId { get; }
        public string Login { get; }
        public string Address { get; }
        public string Name { get; }
        public string Email { get; }
        public string Phone { get; }
        public bool IsActive { get; }
        public string Password { get; }
        public Department Department { get; }
        public Company Company { get; }

        public User(string userId, string login, string address, string name, string email, string phone, bool isActive,
                    string password, Department department, Company company)
        {
            UserId = userId;
            Login = login;
            Address = address;
            Name = name;
            Email = email;
            Phone = phone;
            IsActive = IsActive;
            Password = password;
            Department = department;
            Company = company;
            IsActive = isActive;
        }
    }
}
