﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Entities
{
    public class Custody
    {
        public string CustodyId { get; }
        public string Name { get; }
        public string Email { get; }
        public string Address { get; }
        public string Phone { get; }
        public string CompanyId { get; }

        public Custody(string custodyId, string name, string email, string address, string phone, string companyId)
        {
            CustodyId = custodyId;
            Name = name;
            Email = name;
            Address = address;
            Phone = phone;
            CompanyId = companyId;
        }

    }
}
