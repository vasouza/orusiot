﻿using System;

namespace OrusIOT.Core.Entities
{
    public class Company
    {
        public string CompanyId { get; }
        public string Name { get; }
        public string Address { get; }
        public string PrimaryEmail { get; }
        public string SecondaryEmail { get; }
        public string Logo { get; }
        public bool IsActive { get; }

        public Company(string companyId, string name, string address, string primaryEmail, string secondaryEmail, string logo, bool isActive)
        {
            CompanyId = companyId;
            Name = name;
            Address = address;
            PrimaryEmail = primaryEmail;
            SecondaryEmail = secondaryEmail;
            Logo = logo;
            IsActive = isActive;
        }
    }
}
