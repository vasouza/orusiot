﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Entities
{
    public class MachineUser
    {
        public string MachineUserId { get;  }
        public string Name { get;  }
        public string Login { get; }
        public string CompanyId { get; }

        public MachineUser(string machineUserId, string name, string login, string companyId)
        {
            MachineUserId = machineUserId;
            Name = name;
            Login = login;
            CompanyId = companyId;
        }
    }
}
