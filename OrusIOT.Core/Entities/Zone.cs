﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Core.Entities
{
    public class Zone
    {
        public string ZoneId { get; }
        public string Name { get; }
        public string AreaId { get; }
        public bool IsActive { get; }

        public Zone(string zoneId, string name, string areaId, bool isActive)
        {
            ZoneId = zoneId;
            Name = name;
            AreaId = areaId;
            IsActive = isActive;
        }
    }
}
