﻿namespace OrusIOT.Core.Entities
{
    public class FloorMap
    {
        public string FloorMapId { get; }
        public string Name { get; }
        public string FloorMapZones { get; }
        public string AreaId { get; }
        public string Image { get; }
        public string CompanyId { get; }

        public FloorMap(string floorMapId, string name, string floorMapZones, string areaId, string image, string companyId)
        {
            FloorMapId = floorMapId;
            Name = name;
            FloorMapZones = floorMapZones;
            AreaId = areaId;
            Image = image;
            CompanyId = companyId;
        }
    }
}
