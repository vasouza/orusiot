﻿using System;

namespace OrusIOT.Core.Entities
{
    public class Department
    {
        public string DepartmentId { get; }
        public string Name { get; }
        public Company Company { get; }

        public Department(string departmentId, string name, Company company)
        {
            DepartmentId = departmentId;
            Name = name;
            Company = company;
        }
    }
}
