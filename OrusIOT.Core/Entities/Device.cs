﻿using System;

namespace OrusIOT.Core.Entities
{
    public class Device
    {
        public string DeviceId { get; }
        public string MacAddress { get; }
        public string Name { get; }

        public Device(string deviceId, string macAddress, string name)
        {
            DeviceId = deviceId;
            MacAddress = macAddress;
            Name = name;
        }
    }
}
