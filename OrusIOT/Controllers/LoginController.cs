﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core;
using OrusIOT.Core.Interfaces.UseCases.Login;
using OrusIOT.Infrastructure;
using OrusIOT.Presenter.LoginPresenter;
using System.Threading.Tasks;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILoginUseCase _loginUseCase;
        private readonly LoginPresenter _loginPresenter;

        public LoginController(ILoginUseCase loginUseCase, LoginPresenter loginPresenter)
        {
            _loginUseCase = loginUseCase;
            _loginPresenter = loginPresenter;
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(Model.Login.LoginResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> LoginAsync([FromBody] Model.Login.LoginUser loginUser, [FromServices] LoginRepository usersDAO,
                                                   [FromServices] SigningConfigurations signingConfigurations, [FromServices] TokenConfigurations tokenConfigurations)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _loginUseCase.HandleAsync(new Core.Dto.LoginDto.LoginRequest(loginUser.Login, loginUser.Password,
                                                                               signingConfigurations, tokenConfigurations), _loginPresenter);

            return _loginPresenter.ContentResult;

        }
    }
}