﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases;
using OrusIOT.Model;
using OrusIOT.Presenter.CustodyPresenter;
using System.Threading.Tasks;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize("Bearer")]
    public class CustodyController : ControllerBase
    {
        private readonly ICreatedCustodyUseCase _createdCustodyUseCase;
        private readonly CreateCustodyPresenter _createCustodyPresenter;

        private readonly IDeleteCustodyUseCase _deleteCustodyUseCase;
        private readonly DeleteCustodyPresenter _deleteCustodyPresenter;

        private readonly IGetAllCustodiesUseCase _getAllCustodiesUseCase;
        private readonly GetAllCustodiesPresenter _getAllCustodiesPresenter;

        private readonly IGetCustodyUseCase _getCustodyUseCase;
        private readonly GetCustodyPresenter _getCustodyPresenter;

        private readonly IUpdateCustodyUseCase _updateCustodyUseCase;
        private readonly UpdateCustodyPresenter _updateCustodyPresenter;


        public CustodyController(ICreatedCustodyUseCase createdCustodyUseCase, CreateCustodyPresenter createCustodyPresenter,
                                IDeleteCustodyUseCase deleteCustodyUseCase, DeleteCustodyPresenter deleteCustodyPresenter,
                                IGetAllCustodiesUseCase getAllCustodiesUseCase, GetAllCustodiesPresenter getAllCustodiesPresenter,
                                IGetCustodyUseCase getCustodyUseCase, GetCustodyPresenter getCustodyPresenter,
                                IUpdateCustodyUseCase updateCustodyUseCase, UpdateCustodyPresenter updateCustodyPresenter)
        {
            _createdCustodyUseCase = createdCustodyUseCase;
            _createCustodyPresenter = createCustodyPresenter;

            _deleteCustodyUseCase = deleteCustodyUseCase;
            _deleteCustodyPresenter = deleteCustodyPresenter;

            _getAllCustodiesUseCase = getAllCustodiesUseCase;
            _getAllCustodiesPresenter = getAllCustodiesPresenter;

            _getCustodyUseCase = getCustodyUseCase;
            _getCustodyPresenter = getCustodyPresenter;

            _updateCustodyUseCase = updateCustodyUseCase;
            _updateCustodyPresenter = updateCustodyPresenter;
        }

        [HttpGet]
        [Route("{custodyId}")]
        [ProducesResponseType(typeof(Core.Dto.CustodyDto.GetCustodyByIdResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetCustodyByIdAsync(string custodyId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _getCustodyUseCase.HandleAsync(new Core.Dto.CustodyDto.GetCustodyByIdRequest(custodyId), _getCustodyPresenter);

            return _getCustodyPresenter.ContentResult;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.CustodyDto.GetListCustodiesResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetListCustodiesAsync(string companyId, int quantity = 500, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }


            if (page < 0)
                page = 0;

            await _getAllCustodiesUseCase.HandleAsync(new Core.Dto.CustodyDto.GetListCustodiesRequest(companyId, quantity, skip, page), _getAllCustodiesPresenter);

            return _getAllCustodiesPresenter.ContentResult;
        }


        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.CustodyDto.CreateCustodyResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> CreateCustodyAsync([FromBody] CreateCustodyRequest createCustodyRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _createdCustodyUseCase.HandleAsync(new Core.Dto.CustodyDto.CreateCustodyRequest(createCustodyRequest.Name, createCustodyRequest.Email,
                                                                                                  createCustodyRequest.Phone, createCustodyRequest.Address,
                                                                                                  createCustodyRequest.CompanyId), _createCustodyPresenter);

            return _createCustodyPresenter.ContentResult;
        }

        [HttpDelete]
        [Route("{custodyId}")]
        [ProducesResponseType(typeof(Core.Dto.CustodyDto.DeleteCustodyResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> DeleteCustodyAsync(string custodyId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _deleteCustodyUseCase.HandleAsync(new Core.Dto.CustodyDto.DeleteCustodyRequest(custodyId), _deleteCustodyPresenter);

            return _deleteCustodyPresenter.ContentResult;
        }


        [HttpPut]
        [ProducesResponseType(typeof(Core.Dto.CustodyDto.UpdateCustodyResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> UpdateCustodyAsync([FromBody] UpdateCustodyRequest updateCustodyRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _updateCustodyUseCase.HandleAsync(new Core.Dto.CustodyDto.UpdateCustodyRequest(updateCustodyRequest.CustodyId, updateCustodyRequest.Name,
                                                                                                updateCustodyRequest.Email, updateCustodyRequest.Phone,
                                                                                                updateCustodyRequest.Address, updateCustodyRequest.CompanyId), _updateCustodyPresenter);

            return _updateCustodyPresenter.ContentResult;
        }
    }
}