﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases.Zone;
using OrusIOT.Presenter.ZonePresenter;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ZoneController : ControllerBase
    {
        private readonly IGetAllZoneUseCase _getAllZonesUseCase;
        private readonly GetAllZonesPresenter _getAllZonesPresenter;

        private readonly IGetZoneByIdUseCase _getZoneByIdUseCase;
        private readonly GetZoneByIdPresenter _getZoneByIdPresenter;

        private readonly ICreateZoneUseCase _createZoneUseCase;
        private readonly CreateZonePresenter _createZonePresenter;

        private readonly IUpdateZoneUseCase _updateZoneUseCase;
        private readonly UpdateZonePresenter _updateZonePresenter;

        private readonly IDeleteZoneUseCase _deleteZoneUseCase;
        private readonly DeleteZonePresenter _deleteZonePresenter;


        public ZoneController(IGetAllZoneUseCase getAllZonesUseCase, GetAllZonesPresenter getAllZonesPresenter,
                             IGetZoneByIdUseCase getZoneByIdUseCase, GetZoneByIdPresenter getZoneByIdPresenter,
                             ICreateZoneUseCase createZoneUseCase, CreateZonePresenter createZonePresenter,
                             IUpdateZoneUseCase updateZoneUseCase, UpdateZonePresenter updateZonePresenter,
                             IDeleteZoneUseCase deleteZoneUseCase, DeleteZonePresenter deleteZonePresenter)
        {
            _getAllZonesUseCase = getAllZonesUseCase;
            _getAllZonesPresenter = getAllZonesPresenter;

            _getZoneByIdUseCase = getZoneByIdUseCase;
            _getZoneByIdPresenter = getZoneByIdPresenter;

            _createZoneUseCase = createZoneUseCase;
            _createZonePresenter = createZonePresenter;

            _updateZoneUseCase = updateZoneUseCase;
            _updateZonePresenter = updateZonePresenter;

            _deleteZoneUseCase = deleteZoneUseCase;
            _deleteZonePresenter = deleteZonePresenter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.ZoneDto.GetListZoneResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> GetAllZonesAsync(string areaId, int quantity = 500, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            if (page < 0)
                page = 0;

            await _getAllZonesUseCase.HandleAsync(new Core.Dto.ZoneDto.GetListZoneRequest(areaId, quantity, skip, page), _getAllZonesPresenter);

            return _getAllZonesPresenter.ContentResult;
        }

        [HttpGet]
        [Route("{zoneId}")]
        [ProducesResponseType(typeof(Core.Dto.ZoneDto.GetZoneByIdResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> GetZoneByIdAsync(string zoneId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _getZoneByIdUseCase.HandleAsync(new Core.Dto.ZoneDto.GetZoneByIdRequest(zoneId), _getZoneByIdPresenter);

            return _getZoneByIdPresenter.ContentResult;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.ZoneDto.CreateZoneResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> CreateZoneAsync([FromBody] Model.CreateZoneRequest createZoneRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _createZoneUseCase.HandleAsync(new Core.Dto.ZoneDto.CreateZoneRequest(createZoneRequest.Name, createZoneRequest.AreaId, createZoneRequest.IsActive), _createZonePresenter);

            return _createZonePresenter.ContentResult;
        }

        [HttpPut]
        [ProducesResponseType(typeof(Core.Dto.ZoneDto.UpdateZoneResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> UpdateZoneAsync([FromBody] Model.UpdateZoneByIdRequest updateZoneByIdRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _updateZoneUseCase.HandleAsync(new Core.Dto.ZoneDto.UpdateZoneRequest(updateZoneByIdRequest.ZoneId,
                                                                                        updateZoneByIdRequest.Name,
                                                                                        updateZoneByIdRequest.AreaId,
                                                                                        updateZoneByIdRequest.IsActive), _updateZonePresenter);

            return _updateZonePresenter.ContentResult;
        }

        [HttpDelete]
        [Route("{zoneId}")]
        [ProducesResponseType(typeof(Core.Dto.ZoneDto.DeleteZoneResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> DeleteZoneAsync(string zoneId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _deleteZoneUseCase.HandleAsync(new Core.Dto.ZoneDto.DeleteZoneRequest(zoneId), _deleteZonePresenter);

            return _deleteZonePresenter.ContentResult;
        }
    }
}