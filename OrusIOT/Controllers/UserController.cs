﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases;
using OrusIOT.Core.Interfaces.UseCases.User;
using OrusIOT.Presenter;
using OrusIOT.Presenter.UserPresenter;
using System.Threading.Tasks;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IGetAllUserUseCase _getAllUserUseCase;
        private readonly GetAllUserPresenter _getAllUserPresenter;


        private readonly IGetUserByIdUseCase _getUserByIdUseCase;
        private readonly GetUserByIdPresenter _getUserByIdPresenter;

        private readonly ICreateUserUseCase _createUserUseCase;
        private readonly CreateUserPresenter _createUserPresenter;

        private readonly IUpdateUserUseCase _updateUserUseCase;
        private readonly UpdateUserPresenter _updateUserPresenter;

        private readonly IDeleteUserUseCase _deleteUserUseCase;
        private readonly DeleteUserPresenter _deleteUserPresenter;


        public UserController(IGetAllUserUseCase getAllUserUseCase, GetAllUserPresenter getAllUserPresenter,
                              IGetUserByIdUseCase getUserByIdUseCase, GetUserByIdPresenter getUserByIdPresenter,
                              ICreateUserUseCase createUserUseCase, CreateUserPresenter createUserPresenter,
                              IUpdateUserUseCase updateUserUseCase, UpdateUserPresenter updateUserPresenter,
                              IDeleteUserUseCase deleteUserUseCase, DeleteUserPresenter deleteUserPresenter)
        {
            _getAllUserUseCase = getAllUserUseCase;
            _getAllUserPresenter = getAllUserPresenter;

            _getUserByIdUseCase = getUserByIdUseCase;
            _getUserByIdPresenter = getUserByIdPresenter;

            _createUserUseCase = createUserUseCase;
            _createUserPresenter = createUserPresenter;

            _updateUserUseCase = updateUserUseCase;
            _updateUserPresenter = updateUserPresenter;

            _deleteUserUseCase = deleteUserUseCase;
            _deleteUserPresenter = deleteUserPresenter;
        }


        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.GetListUserResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> GetAllEnterprises(string companyId, int quantity = 500, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            if (page < 0)
                page = 0;

            await _getAllUserUseCase.HandleAsync(new Core.Dto.GetListUserRequest(companyId, quantity, skip, page), _getAllUserPresenter);

            return _getAllUserPresenter.ContentResult;
        }

        [HttpGet]
        [Route("{userId}")]
        [ProducesResponseType(typeof(Core.Dto.UserDto.GetUserByIdResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> GetAllEnterprises(string userId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _getUserByIdUseCase.HandleAsync(new Core.Dto.UserDto.GetUserByIdRequest(userId), _getUserByIdPresenter);

            return _getUserByIdPresenter.ContentResult;
        }


        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.UserDto.CreateUserResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> CreateUserAsync([FromBody] Model.CreateUserRequest createUserRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _createUserUseCase.HandleAsync(new Core.Dto.UserDto.CreateUserRequest(createUserRequest.Login,
                                                                                        createUserRequest.Address,
                                                                                        createUserRequest.Name,
                                                                                        createUserRequest.Email,
                                                                                        createUserRequest.Phone,
                                                                                        createUserRequest.IsActive,
                                                                                        createUserRequest.DepartmentId,
                                                                                        createUserRequest.CompanyId), _createUserPresenter);

            return _createUserPresenter.ContentResult;
        }

        [HttpPut]
        [ProducesResponseType(typeof(Core.Dto.UserDto.UpdateUserResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> UpdateUserAsync([FromBody] Model.UpdateUserByIdRequest updateUserByIdRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _updateUserUseCase.HandleAsync(new Core.Dto.UserDto.UpdateUserRequest(updateUserByIdRequest.UserId,
                                                                                        updateUserByIdRequest.Login,
                                                                                        updateUserByIdRequest.Address,
                                                                                        updateUserByIdRequest.Name,
                                                                                        updateUserByIdRequest.Email,
                                                                                        updateUserByIdRequest.Phone,
                                                                                        updateUserByIdRequest.IsActive,
                                                                                        updateUserByIdRequest.DepartmentId,
                                                                                        updateUserByIdRequest.CompanyId), _updateUserPresenter);

            return _updateUserPresenter.ContentResult;
        }

        [HttpDelete]
        [Route("{UserId}")]
        [ProducesResponseType(typeof(Core.Dto.UserDto.DeleteUserResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> DeleteUserAsync(string UserId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _deleteUserUseCase.HandleAsync(new Core.Dto.UserDto.DeleteUserRequest(UserId), _deleteUserPresenter);

            return _deleteUserPresenter.ContentResult;
        }
    }
}
