﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases.Device;
using OrusIOT.Model;
using OrusIOT.Presenter;
using OrusIOT.Presenter.Device;
using System.Threading.Tasks;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize("Bearer")]
    public class DeviceController : ControllerBase
    {
        private readonly IGetAllDevicesUseCase _getAllDevicesUseCase;
        private readonly GetAllDevicesPresenter _getAllDevicesPresenter;

        private readonly IGetDeviceByIdUseCase _getDeviceByIdUseCase;
        private readonly GetDeviceByIdPresenter _getDeviceByIdPresenter;

        private readonly ICreateDeviceUseCase _createDeviceUseCase;
        private readonly CreateDevicePresenter _createDevicePresenter;

        private readonly IUpdateDeviceUseCase _updateDeviceUseCase;
        private readonly UpdateDevicePresenter _updateDevicePresenter;

        private readonly IDeleteDeviceUseCase _deleteDeviceUseCase;
        private readonly DeleteDevicePresenter _deleteDevicePresenter;

        public DeviceController(IGetAllDevicesUseCase getAllDevicesUseCase, GetAllDevicesPresenter getAllDevicesPresenter,
                                IGetDeviceByIdUseCase getDeviceByIdUseCase, GetDeviceByIdPresenter getDeviceByIdPresenter,
                                ICreateDeviceUseCase createDeviceUseCase, CreateDevicePresenter createDevicePresenter,
                                IUpdateDeviceUseCase updateDeviceUseCase, UpdateDevicePresenter updateDevicePresenter,
                                IDeleteDeviceUseCase deleteDeviceUseCase, DeleteDevicePresenter deleteDevicePresenter)
        {
            _getAllDevicesUseCase = getAllDevicesUseCase;
            _getAllDevicesPresenter = getAllDevicesPresenter;

            _getDeviceByIdUseCase = getDeviceByIdUseCase;
            _getDeviceByIdPresenter = getDeviceByIdPresenter;

            _createDeviceUseCase = createDeviceUseCase;
            _createDevicePresenter = createDevicePresenter;

            _updateDeviceUseCase = updateDeviceUseCase;
            _updateDevicePresenter = updateDevicePresenter;

            _deleteDeviceUseCase = deleteDeviceUseCase;
            _deleteDevicePresenter = deleteDevicePresenter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.Device.GetDeviceRequest), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetAll(string companyId, int quantity = 10, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (page < 0)
                page = 0;

            await _getAllDevicesUseCase.HandleAsync(new Core.Dto.Device.GetDeviceRequest(companyId,
                                                                                         quantity,
                                                                                         skip,
                                                                                         page),
                                                    _getAllDevicesPresenter);
            return _getAllDevicesPresenter.ContentResult;
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(Core.Dto.Device.GetDeviceByIdResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> Get(string id)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _getDeviceByIdUseCase.HandleAsync(new Core.Dto.Device.GetDeviceByIdRequest(id), _getDeviceByIdPresenter);

            return _getDeviceByIdPresenter.ContentResult;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.Device.CreateDeviceResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> CreateDeviceAsync([FromBody] CreateDeviceRequest createDeviceRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _createDeviceUseCase.HandleAsync(
                new Core.Dto.Device.CreateDeviceRequest(createDeviceRequest.Name,
                                                        createDeviceRequest.MacAddress),
                _createDevicePresenter);

            return _createDevicePresenter.ContentResult;
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(Core.Dto.Device.UpdateDeviceResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> UpdateCustodyAsync(string id, [FromBody] UpdateDeviceRequest updateDeviceRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _updateDeviceUseCase.HandleAsync(
                new Core.Dto.Device.UpdateDeviceRequest(id,
                                                        updateDeviceRequest.Name,
                                                        updateDeviceRequest.MacAddress),
                _updateDevicePresenter);

            return _updateDevicePresenter.ContentResult;
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(Core.Dto.Device.DeleteDeviceResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> DeleteDeviceAsync(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _deleteDeviceUseCase.HandleAsync(new Core.Dto.Device.DeleteDeviceRequest(id), _deleteDevicePresenter);

            return _deleteDevicePresenter.ContentResult;
        }
    }
}