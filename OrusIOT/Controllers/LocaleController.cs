﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases.Locale;
using OrusIOT.Model;
using OrusIOT.Presenter.LocalePresenter;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize("Bearer")]
    public class LocaleController : ControllerBase
    {
        private readonly IGetAllLocalesUseCase _getAllLocalesUseCase;
        private readonly GetAllLocalesPresenter _getAllLocalesPresenter;

        private readonly IGetLocaleByIdUseCase _getLocaleByIdUseCase;
        private readonly GetLocaleByIdPresenter _getLocaleByIdPresenter;

        private readonly ICreateLocaleUseCase _createLocaleUseCase;
        private readonly CreateLocalePresenter _createLocalePresenter;

        private readonly IUpdateLocaleUseCase _updateLocaleUseCase;
        private readonly UpdateLocalePresenter _updateLocalePresenter;

        private readonly IDeleteLocaleUseCase _deleteLocaleUseCase;
        private readonly DeleteLocalePresenter _deleteLocalePresenter;

        public LocaleController(IGetAllLocalesUseCase getAllLocalesUseCase, GetAllLocalesPresenter getAllLocalesPresenter,
                                IGetLocaleByIdUseCase getLocaleByIdUseCase, GetLocaleByIdPresenter getLocaleByIdPresenter,
                                ICreateLocaleUseCase createLocaleUseCase, CreateLocalePresenter createLocalePresenter,
                                IUpdateLocaleUseCase updateLocaleUseCase, UpdateLocalePresenter updateLocalePresenter,
                                IDeleteLocaleUseCase deleteLocaleUseCase, DeleteLocalePresenter deleteLocalePresenter)
        {
            _getAllLocalesUseCase = getAllLocalesUseCase;
            _getAllLocalesPresenter = getAllLocalesPresenter;

            _getLocaleByIdUseCase = getLocaleByIdUseCase;
            _getLocaleByIdPresenter = getLocaleByIdPresenter;

            _createLocaleUseCase = createLocaleUseCase;
            _createLocalePresenter = createLocalePresenter;

            _updateLocaleUseCase = updateLocaleUseCase;
            _updateLocalePresenter = updateLocalePresenter;

            _deleteLocaleUseCase = deleteLocaleUseCase;
            _deleteLocalePresenter = deleteLocalePresenter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.LocaleDto.GetListLocalesResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetAllLocalesAsync(string companyId, int quantity = 500, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            if (page < 0)
                page = 0;

            await _getAllLocalesUseCase.HandleAsync(new Core.Dto.LocaleDto.GetListLocalesRequest(companyId, quantity, skip, page), _getAllLocalesPresenter);

            return _getAllLocalesPresenter.ContentResult;
        }

        [HttpGet]
        [Route("{localeId}")]
        [ProducesResponseType(typeof(Core.Dto.LocaleDto.GetLocaleByIdResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetLocaleByIdAsync(string localeId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _getLocaleByIdUseCase.HandleAsync(new Core.Dto.LocaleDto.GetLocaleByIdRequest(localeId), _getLocaleByIdPresenter);

            return _getLocaleByIdPresenter.ContentResult;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.LocaleDto.CreateLocaleResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> CreateLocaleAsync([FromBody] CreateLocaleRequest createLocaleRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _createLocaleUseCase.HandleAsync(new Core.Dto.LocaleDto.CreateLocaleRequest(createLocaleRequest.Name,
                                                                                              createLocaleRequest.IsActive,
                                                                                              createLocaleRequest.CompanyId), _createLocalePresenter);

            return _createLocalePresenter.ContentResult;
        }

        [HttpPut]
        [ProducesResponseType(typeof(Core.Dto.LocaleDto.UpdateLocaleResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> UpdateLocaleAsync([FromBody] UpdateLocaleRequest updateLocaleRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _updateLocaleUseCase.HandleAsync(new Core.Dto.LocaleDto.UpdateLocaleRequest(updateLocaleRequest.LocaleId,
                                                                                              updateLocaleRequest.Name,
                                                                                              updateLocaleRequest.IsActive,
                                                                                              updateLocaleRequest.CompanyId), _updateLocalePresenter);

            return _updateLocalePresenter.ContentResult;
        }


        [HttpDelete]
        [Route("{localeId}")]
        [ProducesResponseType(typeof(Core.Dto.LocaleDto.DeleteLocaleResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> DeleteLocaleAsync(string localeId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _deleteLocaleUseCase.HandleAsync(new Core.Dto.LocaleDto.DeleteLocaleRequest(localeId), _deleteLocalePresenter);

            return _deleteLocalePresenter.ContentResult;
        }
    }
}