﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases.Item;
using OrusIOT.Presenter.ItemPresenter;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize("Bearer")]
    public class ItemController : ControllerBase
    {
        private readonly IGetAllItemUseCase _getAllItemsUseCase;
        private readonly GetAllItemsPresenter _getAllItemsPresenter;

        private readonly IGetItemByIdUseCase _getItemByIdUseCase;
        private readonly GetItemByIdPresenter _getItemByIdPresenter;

        private readonly ICreateItemUseCase _createItemUseCase;
        private readonly CreateItemPresenter _createItemPresenter;

        private readonly IUpdateItemUseCase _updateItemUseCase;
        private readonly UpdateItemPresenter _updateItemPresenter;

        private readonly IDeleteItemUseCase _deleteItemUseCase;
        private readonly DeleteItemPresenter _deleteItemPresenter;


        public ItemController(IGetAllItemUseCase getAllItemsUseCase, GetAllItemsPresenter getAllItemsPresenter,
                             IGetItemByIdUseCase getItemByIdUseCase, GetItemByIdPresenter getItemByIdPresenter,
                             ICreateItemUseCase createItemUseCase, CreateItemPresenter createItemPresenter,
                             IUpdateItemUseCase updateItemUseCase, UpdateItemPresenter updateItemPresenter,
                             IDeleteItemUseCase deleteItemUseCase, DeleteItemPresenter deleteItemPresenter)
        {
            _getAllItemsUseCase = getAllItemsUseCase;
            _getAllItemsPresenter = getAllItemsPresenter;

            _getItemByIdUseCase = getItemByIdUseCase;
            _getItemByIdPresenter = getItemByIdPresenter;

            _createItemUseCase = createItemUseCase;
            _createItemPresenter = createItemPresenter;

            _updateItemUseCase = updateItemUseCase;
            _updateItemPresenter = updateItemPresenter;

            _deleteItemUseCase = deleteItemUseCase;
            _deleteItemPresenter = deleteItemPresenter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.ItemDto.GetListItemResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetAllItemsAsync(string areaId, int quantity = 500, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            if (page < 0)
                page = 0;

            await _getAllItemsUseCase.HandleAsync(new Core.Dto.ItemDto.GetListItemRequest(areaId, quantity, skip, page), _getAllItemsPresenter);

            return _getAllItemsPresenter.ContentResult;
        }

        [HttpGet]
        [Route("{ItemId}")]
        [ProducesResponseType(typeof(Core.Dto.ItemDto.GetItemByIdResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetItemByIdAsync(string ItemId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _getItemByIdUseCase.HandleAsync(new Core.Dto.ItemDto.GetItemByIdRequest(ItemId), _getItemByIdPresenter);

            return _getItemByIdPresenter.ContentResult;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.ItemDto.CreateItemResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> CreateItemAsync([FromBody] Model.CreateItemRequest createItemRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _createItemUseCase.HandleAsync(new Core.Dto.ItemDto.CreateItemRequest(createItemRequest.Name, createItemRequest.Code,
                                                                                        createItemRequest.SerialNumber,
                                                                                        createItemRequest.IsActive, createItemRequest.CustodyId, createItemRequest.LocaleId,
                                                                                        createItemRequest.CompanyId), _createItemPresenter);

            return _createItemPresenter.ContentResult;
        }

        [HttpPut]
        [ProducesResponseType(typeof(Core.Dto.ItemDto.UpdateItemResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> UpdateItemAsync([FromBody] Model.UpdateItemByIdRequest updateItemByIdRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _updateItemUseCase.HandleAsync(new Core.Dto.ItemDto.UpdateItemRequest(updateItemByIdRequest.ItemId, updateItemByIdRequest.Name, updateItemByIdRequest.Code,
                                                                                        updateItemByIdRequest.SerialNumber,
                                                                                        updateItemByIdRequest.IsActive, updateItemByIdRequest.CustodyId, updateItemByIdRequest.LocaleId,
                                                                                        updateItemByIdRequest.CompanyId), _updateItemPresenter);

            return _updateItemPresenter.ContentResult;
        }

        [HttpDelete]
        [Route("{ItemId}")]
        [ProducesResponseType(typeof(Core.Dto.ItemDto.DeleteItemResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> DeleteItemAsync(string ItemId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _deleteItemUseCase.HandleAsync(new Core.Dto.ItemDto.DeleteItemRequest(ItemId), _deleteItemPresenter);

            return _deleteItemPresenter.ContentResult;
        }
    }
}