﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OrusIOT.Model.Beacon;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BeaconController : ControllerBase
    {

        private static string _urlBase = "http://www.mocky.io/v2/5e51c2bb2d00008200357a2f";

        //[Authorize("Bearer")]
        [HttpGet("beacon/{macAddress}")]
        public object GetBeaconInfoByMacAddress(string macAddress)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage respToken = client.PostAsync(_urlBase + $"/gw/{macAddress}/status", null).Result;

                    string conteudo = respToken.Content.ReadAsStringAsync().Result;

                    if (respToken.StatusCode == HttpStatusCode.OK)
                    {
                        List<BeaconData> beacon = JsonConvert.DeserializeObject<List<BeaconData>>(conteudo);

                        return new
                        {
                            beacon,
                            Success = true,
                            respToken.StatusCode,
                            Message = "Beacon encontrado com sucesso"
                        };
                    }
                    else
                    {
                        return new
                        {
                            Success = false,
                            respToken.StatusCode,
                            Message = "Erro ao buscar Beacon - Status - " + respToken.StatusCode
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    Success = false,
                    HttpStatusCode.InternalServerError,
                    Message = ex.Message
                };
            }
        }
    }
}