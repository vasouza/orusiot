﻿using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases.Area;
using OrusIOT.Model;
using OrusIOT.Presenter.AreaPresenter;
using System.Threading.Tasks;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize("Bearer")]
    public class AreaController : ControllerBase
    {
        private readonly IGetAllAreaUseCase _getAllAreasUseCase;
        private readonly GetAllAreasPresenter _getAllAreasPresenter;

        private readonly IGetAreaByIdUseCase _getAreaByIdUseCase;
        private readonly GetAreaByIdPresenter _getAreaByIdPresenter;

        private readonly ICreateAreaUseCase _createAreaUseCase;
        private readonly CreateAreaPresenter _createAreaPresenter;

        private readonly IUpdateAreaUseCase _updateAreaUseCase;
        private readonly UpdateAreaPresenter _updateAreaPresenter;

        private readonly IDeleteAreaUseCase _deleteAreaUseCase;
        private readonly DeleteAreaPresenter _deleteAreaPresenter;

        public AreaController(IGetAllAreaUseCase getAllAreasUseCase, GetAllAreasPresenter getAllAreasPresenter,
                                IGetAreaByIdUseCase getAreaByIdUseCase, GetAreaByIdPresenter getAreaByIdPresenter,
                                ICreateAreaUseCase createAreaUseCase, CreateAreaPresenter createAreaPresenter,
                                IUpdateAreaUseCase updateAreaUseCase, UpdateAreaPresenter updateAreaPresenter,
                                IDeleteAreaUseCase deleteAreaUseCase, DeleteAreaPresenter deleteAreaPresenter)
        {
            _getAllAreasUseCase = getAllAreasUseCase;
            _getAllAreasPresenter = getAllAreasPresenter;

            _getAreaByIdUseCase = getAreaByIdUseCase;
            _getAreaByIdPresenter = getAreaByIdPresenter;

            _createAreaUseCase = createAreaUseCase;
            _createAreaPresenter = createAreaPresenter;

            _updateAreaUseCase = updateAreaUseCase;
            _updateAreaPresenter = updateAreaPresenter;

            _deleteAreaUseCase = deleteAreaUseCase;
            _deleteAreaPresenter = deleteAreaPresenter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.AreaDto.GetListAreaResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetAllAreasAsync(string companyId, int quantity = 500, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            if (page < 0)
                page = 0;

            await _getAllAreasUseCase.HandleAsync(new Core.Dto.AreaDto.GetListAreaRequest(companyId, quantity, skip, page), _getAllAreasPresenter);

            return _getAllAreasPresenter.ContentResult;
        }

        [HttpGet]
        [Route("{areaId}")]
        [ProducesResponseType(typeof(Core.Dto.AreaDto.GetAreaByIdResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetAreaByIdAsync(string areaId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _getAreaByIdUseCase.HandleAsync(new Core.Dto.AreaDto.GetAreaByIdRequest(areaId), _getAreaByIdPresenter);

            return _getAreaByIdPresenter.ContentResult;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.AreaDto.CreateAreaResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> CreateAreaAsync([FromBody] Model.CreateAreaRequest createAreaRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _createAreaUseCase.HandleAsync(new Core.Dto.AreaDto.CreateAreaRequest(createAreaRequest.Name, createAreaRequest.Address,
                                                                                        createAreaRequest.Floor, createAreaRequest.City, createAreaRequest.Country,
                                                                                        createAreaRequest.ZipCode, createAreaRequest.LocaleId, createAreaRequest.CompanyId,
                                                                                        createAreaRequest.IsActive), _createAreaPresenter);

            return _createAreaPresenter.ContentResult;
        }

        [HttpPut]
        [ProducesResponseType(typeof(Core.Dto.AreaDto.UpdateAreaResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> UpdateAreaAsync([FromBody] UpdateAreaByIdRequest updateAreaByIdRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _updateAreaUseCase.HandleAsync(new Core.Dto.AreaDto.UpdateAreaRequest(updateAreaByIdRequest.AreaId, updateAreaByIdRequest.Name,
                                                                                        updateAreaByIdRequest.Address, updateAreaByIdRequest.Floor,
                                                                                        updateAreaByIdRequest.City, updateAreaByIdRequest.Country,
                                                                                        updateAreaByIdRequest.ZipCode, updateAreaByIdRequest.LocaleId,
                                                                                        updateAreaByIdRequest.CompanyId, updateAreaByIdRequest.IsActive), _updateAreaPresenter);

            return _updateAreaPresenter.ContentResult;
        }


        [HttpDelete]
        [Route("{localeId}")]
        [ProducesResponseType(typeof(Core.Dto.AreaDto.DeleteAreaResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> DeleteAreaAsync(string localeId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _deleteAreaUseCase.HandleAsync(new Core.Dto.AreaDto.DeleteAreaRequest(localeId), _deleteAreaPresenter);

            return _deleteAreaPresenter.ContentResult;
        }
    }
}