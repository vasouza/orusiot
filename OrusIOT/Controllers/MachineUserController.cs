﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases.MachineUser;
using OrusIOT.Presenter.MachineUserPresenter;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MachineUserController : ControllerBase
    {
        private readonly IGetAllMachineUserUseCase _getAllMachineUsersUseCase;
        private readonly GetAllMachineUsersPresenter _getAllMachineUsersPresenter;

        private readonly IGetMachineUserByIdUseCase _getMachineUserByIdUseCase;
        private readonly GetMachineUserByIdPresenter _getMachineUserByIdPresenter;

        private readonly ICreateMachineUserUseCase _createMachineUserUseCase;
        private readonly CreateMachineUserPresenter _createMachineUserPresenter;

        private readonly IUpdateMachineUserUseCase _updateMachineUserUseCase;
        private readonly UpdateMachineUserPresenter _updateMachineUserPresenter;

        private readonly IDeleteMachineUserUseCase _deleteMachineUserUseCase;
        private readonly DeleteMachineUserPresenter _deleteMachineUserPresenter;


        public MachineUserController(IGetAllMachineUserUseCase getAllMachineUsersUseCase, GetAllMachineUsersPresenter getAllMachineUsersPresenter,
                                     IGetMachineUserByIdUseCase getMachineUserByIdUseCase, GetMachineUserByIdPresenter getMachineUserByIdPresenter,
                                     ICreateMachineUserUseCase createMachineUserUseCase, CreateMachineUserPresenter createMachineUserPresenter,
                                     IUpdateMachineUserUseCase updateMachineUserUseCase, UpdateMachineUserPresenter updateMachineUserPresenter,
                                     IDeleteMachineUserUseCase deleteMachineUserUseCase, DeleteMachineUserPresenter deleteMachineUserPresenter)
        {
            _getAllMachineUsersUseCase = getAllMachineUsersUseCase;
            _getAllMachineUsersPresenter = getAllMachineUsersPresenter;

            _getMachineUserByIdUseCase = getMachineUserByIdUseCase;
            _getMachineUserByIdPresenter = getMachineUserByIdPresenter;

            _createMachineUserUseCase = createMachineUserUseCase;
            _createMachineUserPresenter = createMachineUserPresenter;

            _updateMachineUserUseCase = updateMachineUserUseCase;
            _updateMachineUserPresenter = updateMachineUserPresenter;

            _deleteMachineUserUseCase = deleteMachineUserUseCase;
            _deleteMachineUserPresenter = deleteMachineUserPresenter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.MachineUserDto.GetListMachineUserResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> GetAllMachineUsersAsync(string areaId, int quantity = 500, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            if (page < 0)
                page = 0;

            await _getAllMachineUsersUseCase.HandleAsync(new Core.Dto.MachineUserDto.GetListMachineUserRequest(areaId, quantity, skip, page), _getAllMachineUsersPresenter);

            return _getAllMachineUsersPresenter.ContentResult;
        }

        [HttpGet]
        [Route("{MachineUserId}")]
        [ProducesResponseType(typeof(Core.Dto.MachineUserDto.GetMachineUserByIdResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> GetMachineUserByIdAsync(string MachineUserId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _getMachineUserByIdUseCase.HandleAsync(new Core.Dto.MachineUserDto.GetMachineUserByIdRequest(MachineUserId), _getMachineUserByIdPresenter);

            return _getMachineUserByIdPresenter.ContentResult;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.MachineUserDto.CreateMachineUserResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> CreateMachineUserAsync([FromBody] Model.CreateMachineUserRequest createMachineUserRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _createMachineUserUseCase.HandleAsync(new Core.Dto.MachineUserDto.CreateMachineUserRequest(createMachineUserRequest.Name, createMachineUserRequest.Login, createMachineUserRequest.CompanyId), _createMachineUserPresenter);

            return _createMachineUserPresenter.ContentResult;
        }

        [HttpPut]
        [ProducesResponseType(typeof(Core.Dto.MachineUserDto.UpdateMachineUserResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> UpdateMachineUserAsync([FromBody] Model.UpdateMachineUserByIdRequest updateMachineUserByIdRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _updateMachineUserUseCase.HandleAsync(new Core.Dto.MachineUserDto.UpdateMachineUserRequest(updateMachineUserByIdRequest.MachineUserId,
                                                                                        updateMachineUserByIdRequest.Name,
                                                                                        updateMachineUserByIdRequest.Login,
                                                                                        updateMachineUserByIdRequest.CompanyId), _updateMachineUserPresenter);

            return _updateMachineUserPresenter.ContentResult;
        }

        [HttpDelete]
        [Route("{MachineUserId}")]
        [ProducesResponseType(typeof(Core.Dto.MachineUserDto.DeleteMachineUserResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> DeleteMachineUserAsync(string MachineUserId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _deleteMachineUserUseCase.HandleAsync(new Core.Dto.MachineUserDto.DeleteMachineUserRequest(MachineUserId), _deleteMachineUserPresenter);

            return _deleteMachineUserPresenter.ContentResult;
        }
    }
}
