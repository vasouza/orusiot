﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases.ReadingDevice;
using OrusIOT.Presenter.ReadingDevicePresenter;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ReadingDeviceController : ControllerBase
    {
        private readonly IGetAllReadingDeviceUseCase _getAllReadingDevicesUseCase;
        private readonly GetAllReadingDevicesPresenter _getAllReadingDevicesPresenter;

        private readonly IGetReadingDeviceByIdUseCase _getReadingDeviceByIdUseCase;
        private readonly GetReadingDeviceByIdPresenter _getReadingDeviceByIdPresenter;

        private readonly ICreateReadingDeviceUseCase _createReadingDeviceUseCase;
        private readonly CreateReadingDevicePresenter _createReadingDevicePresenter;

        private readonly IUpdateReadingDeviceUseCase _updateReadingDeviceUseCase;
        private readonly UpdateReadingDevicePresenter _updateReadingDevicePresenter;

        private readonly IDeleteReadingDeviceUseCase _deleteReadingDeviceUseCase;
        private readonly DeleteReadingDevicePresenter _deleteReadingDevicePresenter;


        public ReadingDeviceController(IGetAllReadingDeviceUseCase getAllReadingDevicesUseCase, GetAllReadingDevicesPresenter getAllReadingDevicesPresenter,
                             IGetReadingDeviceByIdUseCase getReadingDeviceByIdUseCase, GetReadingDeviceByIdPresenter getReadingDeviceByIdPresenter,
                             ICreateReadingDeviceUseCase createReadingDeviceUseCase, CreateReadingDevicePresenter createReadingDevicePresenter,
                             IUpdateReadingDeviceUseCase updateReadingDeviceUseCase, UpdateReadingDevicePresenter updateReadingDevicePresenter,
                             IDeleteReadingDeviceUseCase deleteReadingDeviceUseCase, DeleteReadingDevicePresenter deleteReadingDevicePresenter)
        {
            _getAllReadingDevicesUseCase = getAllReadingDevicesUseCase;
            _getAllReadingDevicesPresenter = getAllReadingDevicesPresenter;

            _getReadingDeviceByIdUseCase = getReadingDeviceByIdUseCase;
            _getReadingDeviceByIdPresenter = getReadingDeviceByIdPresenter;

            _createReadingDeviceUseCase = createReadingDeviceUseCase;
            _createReadingDevicePresenter = createReadingDevicePresenter;

            _updateReadingDeviceUseCase = updateReadingDeviceUseCase;
            _updateReadingDevicePresenter = updateReadingDevicePresenter;

            _deleteReadingDeviceUseCase = deleteReadingDeviceUseCase;
            _deleteReadingDevicePresenter = deleteReadingDevicePresenter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.ReadingDeviceDto.GetListReadingDeviceResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> GetAllReadingDevicesAsync(string companyId, int quantity = 500, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            if (page < 0)
                page = 0;

            await _getAllReadingDevicesUseCase.HandleAsync(new Core.Dto.ReadingDeviceDto.GetListReadingDeviceRequest(companyId, quantity, skip, page),
                                                                                                                    _getAllReadingDevicesPresenter);

            return _getAllReadingDevicesPresenter.ContentResult;
        }

        [HttpGet]
        [Route("{ReadingDeviceId}")]
        [ProducesResponseType(typeof(Core.Dto.ReadingDeviceDto.GetReadingDeviceByIdResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> GetReadingDeviceByIdAsync(string readingDeviceId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _getReadingDeviceByIdUseCase.HandleAsync(new Core.Dto.ReadingDeviceDto.GetReadingDeviceByIdRequest(readingDeviceId), _getReadingDeviceByIdPresenter);

            return _getReadingDeviceByIdPresenter.ContentResult;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.ReadingDeviceDto.CreateReadingDeviceResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> CreateReadingDeviceAsync([FromBody] Model.CreateReadingDeviceRequest createReadingDeviceRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _createReadingDeviceUseCase.HandleAsync(new Core.Dto.ReadingDeviceDto.CreateReadingDeviceRequest(createReadingDeviceRequest.Key,
                                                                                                                   createReadingDeviceRequest.Name,
                                                                                                                   createReadingDeviceRequest.Manufacturer,
                                                                                                                   createReadingDeviceRequest.Model,
                                                                                                                   createReadingDeviceRequest.SerialNumber,
                                                                                                                   createReadingDeviceRequest.IsActive,
                                                                                                                   createReadingDeviceRequest.AssociatedUserId,
                                                                                                                   createReadingDeviceRequest.SharedUserId,
                                                                                                                   createReadingDeviceRequest.MacAddress,
                                                                                                                   createReadingDeviceRequest.Latitude,
                                                                                                                   createReadingDeviceRequest.Longitude,
                                                                                                                   createReadingDeviceRequest.CompanyId), _createReadingDevicePresenter);

            return _createReadingDevicePresenter.ContentResult;
        }

        [HttpPut]
        [ProducesResponseType(typeof(Core.Dto.ReadingDeviceDto.UpdateReadingDeviceResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> UpdateReadingDeviceAsync([FromBody] Model.UpdateReadingDeviceByIdRequest updateReadingDeviceByIdRequest)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _updateReadingDeviceUseCase.HandleAsync(new Core.Dto.ReadingDeviceDto.UpdateReadingDeviceRequest(updateReadingDeviceByIdRequest.ReadDeviceId,
                                                                                                                   updateReadingDeviceByIdRequest.Key,
                                                                                                                   updateReadingDeviceByIdRequest.Name,
                                                                                                                   updateReadingDeviceByIdRequest.Manufacturer,
                                                                                                                   updateReadingDeviceByIdRequest.Model,
                                                                                                                   updateReadingDeviceByIdRequest.SerialNumber,
                                                                                                                   updateReadingDeviceByIdRequest.IsActive,
                                                                                                                   updateReadingDeviceByIdRequest.AssociatedUserId,
                                                                                                                   updateReadingDeviceByIdRequest.SharedUserId,
                                                                                                                   updateReadingDeviceByIdRequest.MacAddress,
                                                                                                                   updateReadingDeviceByIdRequest.Latitude,
                                                                                                                   updateReadingDeviceByIdRequest.Longitude,
                                                                                                                   updateReadingDeviceByIdRequest.CompanyId), _updateReadingDevicePresenter);

            return _updateReadingDevicePresenter.ContentResult;
        }

        [HttpDelete]
        [Route("{ReadingDeviceId}")]
        [ProducesResponseType(typeof(Core.Dto.ReadingDeviceDto.DeleteReadingDeviceResponse), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        //[Authorize("Bearer")]
        public async Task<ActionResult> DeleteReadingDeviceAsync(string ReadingDeviceId)
        {
            if (!ModelState.IsValid)
            { //re-render the view when validation failed.
                return BadRequest(ModelState);
            }

            await _deleteReadingDeviceUseCase.HandleAsync(new Core.Dto.ReadingDeviceDto.DeleteReadingDeviceRequest(ReadingDeviceId), _deleteReadingDevicePresenter);

            return _deleteReadingDevicePresenter.ContentResult;
        }
    }
}