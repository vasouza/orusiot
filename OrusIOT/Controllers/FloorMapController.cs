﻿using Microsoft.AspNetCore.Mvc;
using OrusIOT.Core.Interfaces.UseCases.FloorMap;
using OrusIOT.Model;
using OrusIOT.Presenter.FloorMap;
using System.Threading.Tasks;

namespace OrusIOT.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FloorMapController : ControllerBase
    {
        private readonly IGetListFloorMapUseCase _getListFloorMapUseCase;
        private readonly GetListFloorMapPresenter _getListFloorMapPresenter;

        private readonly IGetFloorMapByIdUseCase _getFloorMapByIdUseCase;
        private readonly GetFloorMapByIdPresenter _getFloorMapByIdPresenter;

        private readonly ICreateFloorMapUseCase _createFloorMapUseCase;
        private readonly CreateFloorMapPresenter _createFloorMapPresenter;

        private readonly IUpdateFloorMapUseCase _updateFloorMapUseCase;
        private readonly UpdateFloorMapPresenter _updateFloorMapPresenter;

        private readonly IDeleteFloorMapUseCase _deleteFloorMapUseCase;
        private readonly DeleteFloorMapPresenter _deleteFloorMapPresenter;

        public FloorMapController(IGetListFloorMapUseCase getListFloorMapUseCase, GetListFloorMapPresenter getListFloorMapPresenter,
                                  IGetFloorMapByIdUseCase getFloorMapByIdUseCase, GetFloorMapByIdPresenter getFloorMapByIdPresenter,
                                  ICreateFloorMapUseCase createFloorMapUseCase, CreateFloorMapPresenter createFloorMapPresenter,
                                  IUpdateFloorMapUseCase updateFloorMapUseCase, UpdateFloorMapPresenter updateFloorMapPresenter,
                                  IDeleteFloorMapUseCase deleteFloorMapUseCase, DeleteFloorMapPresenter deleteFloorMapPresenter)
        {
            _getListFloorMapUseCase = getListFloorMapUseCase;
            _getListFloorMapPresenter = getListFloorMapPresenter;

            _getFloorMapByIdUseCase = getFloorMapByIdUseCase;
            _getFloorMapByIdPresenter = getFloorMapByIdPresenter;

            _createFloorMapUseCase = createFloorMapUseCase;
            _createFloorMapPresenter = createFloorMapPresenter;

            _updateFloorMapUseCase = updateFloorMapUseCase;
            _updateFloorMapPresenter = updateFloorMapPresenter;

            _deleteFloorMapUseCase = deleteFloorMapUseCase;
            _deleteFloorMapPresenter = deleteFloorMapPresenter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Core.Dto.FloorMap.GetListFloorMapRequest), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> GetAll(string companyId, int quantity = 10, int skip = 0, int page = 0)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (page < 0)
                page = 0;

            await _getListFloorMapUseCase.HandleAsync(new Core.Dto.FloorMap.GetListFloorMapRequest(companyId,quantity, skip, page),
                                                      _getListFloorMapPresenter);
            return _getListFloorMapPresenter.ContentResult;
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(Core.Dto.FloorMap.GetFloorMapByIdRequest), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> Get(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _getFloorMapByIdUseCase.HandleAsync(new Core.Dto.FloorMap.GetFloorMapByIdRequest(id), _getFloorMapByIdPresenter);

            return _getFloorMapByIdPresenter.ContentResult;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Core.Dto.FloorMap.CreateFloorMapRequest), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> CreateFloorMapAsync([FromBody] CreateFloorMapModelRequest createFloorMapRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _createFloorMapUseCase.HandleAsync(
                new Core.Dto.FloorMap.CreateFloorMapRequest(createFloorMapRequest.Name,
                                                            createFloorMapRequest.FloorMapZones,
                                                            createFloorMapRequest.AreaId,
                                                            createFloorMapRequest.ImageBase64,
                                                            createFloorMapRequest.CompanyId),
                _createFloorMapPresenter);

            return _createFloorMapPresenter.ContentResult;
        }

        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(Core.Dto.FloorMap.UpdateFloorMapRequest), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> UpdateCustodyAsync(string id, [FromBody] UpdateFloorMapModelRequest updateFloorMapRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _updateFloorMapUseCase.HandleAsync(
                new Core.Dto.FloorMap.UpdateFloorMapRequest(id,
                                                            updateFloorMapRequest.Name,
                                                            updateFloorMapRequest.FloorMapZones,
                                                            updateFloorMapRequest.AreaId,
                                                            updateFloorMapRequest.Image,
                                                            updateFloorMapRequest.CompanyId),
                _updateFloorMapPresenter);

            return _updateFloorMapPresenter.ContentResult;
        }

        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(Core.Dto.FloorMap.DeleteFloorMapRequest), 200)]
        [ProducesResponseType(typeof(void), 400)]
        [ProducesResponseType(typeof(void), 401)]
        [ProducesResponseType(typeof(void), 403)]
        public async Task<ActionResult> DeleteFloorMapAsync(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _deleteFloorMapUseCase.HandleAsync(new Core.Dto.FloorMap.DeleteFloorMapRequest(id), _deleteFloorMapPresenter);

            return _deleteFloorMapPresenter.ContentResult;
        }
    }
}