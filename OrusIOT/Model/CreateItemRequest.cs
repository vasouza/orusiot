﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model
{
    public class CreateItemRequest
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string SerialNumber { get; set; }
        public bool IsActive { get; set; }
        public string CustodyId { get; set; }
        public string CompanyId { get; set; }
        public string LocaleId { get; set; }
    }
}
