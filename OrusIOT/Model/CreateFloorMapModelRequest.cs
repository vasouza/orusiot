﻿namespace OrusIOT.Model
{
    public class CreateFloorMapModelRequest
    {
        public string Name { get; set; }
        public string FloorMapZones { get; set; }
        public string AreaId { get; set; }
        public string ImageBase64 { get; set; }
        public string CompanyId { get; set; }
    }
}
