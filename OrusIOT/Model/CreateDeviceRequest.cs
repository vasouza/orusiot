﻿namespace OrusIOT.Model
{
    public class CreateDeviceRequest
    {
        public string Name { get; set; }
        public string MacAddress { get; set; }
    }
}
