﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model
{
    public class CreateUserRequest
    {
        public string UserId { get; set; }
        public string Login { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }
        public string DepartmentId { get; set; }
        public string CompanyId { get; set; }
    }
}
