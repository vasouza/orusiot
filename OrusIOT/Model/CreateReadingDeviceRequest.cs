﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model
{
    public class CreateReadingDeviceRequest
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public bool IsActive { get; set; }
        public string AssociatedUserId { get; set; }
        public string SharedUserId { get; set; }
        public string MacAddress { get; set; }
        public int Latitude { get; set; }
        public int Longitude { get; set; }
        public string CompanyId { get; set; }
    }
}
