﻿namespace OrusIOT.Model
{
    public class UpdateDeviceRequest
    {
        public string Name { get; set; }
        public string MacAddress { get; set; }
    }
}
