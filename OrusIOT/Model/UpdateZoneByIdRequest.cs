﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model
{
    public class UpdateZoneByIdRequest
    {
        public string ZoneId { get; set; }
        public string Name { get; set; }
        public string AreaId { get; set; }
        public bool IsActive { get; set; }
    }
}
