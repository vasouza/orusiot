﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model.Beacon
{
    public class BeaconData
    {
        public object Timestamp { get; set; }
        public string Type { get; set; }
        public string Mac { get; set; }
        public string BleName { get; set; }
        public string IbeaconUuid { get; set; }
        public int IbeaconMajor { get; set; }
        public int IbeaconMinor { get; set; }
        public int IbeaconTxPower { get; set; }
        public int Rssi { get; set; }
        public int Battery { get; set; }
        public double? Temperature { get; set; }
        public double? Humidity { get; set; }
        public string RawData { get; set; }
    }
}
