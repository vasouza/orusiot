﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model
{
    public class UpdateAreaByIdRequest
    {
        public string AreaId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Floor { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string LocaleId { get; set; }
        public string CompanyId { get; set; }
        public bool IsActive { get; set; }
    }
}
