﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model
{
    public class CreateMachineUserRequest
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string CompanyId { get; set; }
    }
}
