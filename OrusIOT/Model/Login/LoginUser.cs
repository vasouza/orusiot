﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model.Login
{
    public class LoginUser
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
