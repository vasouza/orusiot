﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model
{
    public class UpdateLocaleRequest
    {
        public string LocaleId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string CompanyId { get; set; }
    }
}
