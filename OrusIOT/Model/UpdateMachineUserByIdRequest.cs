﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model
{
    public class UpdateMachineUserByIdRequest
    {
        public string MachineUserId { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string CompanyId { get; set; }
    }
}
