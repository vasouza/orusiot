﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Model
{
    public class CreateZoneRequest
    {
        public string Name { get; set; }
        public string AreaId { get; set; }
        public bool IsActive { get; set; }
    }
}
