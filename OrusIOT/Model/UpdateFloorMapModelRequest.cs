﻿namespace OrusIOT.Model
{
    public class UpdateFloorMapModelRequest
    {
        public string Name { get; set; }
        public string FloorMapZones { get; set; }
        public string AreaId { get; set; }
        public string Image { get; set; }
        public string CompanyId { get; set; }
    }
}
