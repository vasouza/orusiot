﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using OrusIOT.Core;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Core.Interfaces.UseCases;
using OrusIOT.Core.Interfaces.UseCases.Area;
using OrusIOT.Core.Interfaces.UseCases.Device;
using OrusIOT.Core.Interfaces.UseCases.Item;
using OrusIOT.Core.Interfaces.UseCases.FloorMap;
using OrusIOT.Core.Interfaces.UseCases.Locale;
using OrusIOT.Core.Interfaces.UseCases.Login;
using OrusIOT.Core.Interfaces.UseCases.MachineUser;
using OrusIOT.Core.Interfaces.UseCases.ReadingDevice;
using OrusIOT.Core.Interfaces.UseCases.User;
using OrusIOT.Core.Interfaces.UseCases.Zone;
using OrusIOT.Core.UseCases;
using OrusIOT.Core.UseCases.AreaUseCases;
using OrusIOT.Core.UseCases.CustodyUseCases;
using OrusIOT.Core.UseCases.DeviceUseCases;
using OrusIOT.Core.UseCases.ItemUseCases;
using OrusIOT.Core.UseCases.FloorMapUseCases;
using OrusIOT.Core.UseCases.LocaleUseCases;
using OrusIOT.Core.UseCases.LoginUseCases;
using OrusIOT.Core.UseCases.MachineUserUseCases;
using OrusIOT.Core.UseCases.ReadingDeviceUseCases;
using OrusIOT.Core.UseCases.UserUseCases;
using OrusIOT.Core.UseCases.ZoneUseCases;
using OrusIOT.Infrastructure;
using OrusIOT.Infrastructure.EntityFramework.Repositories;
using OrusIOT.Presenter;
using OrusIOT.Presenter.AreaPresenter;
using OrusIOT.Presenter.CustodyPresenter;
using OrusIOT.Presenter.Device;
using OrusIOT.Presenter.ItemPresenter;
using OrusIOT.Presenter.FloorMap;
using OrusIOT.Presenter.LocalePresenter;
using OrusIOT.Presenter.LoginPresenter;
using OrusIOT.Presenter.MachineUserPresenter;
using OrusIOT.Presenter.ReadingDevicePresenter;
using OrusIOT.Presenter.UserPresenter;
using OrusIOT.Presenter.ZonePresenter;
using System;

namespace OrusIOT
{
    public class Startup
    {
        private IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {
            _env = env;

            Console.WriteLine("Environment Name: " + env.EnvironmentName.ToLower());

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName.ToLower()}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Default")));

            services.AddMvc(options => options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            // Registro do gerador do swagger, definindo um ou mais documentos.
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Orus IoT",
                    Version = "v1",
                    Description = "Orus IoT backend API"
                });
            });

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICustodyRepository, CustodyRepository>();
            services.AddScoped<IDeviceRepository, DeviceRepository>();
            services.AddScoped<ILocaleRepository, LocaleRepository>();
            services.AddScoped<IAreaRepository, AreaRepository>();
            services.AddScoped<IZoneRepository, ZoneRepository>();
            services.AddScoped<IMachineUserRepository, MachineUserRepository>();
            services.AddScoped<IReadingDeviceRepository, ReadingDeviceRepository>();
            services.AddScoped<IItemRepository, ItemRepository>();
            services.AddScoped<IFloorMapRepository, FloorMapRepository>();



            #region Custodies
            services.AddScoped<GetAllCustodiesPresenter>();
            services.AddScoped<IGetAllCustodiesUseCase, GetAllCustodiesUseCase>();

            services.AddScoped<GetCustodyPresenter>();
            services.AddScoped<IGetCustodyUseCase, GetCustodyUseCase>();

            services.AddScoped<CreateCustodyPresenter>();
            services.AddScoped<ICreatedCustodyUseCase, CreatedCustodyUseCase>();

            services.AddSingleton<UpdateCustodyPresenter>();
            services.AddTransient<IUpdateCustodyUseCase, UpdateCustodyUseCase>();

            services.AddScoped<DeleteCustodyPresenter>();
            services.AddScoped<IDeleteCustodyUseCase, DeleteCustodyUseCase>();
            #endregion

            #region Devices
            services.AddScoped<GetAllDevicesPresenter>();
            services.AddScoped<IGetAllDevicesUseCase, GetAllDevicesUseCase>();

            services.AddScoped<GetDeviceByIdPresenter>();
            services.AddScoped<IGetDeviceByIdUseCase, GetDeviceByIdUseCase>();

            services.AddScoped<CreateDevicePresenter>();
            services.AddScoped<ICreateDeviceUseCase, CreateDeviceUseCase>();

            services.AddScoped<UpdateDevicePresenter>();
            services.AddScoped<IUpdateDeviceUseCase, UpdateDeviceUseCase>();

            services.AddScoped<DeleteDevicePresenter>();
            services.AddScoped<IDeleteDeviceUseCase, DeleteDeviceUseCase>();
            #endregion

            #region Locale
            services.AddScoped<GetAllLocalesPresenter>();
            services.AddScoped<IGetAllLocalesUseCase, GetAllLocalesUseCase>();

            services.AddScoped<GetLocaleByIdPresenter>();
            services.AddScoped<IGetLocaleByIdUseCase, GetLocaleByIdUseCase>();

            services.AddScoped<CreateLocalePresenter>();
            services.AddScoped<ICreateLocaleUseCase, CreateLocaleUseCase>();

            services.AddScoped<UpdateLocalePresenter>();
            services.AddScoped<IUpdateLocaleUseCase, UpdateLocaleUseCase>();

            services.AddScoped<DeleteLocalePresenter>();
            services.AddScoped<IDeleteLocaleUseCase, DeleteLocaleUseCase>();

            services.AddScoped<GetListFloorMapPresenter>();
            services.AddScoped<IGetListFloorMapUseCase, GetListFloorMapUseCase>();

            services.AddScoped<GetFloorMapByIdPresenter>();
            services.AddScoped<IGetFloorMapByIdUseCase, GetFloorMapByIdUseCase>();

            services.AddScoped<CreateFloorMapPresenter>();
            services.AddScoped<ICreateFloorMapUseCase, CreateFloorMapUseCase>();

            services.AddScoped<UpdateFloorMapPresenter>();
            services.AddScoped<IUpdateFloorMapUseCase, UpdateFloorMapUseCase>();

            services.AddScoped<DeleteFloorMapPresenter>();
            services.AddScoped<IDeleteFloorMapUseCase, DeleteFloorMapUseCase>();
            #endregion

            #region Areas
            services.AddScoped<GetAllAreasPresenter>();
            services.AddScoped<IGetAllAreaUseCase, GetAllAreaUseCase>();

            services.AddScoped<GetAreaByIdPresenter>();
            services.AddScoped<IGetAreaByIdUseCase, GetAreaByIdUseCase>();

            services.AddScoped<CreateAreaPresenter>();
            services.AddScoped<ICreateAreaUseCase, CreateAreaUseCase>();

            services.AddScoped<UpdateAreaPresenter>();
            services.AddScoped<IUpdateAreaUseCase, UpdateAreaUseCase>();

            services.AddScoped<DeleteAreaPresenter>();
            services.AddScoped<IDeleteAreaUseCase, DeleteAreaUseCase>();
            #endregion

            #region Zones
            services.AddScoped<GetAllZonesPresenter>();
            services.AddScoped<IGetAllZoneUseCase, GetAllZoneUseCase>();

            services.AddScoped<GetZoneByIdPresenter>();
            services.AddScoped<IGetZoneByIdUseCase, GetZoneByIdUseCase>();

            services.AddScoped<CreateZonePresenter>();
            services.AddScoped<ICreateZoneUseCase, CreateZoneUseCase>();

            services.AddScoped<UpdateZonePresenter>();
            services.AddScoped<IUpdateZoneUseCase, UpdateZoneUseCase>();

            services.AddScoped<DeleteZonePresenter>();
            services.AddScoped<IDeleteZoneUseCase, DeleteZoneUseCase>();
            #endregion

            #region MachineUsers
            services.AddScoped<GetAllMachineUsersPresenter>();
            services.AddScoped<IGetAllMachineUserUseCase, GetAllMachineUserUseCase>();

            services.AddScoped<GetMachineUserByIdPresenter>();
            services.AddScoped<IGetMachineUserByIdUseCase, GetMachineUserByIdUseCase>();

            services.AddScoped<CreateMachineUserPresenter>();
            services.AddScoped<ICreateMachineUserUseCase, CreateMachineUserUseCase>();

            services.AddScoped<UpdateMachineUserPresenter>();
            services.AddScoped<IUpdateMachineUserUseCase, UpdateMachineUserUseCase>();

            services.AddScoped<DeleteMachineUserPresenter>();
            services.AddScoped<IDeleteMachineUserUseCase, DeleteMachineUserUseCase>();
            #endregion

            #region Reading Devices
            services.AddScoped<GetAllReadingDevicesPresenter>();
            services.AddScoped<IGetAllReadingDeviceUseCase, GetAllReadingDeviceUseCase>();

            services.AddScoped<GetReadingDeviceByIdPresenter>();
            services.AddScoped<IGetReadingDeviceByIdUseCase, GetReadingDeviceByIdUseCase>();

            services.AddScoped<CreateReadingDevicePresenter>();
            services.AddScoped<ICreateReadingDeviceUseCase, CreateReadingDeviceUseCase>();

            services.AddScoped<UpdateReadingDevicePresenter>();
            services.AddScoped<IUpdateReadingDeviceUseCase, UpdateReadingDeviceUseCase>();

            services.AddScoped<DeleteReadingDevicePresenter>();
            services.AddScoped<IDeleteReadingDeviceUseCase, DeleteReadingDeviceUseCase>();
            #endregion

            #region Item
            services.AddScoped<GetAllItemsPresenter>();
            services.AddScoped<IGetAllItemUseCase, GetAllItemUseCase>();

            services.AddScoped<GetItemByIdPresenter>();
            services.AddScoped<IGetItemByIdUseCase, GetItemByIdUseCase>();

            services.AddScoped<CreateItemPresenter>();
            services.AddScoped<ICreateItemUseCase, CreateItemUseCase>();

            services.AddScoped<UpdateItemPresenter>();
            services.AddScoped<IUpdateItemUseCase, UpdateItemUseCase>();

            services.AddScoped<DeleteItemPresenter>();
            services.AddScoped<IDeleteItemUseCase, DeleteItemUseCase>();
            #endregion

            #region User
            services.AddScoped<GetAllUserPresenter>();
            services.AddScoped<IGetAllUserUseCase, GetAllUserUseCase>();

            services.AddScoped<GetUserByIdPresenter>();
            services.AddScoped<IGetUserByIdUseCase, GetUserByIdUseCase>();

            services.AddScoped<CreateUserPresenter>();
            services.AddScoped<ICreateUserUseCase, CreateUserUseCase>();

            services.AddScoped<UpdateUserPresenter>();
            services.AddScoped<IUpdateUserUseCase, UpdateUserUseCase>();

            services.AddScoped<DeleteUserPresenter>();
            services.AddScoped<IDeleteUserUseCase, DeleteUserUseCase>();
            #endregion

            #region Login
            services.AddScoped<LoginPresenter>();
            services.AddScoped<ILoginUseCase, LoginUseCase>();
            #endregion

            services.AddTransient<LoginRepository>();
            services.AddScoped<ILoginRepository, LoginRepository>();

            var signingConfigurations = new SigningConfigurations();
            services.AddSingleton(signingConfigurations);

            var tokenConfigurations = new TokenConfigurations();
            new ConfigureFromConfigurationOptions<TokenConfigurations>(
                Configuration.GetSection("TokenConfigurations"))
                    .Configure(tokenConfigurations);
            services.AddSingleton(tokenConfigurations);

            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(bearerOptions =>
            {
                var paramsValidation = bearerOptions.TokenValidationParameters;
                paramsValidation.IssuerSigningKey = signingConfigurations.Key;
                paramsValidation.ValidAudience = tokenConfigurations.Audience;
                paramsValidation.ValidIssuer = tokenConfigurations.Issuer;

                // Valida a assinatura de um token recebido
                paramsValidation.ValidateIssuerSigningKey = true;

                // Verifica se um token recebido ainda é válido
                paramsValidation.ValidateLifetime = true;

                // Tempo de tolerância para a expiração de um token (utilizado
                // caso haja problemas de sincronismo de horário entre diferentes
                // computadores envolvidos no processo de comunicação)
                paramsValidation.ClockSkew = TimeSpan.Zero;
            });

            // Ativa o uso do token como forma de autorizar o acesso
            // a recursos deste projeto
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Enable swagger json.
            app.UseSwagger();
            // Enable swagger UI.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "OrusIOT v1");
            });

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
