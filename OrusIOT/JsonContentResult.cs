﻿using Microsoft.AspNetCore.Mvc;

namespace OrusIOT
{
    public sealed class JsonContentResult : ContentResult
    {
        public JsonContentResult()
        {
            ContentType = "application/json";
        }
    }
}
