﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.FloorMap
{
    public class CreateFloorMapPresenter : IOutputPort<CreateFloorMapResponse>
    {
        public JsonContentResult ContentResult { get; }

        public CreateFloorMapPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(CreateFloorMapResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma planta encontrada")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NoContent;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
