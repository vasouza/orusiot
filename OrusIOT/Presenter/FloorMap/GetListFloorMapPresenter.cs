﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.FloorMap
{
    public class GetListFloorMapPresenter : IOutputPort<GetListFloorMapResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetListFloorMapPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetListFloorMapResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma planta encontrada.")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
