﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.Device
{
    public class UpdateDevicePresenter : IOutputPort<UpdateDeviceResponse>
    {
        public JsonContentResult ContentResult { get; }

        public UpdateDevicePresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(UpdateDeviceResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum aparelho encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NoContent;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
