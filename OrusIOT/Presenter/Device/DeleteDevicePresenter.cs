﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.Device
{
    public class DeleteDevicePresenter : IOutputPort<DeleteDeviceResponse>
    {
        public JsonContentResult ContentResult { get; }

        public DeleteDevicePresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(DeleteDeviceResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum aparelho encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NoContent;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
