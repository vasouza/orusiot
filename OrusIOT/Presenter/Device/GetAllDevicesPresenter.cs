﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter
{
    public class GetAllDevicesPresenter : IOutputPort<GetDeviceResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAllDevicesPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetDeviceResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum aparelho encontrado.")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
