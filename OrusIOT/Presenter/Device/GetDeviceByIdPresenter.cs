﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.Device
{
    public class GetDeviceByIdPresenter : IOutputPort<GetDeviceByIdResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetDeviceByIdPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetDeviceByIdResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum aparelho encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NoContent;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
