﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.ReadingDevicePresenter
{
    public class GetReadingDeviceByIdPresenter : IOutputPort<GetReadingDeviceByIdResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetReadingDeviceByIdPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetReadingDeviceByIdResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum dispositivo de leitura encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
