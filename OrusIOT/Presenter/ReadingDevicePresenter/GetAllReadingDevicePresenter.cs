﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.ReadingDevicePresenter
{
    public class GetAllReadingDevicesPresenter : IOutputPort<GetListReadingDeviceResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAllReadingDevicesPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetListReadingDeviceResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum dispositivo de leitura encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
