﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.ReadingDevicePresenter
{
    public class UpdateReadingDevicePresenter : IOutputPort<UpdateReadingDeviceResponse>
    {
        public JsonContentResult ContentResult { get; }

        public UpdateReadingDevicePresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(UpdateReadingDeviceResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum dispositivo de leitura encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
