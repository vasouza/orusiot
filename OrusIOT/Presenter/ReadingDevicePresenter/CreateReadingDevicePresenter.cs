﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.ReadingDevicePresenter
{
    public class CreateReadingDevicePresenter : IOutputPort<CreateReadingDeviceResponse>
    {
        public JsonContentResult ContentResult { get; }

        public CreateReadingDevicePresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(CreateReadingDeviceResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
