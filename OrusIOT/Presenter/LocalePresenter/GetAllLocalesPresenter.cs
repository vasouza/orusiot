﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.LocalePresenter
{
    public class GetAllLocalesPresenter : IOutputPort<GetListLocalesResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAllLocalesPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetListLocalesResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma localidade encontrada")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
