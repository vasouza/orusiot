﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.LocalePresenter
{
    public class CreateLocalePresenter : IOutputPort<CreateLocaleResponse>
    {
        public JsonContentResult ContentResult { get; }

        public CreateLocalePresenter()
        {
            ContentResult = new JsonContentResult();
        }


        public void Handle(CreateLocaleResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
