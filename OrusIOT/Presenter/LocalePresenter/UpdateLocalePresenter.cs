﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.LocalePresenter
{
    public class UpdateLocalePresenter : IOutputPort<UpdateLocaleResponse>
    {
        public JsonContentResult ContentResult { get; }

        public UpdateLocalePresenter()
        {
            ContentResult = new JsonContentResult();
        } 

        public void Handle(UpdateLocaleResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma localidade encontrada")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
