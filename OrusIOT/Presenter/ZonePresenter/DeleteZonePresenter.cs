﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ZoneDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.ZonePresenter
{
    public class DeleteZonePresenter : IOutputPort<DeleteZoneResponse>
    {
        public JsonContentResult ContentResult { get; }

        public DeleteZonePresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(DeleteZoneResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma zona encontrada")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
