﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ZoneDto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.ZonePresenter
{
    public class GetAllZonesPresenter : IOutputPort<GetListZoneResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAllZonesPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetListZoneResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma zona encontrada")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
