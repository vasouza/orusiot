﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ZoneDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.ZonePresenter
{
    public class CreateZonePresenter : IOutputPort<CreateZoneResponse>
    {
        public JsonContentResult ContentResult { get; }

        public CreateZonePresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(CreateZoneResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
