﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.CustodyDto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.CustodyPresenter
{
    public class CreateCustodyPresenter : IOutputPort<CreateCustodyResponse>
    {
        public JsonContentResult ContentResult { get; }

        public CreateCustodyPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(CreateCustodyResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
