﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.CustodyDto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.CustodyPresenter
{
    public class GetAllCustodiesPresenter : IOutputPort<GetListCustodiesResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAllCustodiesPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetListCustodiesResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma custódia encontrada")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
