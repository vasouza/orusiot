﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ItemDto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.ItemPresenter
{
    public class GetAllItemsPresenter : IOutputPort<GetListItemResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAllItemsPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetListItemResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum Item encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
