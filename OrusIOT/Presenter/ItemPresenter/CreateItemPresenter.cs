﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ItemDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.ItemPresenter
{
    public class CreateItemPresenter : IOutputPort<CreateItemResponse>
    {
        public JsonContentResult ContentResult { get; }

        public CreateItemPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(CreateItemResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
