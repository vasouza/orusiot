﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.ItemDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.ItemPresenter
{
    public class DeleteItemPresenter : IOutputPort<DeleteItemResponse>
    {
        public JsonContentResult ContentResult { get; }

        public DeleteItemPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(DeleteItemResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum Item encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
