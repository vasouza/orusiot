﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.AreaDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.AreaPresenter
{
    public class CreateAreaPresenter : IOutputPort<CreateAreaResponse>
    {
        public JsonContentResult ContentResult { get; }

        public CreateAreaPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(CreateAreaResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
