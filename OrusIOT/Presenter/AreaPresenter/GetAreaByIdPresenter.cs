﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.AreaDto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.AreaPresenter
{
    public class GetAreaByIdPresenter : IOutputPort<GetAreaByIdResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAreaByIdPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetAreaByIdResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma área encontrada")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
