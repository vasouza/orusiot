﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.AreaDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.AreaPresenter
{
    public class GetAllAreasPresenter :  IOutputPort<GetListAreaResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAllAreasPresenter()
        {
            ContentResult = new JsonContentResult();
        }


        public void Handle(GetListAreaResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma área encontrada")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
