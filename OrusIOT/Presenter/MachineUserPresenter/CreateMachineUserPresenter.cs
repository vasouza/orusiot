﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.MachineUserDto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.MachineUserPresenter
{
    public class CreateMachineUserPresenter : IOutputPort<CreateMachineUserResponse>
    {
        public JsonContentResult ContentResult { get; }

        public CreateMachineUserPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(CreateMachineUserResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
