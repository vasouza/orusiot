﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.MachineUserDto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.MachineUserPresenter
{
    public class DeleteMachineUserPresenter : IOutputPort<DeleteMachineUserResponse>
    {
        public JsonContentResult ContentResult { get; }

        public DeleteMachineUserPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(DeleteMachineUserResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhuma máquina de usuário encontrada")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
