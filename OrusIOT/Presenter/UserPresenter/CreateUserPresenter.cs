﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.UserDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.UserPresenter
{
    public class CreateUserPresenter : IOutputPort<CreateUserResponse>
    {
        public JsonContentResult ContentResult { get; }

        public CreateUserPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(CreateUserResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
