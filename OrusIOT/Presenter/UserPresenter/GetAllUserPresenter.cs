﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto;
using OrusIOT.Core.Interfaces;
using System.Net;

namespace OrusIOT.Presenter.UserPresenter
{
    public class GetAllUserPresenter : IOutputPort<GetListUserResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAllUserPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetListUserResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum usuário encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
