﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.UserDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.UserPresenter
{
    public class UpdateUserPresenter : IOutputPort<UpdateUserResponse>
    {
        public JsonContentResult ContentResult { get; }

        public UpdateUserPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(UpdateUserResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum usuário encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NotFound;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
