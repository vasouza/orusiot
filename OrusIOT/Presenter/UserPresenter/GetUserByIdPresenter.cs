﻿using Newtonsoft.Json;
using OrusIOT.Core.Dto.UserDto;
using OrusIOT.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OrusIOT.Presenter.UserPresenter
{
    public class GetUserByIdPresenter : IOutputPort<GetUserByIdResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetUserByIdPresenter()
        {
            ContentResult = new JsonContentResult();
        }

        public void Handle(GetUserByIdResponse response)
        {
            if (response.Success && string.IsNullOrEmpty(response.Message))
            {
                ContentResult.StatusCode = (int)HttpStatusCode.OK;
            }
            else if (response.Success && !string.IsNullOrEmpty(response.Message) && response.Message == "Nenhum usuário encontrado")
            {
                ContentResult.StatusCode = (int)HttpStatusCode.NoContent;
            }
            else
            {
                ContentResult.StatusCode = (int)HttpStatusCode.BadRequest;
            }

            ContentResult.Content = JsonConvert.SerializeObject(response, Formatting.Indented);
        }
    }
}
