﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto.LoginDto;
using OrusIOT.Core.Interfaces.Repositories;
using OrusIOT.Infrastructure.EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure
{
    public class LoginRepository : ILoginRepository
    {
        private IConfiguration _configuration;

        public LoginRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<LoginResponse> FindUser(LoginRequest login)
        {
            try
            {
                Task<LoginResponse> loginResponse = null;
                using (SqlConnection conexao = new SqlConnection(
                        _configuration.GetConnectionString("Default")))
                {
                    var usuarioBase = conexao.QueryFirstOrDefault<UserLogin>(
                                                "SELECT Login, Password " +
                                                "FROM [dbo].[User] " +
                                                "WHERE Login = @Login", new { Login = login.Login });

                    if (usuarioBase != null && !string.IsNullOrEmpty(usuarioBase.Login))
                    {

                        bool credenciaisValidas = false;
                        if (usuarioBase != null && !String.IsNullOrWhiteSpace(usuarioBase.Login))
                        {
                            credenciaisValidas = (usuarioBase != null &&
                                login.Login == usuarioBase.Login &&
                                login.Password == usuarioBase.Password);

                            loginResponse = Task.FromResult(new LoginResponse(credenciaisValidas, null, null, null, true)); ;
                        }
                        else
                        {
                            loginResponse = Task.FromResult(new LoginResponse(false, null, null, null, true, "Usuário ou Senha incorretos"));
                        }
                    }
                }

                return await loginResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new LoginResponse(errors, false, null);
            }
        }
    }

    public class UserLogin
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
