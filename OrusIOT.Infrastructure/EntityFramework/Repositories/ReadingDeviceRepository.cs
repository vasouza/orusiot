﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto.ReadingDeviceDto;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure.EntityFramework.Repositories
{
    public class ReadingDeviceRepository : IReadingDeviceRepository
    {
        private DatabaseContext DbContext { get; }

        public ReadingDeviceRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<CreateReadingDeviceResponse> CreateReadingDevice(CreateReadingDeviceRequest createReadingDeviceRequest)
        {
            Task<CreateReadingDeviceResponse> response = null;

            try
            {
                var readingDevice = await DbContext.ReadingDevice.AddAsync(new Entities.ReadingDevice()
                {
                    ReadingDeviceId = Convert.ToString(Guid.NewGuid()),
                    Key = createReadingDeviceRequest.Key,
                    Name = createReadingDeviceRequest.Name,
                    Manufacturer = createReadingDeviceRequest.Manufacturer,
                    Model = createReadingDeviceRequest.Model,
                    SerialNumber = createReadingDeviceRequest.SerialNumber,
                    IsActive = createReadingDeviceRequest.IsActive,
                    AssociatedUserId = createReadingDeviceRequest.AssociatedUserId,
                    SharedUserId = createReadingDeviceRequest.SharedUserId,
                    MacAddress = createReadingDeviceRequest.MacAddress,
                    Latitude = createReadingDeviceRequest.Latitude,
                    Longitude = createReadingDeviceRequest.Longitude,
                    CompanyId = createReadingDeviceRequest.CompanyId
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateReadingDeviceResponse(readingDevice.Entity.ReadingDeviceId,
                                                                           readingDevice.Entity.Key,
                                                                           readingDevice.Entity.Name,
                                                                           readingDevice.Entity.Manufacturer,
                                                                           readingDevice.Entity.Model,
                                                                           readingDevice.Entity.SerialNumber,
                                                                           readingDevice.Entity.IsActive,
                                                                           readingDevice.Entity.AssociatedUserId,
                                                                           readingDevice.Entity.SharedUserId,
                                                                           readingDevice.Entity.MacAddress,
                                                                           readingDevice.Entity.Latitude,
                                                                           readingDevice.Entity.Longitude,
                                                                           readingDevice.Entity.CompanyId,
                                                                           true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateReadingDeviceResponse(errors, false, null); ;
            }
        }

        public async Task<DeleteReadingDeviceResponse> DeleteReadingDeviceById(DeleteReadingDeviceRequest deleteReadingDeviceRequest)
        {
            Task<DeleteReadingDeviceResponse> response = null;
            try
            {

                var readingDevice = await DbContext.ReadingDevice.FirstOrDefaultAsync(x => x.ReadingDeviceId == deleteReadingDeviceRequest.ReadingDeviceId);

                if (readingDevice != null && readingDevice.ReadingDeviceId != null)
                {
                    DbContext.Remove(readingDevice);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteReadingDeviceResponse(deleteReadingDeviceRequest.ReadingDeviceId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteReadingDeviceResponse(null, true, "Nenhum dispositivo de leitura encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteReadingDeviceResponse(errors, ex.Message, false); ;
            }
        }

        public async Task<GetListReadingDeviceResponse> GetListReadingDevices(GetListReadingDeviceRequest getListReadingDeviceRequest)
        {
            Task<GetListReadingDeviceResponse> response = null;

            try
            {
                var ReadingDeviceList = DbContext.ReadingDevice.Where(x => x.CompanyId == getListReadingDeviceRequest.CompanyId).Skip(getListReadingDeviceRequest.Skip).Take(getListReadingDeviceRequest.Quantity).ToList();

                if (ReadingDeviceList != null && ReadingDeviceList.Any())
                {
                    var ReadingDeviceEntity = new List<OrusIOT.Core.Entities.ReadingDevice>();

                    foreach (var item in ReadingDeviceList)
                    {
                        var ReadingDevice = new OrusIOT.Core.Entities.ReadingDevice(item.ReadingDeviceId,
                                                                                    item.Key,
                                                                                    item.Name,
                                                                                    item.Manufacturer,
                                                                                    item.Model,
                                                                                    item.SerialNumber,
                                                                                    item.IsActive,
                                                                                    item.AssociatedUserId,
                                                                                    item.SharedUserId,
                                                                                    item.MacAddress,
                                                                                    item.Latitude,
                                                                                    item.Longitude,
                                                                                    item.CompanyId);

                        ReadingDeviceEntity.Add(ReadingDevice);
                    }

                    response = Task.FromResult(new GetListReadingDeviceResponse(ReadingDeviceEntity, ReadingDeviceList.Count(), getListReadingDeviceRequest.Page, true));
                }
                else
                {
                    response = Task.FromResult(new GetListReadingDeviceResponse(null, true, "Nenhum dispositivo de leitura encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetListReadingDeviceResponse(errors, false, null); ;
            }
        }

        public async Task<GetReadingDeviceByIdResponse> GetReadingDeviceById(GetReadingDeviceByIdRequest getReadingDeviceByIdRequest)
        {
            Task<GetReadingDeviceByIdResponse> response = null;

            try
            {
                var readingDevice = await DbContext.ReadingDevice.FirstOrDefaultAsync(x => x.ReadingDeviceId == getReadingDeviceByIdRequest.ReadingDeviceId);

                if (readingDevice != null && readingDevice.ReadingDeviceId != null)
                {
                    response = Task.FromResult(new GetReadingDeviceByIdResponse(readingDevice.ReadingDeviceId,
                                                                           readingDevice.Key,
                                                                           readingDevice.Name,
                                                                           readingDevice.Manufacturer,
                                                                           readingDevice.Model,
                                                                           readingDevice.SerialNumber,
                                                                           readingDevice.IsActive,
                                                                           readingDevice.AssociatedUserId,
                                                                           readingDevice.SharedUserId,
                                                                           readingDevice.MacAddress,
                                                                           readingDevice.Latitude,
                                                                           readingDevice.Longitude,
                                                                           readingDevice.CompanyId,
                                                                           true));
                }
                else
                {
                    response = Task.FromResult(new GetReadingDeviceByIdResponse(null, true, "Nenhum dispositivo de leitura encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetReadingDeviceByIdResponse(errors, false, null); ;
            }
        }

        public async Task<UpdateReadingDeviceResponse> UpdateReadingDevice(UpdateReadingDeviceRequest updateReadingDeviceRequest)
        {
            Task<UpdateReadingDeviceResponse> response = null;
            try
            {

                var readingDevice = await DbContext.ReadingDevice.FirstOrDefaultAsync(x => x.ReadingDeviceId == updateReadingDeviceRequest.ReadingDeviceId);

                if (readingDevice != null && readingDevice.ReadingDeviceId != null)
                {
                    readingDevice.Key = updateReadingDeviceRequest.Key;
                    readingDevice.Name = updateReadingDeviceRequest.Name;
                    readingDevice.Manufacturer = updateReadingDeviceRequest.Manufacturer;
                    readingDevice.Model = updateReadingDeviceRequest.Model;
                    readingDevice.SerialNumber = updateReadingDeviceRequest.SerialNumber;
                    readingDevice.IsActive = updateReadingDeviceRequest.IsActive;
                    readingDevice.AssociatedUserId = updateReadingDeviceRequest.AssociatedUserId;
                    readingDevice.SharedUserId = updateReadingDeviceRequest.SharedUserId;
                    readingDevice.MacAddress = updateReadingDeviceRequest.MacAddress;
                    readingDevice.Latitude = updateReadingDeviceRequest.Latitude;
                    readingDevice.Longitude = updateReadingDeviceRequest.Longitude;
                    readingDevice.CompanyId = updateReadingDeviceRequest.CompanyId;



                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new UpdateReadingDeviceResponse(readingDevice.ReadingDeviceId,
                                                                           readingDevice.Key,
                                                                           readingDevice.Name,
                                                                           readingDevice.Manufacturer,
                                                                           readingDevice.Model,
                                                                           readingDevice.SerialNumber,
                                                                           readingDevice.IsActive,
                                                                           readingDevice.AssociatedUserId,
                                                                           readingDevice.SharedUserId,
                                                                           readingDevice.MacAddress,
                                                                           readingDevice.Latitude,
                                                                           readingDevice.Longitude,
                                                                           readingDevice.CompanyId,
                                                                           true));
                }
                else
                {
                    response = Task.FromResult(new UpdateReadingDeviceResponse(null, true, "Nenhum dispositivo de leitura encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateReadingDeviceResponse(errors, false, null); ;
            }
        }
    }
}
