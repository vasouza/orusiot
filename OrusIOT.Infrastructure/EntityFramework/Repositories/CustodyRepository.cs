﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto.CustodyDto;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure.EntityFramework.Repositories
{
    public class CustodyRepository : ICustodyRepository
    {
        private DatabaseContext DbContext { get; }

        public CustodyRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<GetCustodyByIdResponse> GetCustodyById(GetCustodyByIdRequest getCustodyRequest)
        {
            Task<GetCustodyByIdResponse> custodyResponse = null;

            try
            {
                var custody = await DbContext.Custody.FirstOrDefaultAsync(x => x.CustodyId == getCustodyRequest.CustodyId);

                if (custody != null && custody.CustodyId != null)
                {
                    custodyResponse = Task.FromResult(new GetCustodyByIdResponse(custody.CustodyId, custody.Name, custody.Email, custody.Address, custody.Phone, custody.CompanyId, true));
                }
                else
                {
                    custodyResponse = Task.FromResult(new GetCustodyByIdResponse(null, true, "Nenhuma custódia encontrada"));
                }

                return await custodyResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetCustodyByIdResponse(errors, false, null); ;
            }
        }

        public async Task<GetListCustodiesResponse> GetListCustodies(GetListCustodiesRequest getListCustodiesRequest)
        {
            Task<GetListCustodiesResponse> custodyResponse = null;

            try
            {
                var custodyList = DbContext.Custody.Skip(getListCustodiesRequest.Skip).Take(getListCustodiesRequest.Quantity).ToList();

                if (custodyList != null && custodyList.Any())
                {
                    var custodies = new List<OrusIOT.Core.Entities.Custody>();

                    foreach (var item in custodyList)
                    {
                        var custody = new OrusIOT.Core.Entities.Custody(item.CustodyId, item.Name, item.Email, item.Address, item.Phone, item.CompanyId);

                        custodies.Add(custody);

                    }

                    custodyResponse = Task.FromResult(new GetListCustodiesResponse(custodies, custodyList.Count(), getListCustodiesRequest.Page, true));
                }
                else
                {
                    custodyResponse = Task.FromResult(new GetListCustodiesResponse(null, true, "Nenhuma custódia encontrada"));
                }

                return await custodyResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetListCustodiesResponse(errors, false, null); ;
            }
        }

        public async Task<DeleteCustodyResponse> DeleteCustodyById(string custodyId)
        {
            Task<DeleteCustodyResponse> response = null;
            try
            {

                var custody = await DbContext.Custody.FirstOrDefaultAsync(x => x.CustodyId == custodyId);

                if (custody != null && custody.CustodyId != null)
                {
                    DbContext.Remove(custody);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteCustodyResponse(custodyId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteCustodyResponse(null, true, "Nenhuma custódia encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteCustodyResponse(errors, ex.Message, false); ;
            }
        }

        public async Task<CreateCustodyResponse> CreateCustody(CreateCustodyRequest createCustodyRequest)
        {
            Task<CreateCustodyResponse> response = null;
            try
            {
                var custody = await DbContext.Custody.AddAsync(new Entities.Custody()
                {
                    CustodyId = Convert.ToString(Guid.NewGuid()),
                    Address = createCustodyRequest.Address,
                    CompanyId = createCustodyRequest.CompanyId,
                    Email = createCustodyRequest.Email,
                    Name = createCustodyRequest.Name,
                    Phone = createCustodyRequest.Phone
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateCustodyResponse(
                    custody.Entity.CustodyId, custody.Entity.Name, custody.Entity.Email, custody.Entity.Address, custody.Entity.Phone, custody.Entity.CompanyId, true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateCustodyResponse(errors, false, null); ;
            }
        }

        public async Task<UpdateCustodyResponse> UpdateCustody(UpdateCustodyRequest updateCustodyRequest)
        {
            Task<UpdateCustodyResponse> response = null;
            try
            {

                var custody = await DbContext.Custody.FirstOrDefaultAsync(x => x.CustodyId == updateCustodyRequest.CustodyId);

                if (custody != null && custody.CustodyId != null)
                {
                    custody.Email = updateCustodyRequest.Email;
                    custody.CompanyId = updateCustodyRequest.CompanyId;
                    custody.Phone = updateCustodyRequest.Phone;
                    custody.Name = updateCustodyRequest.Name;
                    custody.Address = updateCustodyRequest.Address;

                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new UpdateCustodyResponse(
                                                         updateCustodyRequest.CustodyId,
                                                         updateCustodyRequest.Name,
                                                         updateCustodyRequest.Email,
                                                         updateCustodyRequest.Address,
                                                         updateCustodyRequest.Phone,
                                                         updateCustodyRequest.CompanyId, true));
                }
                else
                {
                    response = Task.FromResult(new UpdateCustodyResponse(null, true, "Nenhuma custódia encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateCustodyResponse(errors, false, null); ;
            }
        }
    }
}
