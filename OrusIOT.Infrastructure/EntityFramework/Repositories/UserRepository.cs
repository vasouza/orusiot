﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto;
using OrusIOT.Core.Dto.CustodyDto;
using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Dto.UserDto;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure
{
    public class UserRepository : IUserRepository
    {
        private DatabaseContext DbContext { get; }

        public UserRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<GetListUserResponse> GetUsers(GetListUserRequest request)
        {
            Task<GetListUserResponse> userResponse = null;

            try
            {
                var allUsers = DbContext.User.Include(d => d.DepartmentNavigation).Include(c => c.CompanyNavigation)
                                             .Where(x => x.CompanyId == request.CompanyId).Skip(request.Skip).Take(request.Quantity).ToList();

                if (allUsers != null && allUsers.Any())
                {
                    var users = new List<OrusIOT.Core.Entities.User>();
                    foreach (var item in allUsers)
                    {
                        var company = new Core.Entities.Company(item.CompanyNavigation.CompanyId, item.CompanyNavigation.Name,
                                                                item.CompanyNavigation.Address, item.CompanyNavigation.PrimaryEmail, item.CompanyNavigation.SecondaryEmail,
                                                                item.CompanyNavigation.Logo, item.CompanyNavigation.IsActive);

                        var department = new Core.Entities.Department(item.DepartmentNavigation.DepartmentId, item.DepartmentNavigation.Name, company);

                        users.Add(new Core.Entities.User(
                                                    item.UserId,
                                                    item.Login,
                                                    item.Address,
                                                    item.Name,
                                                    item.Email,
                                                    item.Phone,
                                                    item.IsActive,
                                                    item.Password,
                                                    department,
                                                    company));
                    }
                    userResponse = Task.FromResult(new GetListUserResponse(users, users.Count, request.Page, true));
                }
                else
                {
                    userResponse = Task.FromResult(new GetListUserResponse(null, true, "Nenhum usuário encontrado"));
                }

                return await userResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetListUserResponse(errors, false, null);
            }
        }

        public async Task<GetUserByIdResponse> GetUserById(GetUserByIdRequest request)
        {
            Task<GetUserByIdResponse> userResponse = null;

            try
            {
                var user = await DbContext.User.Include(d => d.DepartmentNavigation).Include(c => c.CompanyNavigation).FirstOrDefaultAsync(x => x.UserId == request.UserId);

                if (user != null && user.UserId != null)
                {
                    var company = new Core.Entities.Company(user.CompanyNavigation.CompanyId, user.CompanyNavigation.Name,
                                                                user.CompanyNavigation.Address, user.CompanyNavigation.PrimaryEmail, user.CompanyNavigation.SecondaryEmail,
                                                                user.CompanyNavigation.Logo, user.CompanyNavigation.IsActive);

                    var department = new Core.Entities.Department(user.DepartmentNavigation.DepartmentId, user.DepartmentNavigation.Name, company);

                    var userReturn = new OrusIOT.Core.Entities.User(
                                                                user.UserId,
                                                                user.Login,
                                                                user.Address,
                                                                user.Name,
                                                                user.Email,
                                                                user.Phone,
                                                                user.IsActive,
                                                                user.Password,
                                                                department,
                                                                company);

                    userResponse = Task.FromResult(new GetUserByIdResponse(userReturn, true));
                }
                else
                {
                    userResponse = Task.FromResult(new GetUserByIdResponse(null, true, "Nenhum usuário encontrado"));
                }

                return await userResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetUserByIdResponse(errors, ex.Message, false);
            }
        }

        public async Task<DeleteUserResponse> DeleteUserById(DeleteUserRequest deleteUserRequest)
        {
            Task<DeleteUserResponse> response = null;
            try
            {

                var User = await DbContext.User.FirstOrDefaultAsync(x => x.UserId == deleteUserRequest.UserId);

                if (User != null && User.UserId != null)
                {
                    DbContext.Remove(User);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteUserResponse(deleteUserRequest.UserId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteUserResponse(null, true, "Nenhuma usuário encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteUserResponse(errors, ex.Message, false); ;
            }
        }

        public async Task<CreateUserResponse> CreateUser(CreateUserRequest createUserRequest)
        {
            Task<CreateUserResponse> response = null;

            try
            {
                var User = await DbContext.User.AddAsync(new EntityFramework.Entities.User()
                {
                    UserId = Convert.ToString(Guid.NewGuid()),
                    Name = createUserRequest.Name,
                    Login = createUserRequest.Login,
                    Address = createUserRequest.Address,
                    CompanyId = createUserRequest.CompanyId,
                    Password = createUserRequest.Password,
                    IsActive = createUserRequest.IsActive,
                    DepartmentId = createUserRequest.DepartmentId,
                    Email = createUserRequest.Email,
                    Phone = createUserRequest.Phone
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateUserResponse(User.Entity.Login, User.Entity.Address, User.Entity.Name, User.Entity.Email, User.Entity.Phone
                                                                  , User.Entity.IsActive, User.Entity.DepartmentId, User.Entity.CompanyId, true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateUserResponse(errors, false, null); ;
            }
        }

        public async Task<UpdateUserResponse> UpdateUser(UpdateUserRequest updateUserRequest)
        {
            Task<UpdateUserResponse> response = null;
            try
            {

                var user = await DbContext.User.Include(d => d.DepartmentNavigation).Include(c => c.CompanyNavigation).FirstOrDefaultAsync(x => x.UserId == updateUserRequest.UserId);

                if (user != null && user.UserId != null)
                {
                    user.Address = updateUserRequest.Address;
                    user.Name = updateUserRequest.Name;
                    user.Email = updateUserRequest.Email;
                    user.IsActive = updateUserRequest.IsActive;
                    user.Phone = updateUserRequest.Phone;
                    user.DepartmentId = updateUserRequest.DepartmentId;

                    await DbContext.SaveChangesAsync();


                    var company = new Core.Entities.Company(user.CompanyNavigation.CompanyId, user.CompanyNavigation.Name,
                                                           user.CompanyNavigation.Address, user.CompanyNavigation.PrimaryEmail, user.CompanyNavigation.SecondaryEmail,
                                                           user.CompanyNavigation.Logo, user.CompanyNavigation.IsActive);

                    var department = new Core.Entities.Department(user.DepartmentNavigation.DepartmentId, user.DepartmentNavigation.Name, company);

                    var userReturn = new OrusIOT.Core.Entities.User(
                                                            user.UserId,
                                                            user.Login,
                                                            user.Address,
                                                            user.Name,
                                                            user.Email,
                                                            user.Phone,
                                                            user.IsActive,
                                                            user.Password,
                                                            department,
                                                            company);


                    response = Task.FromResult(new UpdateUserResponse(new Core.Entities.User(user.UserId,
                                                                                                user.Login,
                                                                                                user.Address,
                                                                                                user.Name,
                                                                                                user.Email,
                                                                                                user.Phone,
                                                                                                user.IsActive,
                                                                                                user.Password,
                                                                                                department,
                                                                                                company), true));
                }
                else
                {
                    response = Task.FromResult(new UpdateUserResponse(null, true, "Nenhum usuário encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateUserResponse(errors, null, false); ;
            }
        }

        //public async Task<RecoverUserPasswordResponse> RecoverPassword(RecoverUserPasswordRequest recoverUserPassword)
        //{
        //    try
        //    {
        //        Task<RecoverUserPasswordResponse> userResponse;
        //        EntityFramework.Entities.User user = null;

        //        if (!string.IsNullOrEmpty(recoverUserPassword.Email))
        //        {
        //            user = await DbContext.User.FirstOrDefaultAsync(x => x.Email.ToLower() == recoverUserPassword.Email.ToLower());
        //        }
        //        else if(!string.IsNullOrEmpty(recoverUserPassword.Login))
        //        {
        //            user = await DbContext.User.FirstOrDefaultAsync(x => x.Login.ToLower() == recoverUserPassword.Login.ToLower());
        //        }

        //        if (user != null)
        //        {
        //            userResponse = Task.FromResult(new RecoverUserPasswordResponse(Convert.ToString(Guid.NewGuid()), true));
        //        }
        //        else
        //        {
        //            userResponse = Task.FromResult(new RecoverUserPasswordResponse(null, true, "Nenhum usuário encontrado"));
        //        }

        //        return await userResponse;
        //    }
        //    catch (Exception ex)
        //    {
        //        List<string> errors = new List<string>();
        //        errors.Add(ex.Message);
        //        return new RecoverUserPasswordResponse(errors, ex.Message, false);
        //    }
        //}

        //public Task<LoginUserResponse> LoginUser(LoginUserRequest loginUserRequest)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
