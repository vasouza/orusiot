﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto.LocaleDto;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure.EntityFramework.Repositories
{
    public class LocaleRepository : ILocaleRepository
    {
        private DatabaseContext DbContext { get; }

        public LocaleRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<GetListLocalesResponse> GetLocales(GetListLocalesRequest request)
        {
            Task<GetListLocalesResponse> localesResponse = null;

            try
            {
                var allLocales = DbContext.Locale.Include(c => c.CompanyNavigation).Where(x => x.CompanyId == request.CompanyId).Skip(request.Skip).Take(request.Quantity).ToList();

                if (allLocales != null && allLocales.Any())
                {
                    var locales = new List<OrusIOT.Core.Entities.Locale>();

                    foreach (var item in allLocales)
                    {

                        locales.Add(new Core.Entities.Locale(item.LocaleId, item.Name, item.IsActive, item.CompanyId));
                    }

                    localesResponse = Task.FromResult(new GetListLocalesResponse(locales, locales.Count, request.Page, true));
                }
                else
                {
                    localesResponse = Task.FromResult(new GetListLocalesResponse(null, true, "Nenhuma localidade encontrada"));
                }

                return await localesResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetListLocalesResponse(errors, false, null);
            }
        }

        public async Task<GetLocaleByIdResponse> GetLocaleById(GetLocaleByIdRequest request)
        {
            Task<GetLocaleByIdResponse> localesResponse = null;
            try
            {
                var locale = await DbContext.Locale.Include(c => c.CompanyNavigation).FirstOrDefaultAsync(x => x.LocaleId == request.LocaleId);

                if (locale != null && !string.IsNullOrEmpty(locale.LocaleId))
                {
                    localesResponse = Task.FromResult(new GetLocaleByIdResponse(locale.LocaleId, locale.Name, locale.IsActive, locale.CompanyId, true));
                }
                else
                {
                    localesResponse = Task.FromResult(new GetLocaleByIdResponse(null, true, "Nenhuma localidade encontrada"));
                }

                return await localesResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetLocaleByIdResponse(errors, false, null);
            }
        }

        public async Task<UpdateLocaleResponse> UpdateLocale(UpdateLocaleRequest request)
        {
            Task<UpdateLocaleResponse> localesResponse = null;
            try
            {
                var locale = await DbContext.Locale.Include(c => c.CompanyNavigation).FirstOrDefaultAsync(x => x.LocaleId == request.LocaleId);

                if (locale != null && !string.IsNullOrEmpty(locale.LocaleId))
                {
                    locale.Name = request.Name;
                    locale.CompanyId = request.CompanyId;
                    locale.IsActive = request.IsActive;

                    await DbContext.SaveChangesAsync();

                    localesResponse = Task.FromResult(new UpdateLocaleResponse(
                                                        request.LocaleId,
                                                        request.Name,
                                                        request.IsActive,
                                                        request.CompanyId, true));
                }
                else
                {
                    localesResponse = Task.FromResult(new UpdateLocaleResponse(null, true, "Nenhuma localidade encontrada"));
                }

                return await localesResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateLocaleResponse(errors, false, null);
            }
        }

        public async Task<CreateLocaleResponse> CreateLocale(CreateLocaleRequest request)
        {
            Task<CreateLocaleResponse> response = null;
            try
            {
                var locale = await DbContext.Locale.AddAsync(new Entities.Locale()
                {
                    LocaleId = Convert.ToString(Guid.NewGuid()),
                    Name = request.Name,
                    CompanyId = request.CompanyId,
                    IsActive = request.IsActive
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateLocaleResponse(                                                    
                                                            locale.Entity.LocaleId,
                                                            locale.Entity.Name,
                                                            locale.Entity.IsActive,
                                                            locale.Entity.CompanyId, true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateLocaleResponse(errors, false, null); ;
            }
        }

        public async Task<DeleteLocaleResponse> DeleteLocale(DeleteLocaleRequest request)
        {
            Task<DeleteLocaleResponse> response = null;
            try
            {

                var locale = await DbContext.Locale.FirstOrDefaultAsync(x => x.LocaleId == request.LocaleId);

                if (locale != null && !string.IsNullOrEmpty(locale.LocaleId))
                {
                    DbContext.Remove(locale);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteLocaleResponse(request.LocaleId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteLocaleResponse(null, true, "Nenhuma localidade encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteLocaleResponse(errors, ex.Message, false); ;
            }
        }
    }
}
