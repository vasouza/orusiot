﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto.FloorMap;
using OrusIOT.Core.Entities;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure.EntityFramework.Repositories
{
    public class FloorMapRepository : IFloorMapRepository
    {
        private DatabaseContext DbContext { get; }

        public FloorMapRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<CreateFloorMapResponse> CreateFloorMap(CreateFloorMapRequest createFloorMapRequest)
        {
            Task<CreateFloorMapResponse> response = null;

            try
            {
                var floorMap = await DbContext.FloorMap.AddAsync(new Entities.FloorMap()
                {
                    FloorMapId = Guid.NewGuid().ToString(),
                    Name = createFloorMapRequest.Name,
                    FloorMapZones = createFloorMapRequest.FloorMapZones,
                    AreaId = createFloorMapRequest.AreaId,
                    Image = createFloorMapRequest.Image
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateFloorMapResponse(floorMap.Entity.FloorMapId,
                                                                      floorMap.Entity.Name,
                                                                      floorMap.Entity.FloorMapZones,
                                                                      floorMap.Entity.AreaId,
                                                                      floorMap.Entity.Image,
                                                                      floorMap.Entity.CompanyId,
                                                                      true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateFloorMapResponse(errors, false, null); ;
            }
        }

        public async Task<GetFloorMapByIdResponse> GetFloorMapById(GetFloorMapByIdRequest getFloorMapRequest)
        {
            Task<GetFloorMapByIdResponse> floorMapResponse = null;

            try
            {
                var floorMap = await DbContext.FloorMap.FirstOrDefaultAsync(x => x.FloorMapId == getFloorMapRequest.FloorMapId);

                if (floorMap != null && floorMap.FloorMapId != null)
                {
                    floorMapResponse = Task.FromResult(new GetFloorMapByIdResponse(floorMap.FloorMapId,
                                                                                   floorMap.Name,
                                                                                   floorMap.FloorMapZones,
                                                                                   floorMap.AreaId,
                                                                                   floorMap.Image,
                                                                                   floorMap.CompanyId,
                                                                                   true));
                }
                else
                {
                    floorMapResponse = Task.FromResult(new GetFloorMapByIdResponse(null, true, "Nenhuma planta encontrada"));
                }

                return await floorMapResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetFloorMapByIdResponse(errors, false, null); ;
            }
        }

        public async Task<GetListFloorMapResponse> GetListFloorMaps(GetListFloorMapRequest getListFloorMapRequest)
        {
            Task<GetListFloorMapResponse> getListFloorMapResponse = null;

            try
            {
                var dbFloorMaps = DbContext.FloorMap.Where(x => x.CompanyId == getListFloorMapRequest.CompanyId).
                                                    Skip(getListFloorMapRequest.Skip).Take(getListFloorMapRequest.Quantity);

                if (dbFloorMaps != null && dbFloorMaps.Any())
                {
                    var floorMaps = new List<FloorMap>();

                    foreach (var item in dbFloorMaps)
                    {
                        floorMaps.Add(new FloorMap(item.FloorMapId, item.Name, item.FloorMapZones, item.AreaId, item.Image, item.CompanyId));
                    }

                    getListFloorMapResponse = Task.FromResult(new GetListFloorMapResponse(floorMaps,
                                                                                          floorMaps.Count,
                                                                                          getListFloorMapRequest.Page,
                                                                                          true,
                                                                                          string.Empty));
                }
                else
                {
                    getListFloorMapResponse = Task.FromResult(new GetListFloorMapResponse(null, true, "Nenhuma planta encontrada."));
                }

                return await getListFloorMapResponse;
            }
            catch (Exception ex)
            {
                var errors = new List<string>();
                errors.Add(ex.Message);
                return new GetListFloorMapResponse(errors, false);
            }
        }

        public async Task<UpdateFloorMapResponse> UpdateFloorMap(UpdateFloorMapRequest updateFloorMapRequest)
        {
            Task<UpdateFloorMapResponse> response = null;

            try
            {
                var floorMap = await DbContext.FloorMap.FirstOrDefaultAsync(x => x.FloorMapId == updateFloorMapRequest.FloorMapId);

                if (floorMap != null && floorMap.FloorMapId != null)
                {
                    floorMap.Name = updateFloorMapRequest.Name ?? floorMap.Name;
                    floorMap.FloorMapZones = updateFloorMapRequest.FloorMapZones ?? floorMap.FloorMapZones;
                    floorMap.AreaId = updateFloorMapRequest.AreaId ?? floorMap.AreaId;
                    floorMap.Image = updateFloorMapRequest.Image ?? floorMap.Image;

                    await DbContext.SaveChangesAsync();
                    response = Task.FromResult(new UpdateFloorMapResponse(floorMap.FloorMapId,
                                                                          floorMap.Name,
                                                                          floorMap.FloorMapZones,
                                                                          floorMap.AreaId,
                                                                          floorMap.Image,
                                                                          floorMap.CompanyId,
                                                                          true));
                }
                else
                {
                    response = Task.FromResult(new UpdateFloorMapResponse(null, true, "Nenhuma planta encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateFloorMapResponse(errors, false, null); ;
            }
        }

        public async Task<DeleteFloorMapResponse> DeleteFloorMap(string floorMapId)
        {
            Task<DeleteFloorMapResponse> response = null;

            try
            {

                var floorMap = await DbContext.FloorMap.FirstOrDefaultAsync(x => x.FloorMapId == floorMapId);

                if (floorMap != null && floorMap.FloorMapId != null)
                {
                    DbContext.Remove(floorMap);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteFloorMapResponse(floorMapId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteFloorMapResponse(null, true, "Nenhuma planta encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteFloorMapResponse(errors, ex.Message, false); ;
            }
        }
    }
}
