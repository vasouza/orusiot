﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto.MachineUserDto;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure.EntityFramework.Repositories
{
    public class MachineUserRepository : IMachineUserRepository
    {
        private DatabaseContext DbContext { get; }

        public MachineUserRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<CreateMachineUserResponse> CreateMachineUser(CreateMachineUserRequest createMachineUserRequest)
        {
            Task<CreateMachineUserResponse> response = null;

            try
            {
                var MachineUser = await DbContext.MachineUser.AddAsync(new Entities.MachineUser()
                {
                    MachineUserId = Convert.ToString(Guid.NewGuid()),
                    Name = createMachineUserRequest.Name,
                    Login = createMachineUserRequest.Login,
                    CompanyId = createMachineUserRequest.CompanyId
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateMachineUserResponse(MachineUser.Entity.MachineUserId,
                                                                         MachineUser.Entity.Name,
                                                                         MachineUser.Entity.Login,
                                                                         MachineUser.Entity.CompanyId, true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateMachineUserResponse(errors, false, null); ;
            }
        }

        public async Task<DeleteMachineUserResponse> DeleteMachineUserById(DeleteMachineUserRequest deleteMachineUserRequest)
        {
            Task<DeleteMachineUserResponse> response = null;
            try
            {

                var area = await DbContext.MachineUser.FirstOrDefaultAsync(x => x.MachineUserId == deleteMachineUserRequest.MachineUserId);

                if (area != null && area.MachineUserId != null)
                {
                    DbContext.Remove(area);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteMachineUserResponse(deleteMachineUserRequest.MachineUserId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteMachineUserResponse(null, true, "Nenhuma máquina de usuário encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteMachineUserResponse(errors, ex.Message, false); ;
            }
        }

        public async Task<GetListMachineUserResponse> GetListMachineUsers(GetListMachineUserRequest getListMachineUserRequest)
        {
            Task<GetListMachineUserResponse> response = null;

            try
            {
                var machineUserList = DbContext.MachineUser.Where(x => x.CompanyId == getListMachineUserRequest.CompanyId).Skip(getListMachineUserRequest.Skip).Take(getListMachineUserRequest.Quantity).ToList();

                if (machineUserList != null && machineUserList.Any())
                {
                    var machineList = new List<OrusIOT.Core.Entities.MachineUser>();

                    foreach (var item in machineUserList)
                    {
                        var machineUser = new OrusIOT.Core.Entities.MachineUser(item.MachineUserId, item.Name, item.Login, item.CompanyId);

                        machineList.Add(machineUser);
                    }

                    response = Task.FromResult(new GetListMachineUserResponse(machineList, machineUserList.Count(), getListMachineUserRequest.Page, true));
                }
                else
                {
                    response = Task.FromResult(new GetListMachineUserResponse(null, true, "Nenhuma máquina de usuário encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetListMachineUserResponse(errors, false, null); ;
            }
        }

        public async Task<GetMachineUserByIdResponse> GetMachineUserById(GetMachineUserByIdRequest getMachineUserByIdRequest)
        {
            Task<GetMachineUserByIdResponse> response = null;

            try
            {
                var machineUser = await DbContext.MachineUser.FirstOrDefaultAsync(x => x.MachineUserId == getMachineUserByIdRequest.MachineUserId);

                if (machineUser != null && machineUser.MachineUserId != null)
                {
                    response = Task.FromResult(new GetMachineUserByIdResponse(machineUser.MachineUserId,
                                                                              machineUser.Name,
                                                                              machineUser.Login,
                                                                              machineUser.CompanyId,
                                                                              true));
                }
                else
                {
                    response = Task.FromResult(new GetMachineUserByIdResponse(null, true, "Nenhuma máquina de usuário encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetMachineUserByIdResponse(errors, false, null); ;
            }
        }

        public async Task<UpdateMachineUserResponse> UpdateMachineUser(UpdateMachineUserRequest updateMachineUserRequest)
        {
            Task<UpdateMachineUserResponse> response = null;
            try
            {

                var MachineUser = await DbContext.MachineUser.FirstOrDefaultAsync(x => x.MachineUserId == updateMachineUserRequest.MachineUserId);

                if (MachineUser != null && MachineUser.MachineUserId != null)
                {
                    MachineUser.Name = updateMachineUserRequest.Name;
                    MachineUser.Login = updateMachineUserRequest.Login;
                    MachineUser.CompanyId = updateMachineUserRequest.CompanyId;

                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new UpdateMachineUserResponse(
                                                         updateMachineUserRequest.MachineUserId,
                                                         updateMachineUserRequest.Name,
                                                         updateMachineUserRequest.Login,
                                                         updateMachineUserRequest.CompanyId, true));
                }
                else
                {
                    response = Task.FromResult(new UpdateMachineUserResponse(null, true, "Nenhuma máquina de usuário encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateMachineUserResponse(errors, false, null); ;
            }
        }
    }
}
