﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto.ItemDto;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure.EntityFramework.Repositories
{
    public class ItemRepository : IItemRepository
    {
        private DatabaseContext DbContext { get; }

        public ItemRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<CreateItemResponse> CreateItem(CreateItemRequest createItemRequest)
        {
            Task<CreateItemResponse> response = null;

            try
            {
                var Item = await DbContext.Item.AddAsync(new Entities.Item()
                {
                    ItemId = Convert.ToString(Guid.NewGuid()),
                    Name = createItemRequest.Name,
                    Code = createItemRequest.Code,
                    SerialNumber = createItemRequest.SerialNumber,
                    IsActive = createItemRequest.IsActive,
                    CompanyId = createItemRequest.CompanyId,
                    LocaleId = createItemRequest.LocaleId,
                    CustodyId = createItemRequest.CustodyId                    
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateItemResponse(Item.Entity.ItemId, Item.Entity.Name, Item.Entity.Code, Item.Entity.SerialNumber,
                                                                  Item.Entity.IsActive, Item.Entity.CustodyId, Item.Entity.LocaleId,
                                                                  Item.Entity.CompanyId, true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateItemResponse(errors, false, null); ;
            }
        }

        public async Task<DeleteItemResponse> DeleteItemById(DeleteItemRequest deleteItemRequest)
        {
            Task<DeleteItemResponse> response = null;
            try
            {

                var Item = await DbContext.Item.FirstOrDefaultAsync(x => x.ItemId == deleteItemRequest.ItemId);

                if (Item != null && Item.ItemId != null)
                {
                    DbContext.Remove(Item);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteItemResponse(deleteItemRequest.ItemId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteItemResponse(null, true, "Nenhum Item encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteItemResponse(errors, ex.Message, false); ;
            }
        }

        public async Task<GetListItemResponse> GetListItems(GetListItemRequest getListItemRequest)
        {
            Task<GetListItemResponse> response = null;

            try
            {
                var ItemList = DbContext.Item.Where(x => x.CompanyId == getListItemRequest.CompanyId)
                                             .Skip(getListItemRequest.Skip).Take(getListItemRequest.Quantity).ToList();

                if (ItemList != null && ItemList.Any())
                {
                    var ItemEntity = new List<OrusIOT.Core.Entities.Item>();

                    foreach (var item in ItemList)
                    {
                        var itemCreated = new OrusIOT.Core.Entities.Item(item.ItemId, item.Name, item.Code, item.SerialNumber,
                                                                          item.IsActive, item.CustodyId, item.LocaleId,
                                                                          item.CompanyId);

                        ItemEntity.Add(itemCreated);
                    }

                    response = Task.FromResult(new GetListItemResponse(ItemEntity, ItemList.Count(), getListItemRequest.Page, true));
                }
                else
                {
                    response = Task.FromResult(new GetListItemResponse(null, true, "Nenhum Item encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetListItemResponse(errors, false, null); ;
            }
        }

        public async Task<GetItemByIdResponse> GetItemById(GetItemByIdRequest getItemByIdRequest)
        {
            Task<GetItemByIdResponse> response = null;

            try
            {
                var item = await DbContext.Item.FirstOrDefaultAsync(x => x.ItemId == getItemByIdRequest.ItemId);

                if (item != null && item.ItemId != null)
                {
                    response = Task.FromResult(new GetItemByIdResponse(item.ItemId, item.Name, item.Code, item.SerialNumber,
                                                                          item.IsActive, item.CustodyId, item.LocaleId,
                                                                          item.CompanyId, true));
                }
                else
                {
                    response = Task.FromResult(new GetItemByIdResponse(null, true, "Nenhum Item encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetItemByIdResponse(errors, false, null); ;
            }
        }

        public async Task<UpdateItemResponse> UpdateItem(UpdateItemRequest updateItemRequest)
        {
            Task<UpdateItemResponse> response = null;
            try
            {

                var item = await DbContext.Item.FirstOrDefaultAsync(x => x.ItemId == updateItemRequest.ItemId);

                if (item != null && item.ItemId != null)
                {
                    item.Name = updateItemRequest.Name;
                    item.Code = updateItemRequest.Code;
                    item.SerialNumber = updateItemRequest.SerialNumber;
                    item.IsActive = updateItemRequest.IsActive;
                    item.CompanyId = updateItemRequest.CompanyId;
                    item.LocaleId = updateItemRequest.LocaleId;
                    item.CustodyId = updateItemRequest.CustodyId;

                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new UpdateItemResponse(item.ItemId, item.Name, item.Code, item.SerialNumber,
                                                                          item.IsActive, item.CustodyId, item.LocaleId,
                                                                          item.CompanyId, true));
                }
                else
                {
                    response = Task.FromResult(new UpdateItemResponse(null, true, "Nenhum Item encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateItemResponse(errors, false, null); ;
            }
        }
    }
}
