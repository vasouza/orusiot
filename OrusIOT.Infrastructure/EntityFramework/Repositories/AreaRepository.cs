﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto.AreaDto;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure.EntityFramework.Repositories
{
    public class AreaRepository : IAreaRepository
    {
        private DatabaseContext DbContext { get; }

        public AreaRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<CreateAreaResponse> CreateArea(CreateAreaRequest createAreaRequest)
        {
            Task<CreateAreaResponse> response = null;

            try
            {
                var area = await DbContext.Area.AddAsync(new Entities.Area()
                {
                    AreaId = Convert.ToString(Guid.NewGuid()),
                    Name = createAreaRequest.Name,
                    Address = createAreaRequest.Address,
                    Floor = createAreaRequest.Floor,
                    City = createAreaRequest.City,
                    Country = createAreaRequest.Country,
                    ZipCode = createAreaRequest.ZipCode,
                    IsActive = createAreaRequest.IsActive,
                    CompanyId = createAreaRequest.CompanyId,
                    LocaleId = createAreaRequest.LocaleId
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateAreaResponse(area.Entity.AreaId, area.Entity.Name, area.Entity.Address, area.Entity.Floor,
                                                                  area.Entity.City, area.Entity.Country, area.Entity.ZipCode, area.Entity.LocaleId,
                                                                  area.Entity.CompanyId, true, true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateAreaResponse(errors, false, null); ;
            }
        }

        public async Task<DeleteAreaResponse> DeleteAreaById(DeleteAreaRequest deleteAreaRequest)
        {
            Task<DeleteAreaResponse> response = null;
            try
            {

                var area = await DbContext.Area.FirstOrDefaultAsync(x => x.AreaId == deleteAreaRequest.AreaId);

                if (area != null && area.AreaId != null)
                {
                    DbContext.Remove(area);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteAreaResponse(deleteAreaRequest.AreaId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteAreaResponse(null, true, "Nenhuma área encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteAreaResponse(errors, ex.Message, false); ;
            }
        }

        public async Task<GetAreaByIdResponse> GetAreaById(GetAreaByIdRequest getAreaByIdRequest)
        {
            Task<GetAreaByIdResponse> response = null;

            try
            {
                var area = await DbContext.Area.FirstOrDefaultAsync(x => x.AreaId == getAreaByIdRequest.AreaId);

                if (area != null && area.AreaId != null)
                {
                    response = Task.FromResult(new GetAreaByIdResponse(area.AreaId, area.Name, area.Address, area.Floor, area.City, area.Country,
                                                                        area.ZipCode, area.LocaleId, area.CompanyId,
                                                                        area.IsActive, true));
                }
                else
                {
                    response = Task.FromResult(new GetAreaByIdResponse(null, true, "Nenhuma área encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetAreaByIdResponse(errors, false, null); ;
            }
        }

        public async Task<GetListAreaResponse> GetListAreas(GetListAreaRequest getListAreaRequest)
        {
            Task<GetListAreaResponse> custodyResponse = null;

            try
            {
                var areaList = DbContext.Area.Where(x => x.CompanyId == getListAreaRequest.CompanyId).Skip(getListAreaRequest.Skip).Take(getListAreaRequest.Quantity).ToList();

                if (areaList != null && areaList.Any())
                {
                    var areas = new List<OrusIOT.Core.Entities.Area>();

                    foreach (var item in areaList)
                    {
                        var area = new OrusIOT.Core.Entities.Area(item.AreaId, item.Name, item.Address, item.Floor, item.City,
                                                                  item.Country, item.ZipCode, item.LocaleId, item.CompanyId, item.IsActive);

                        areas.Add(area);
                    }

                    custodyResponse = Task.FromResult(new GetListAreaResponse(areas, areaList.Count(), getListAreaRequest.Page, true));
                }
                else
                {
                    custodyResponse = Task.FromResult(new GetListAreaResponse(null, true, "Nenhuma área encontrada"));
                }

                return await custodyResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetListAreaResponse(errors, false, null); ;
            }
        }

        public async Task<UpdateAreaResponse> UpdateArea(UpdateAreaRequest updateAreaRequest)
        {
            Task<UpdateAreaResponse> response = null;
            try
            {

                var area = await DbContext.Area.FirstOrDefaultAsync(x => x.AreaId == updateAreaRequest.AreaId);

                if (area != null && area.AreaId != null)
                {
                    area.Name = updateAreaRequest.Name;
                    area.Address = updateAreaRequest.Address;
                    area.Floor = updateAreaRequest.Floor;
                    area.City = updateAreaRequest.City;
                    area.Country = updateAreaRequest.Country;
                    area.ZipCode = updateAreaRequest.ZipCode;
                    area.IsActive = updateAreaRequest.IsActive;
                    area.LocaleId = updateAreaRequest.LocaleId;
                    area.CompanyId = updateAreaRequest.CompanyId;

                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new UpdateAreaResponse(
                                                         updateAreaRequest.AreaId,
                                                         updateAreaRequest.Name,
                                                         updateAreaRequest.Address,
                                                         updateAreaRequest.Floor,
                                                         updateAreaRequest.City,
                                                         updateAreaRequest.Country,
                                                         updateAreaRequest.ZipCode,
                                                         updateAreaRequest.LocaleId,
                                                         updateAreaRequest.CompanyId,
                                                         updateAreaRequest.IsActive, true));
                }
                else
                {
                    response = Task.FromResult(new UpdateAreaResponse(null, true, "Nenhuma área encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateAreaResponse(errors, false, null); ;
            }
        }
    }
}
