﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto.ZoneDto;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure.EntityFramework.Repositories
{
    public class ZoneRepository : IZoneRepository
    {
        private DatabaseContext DbContext { get; }

        public ZoneRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<CreateZoneResponse> CreateZone(CreateZoneRequest createZoneRequest)
        {
            Task<CreateZoneResponse> response = null;

            try
            {
                var zone = await DbContext.Zone.AddAsync(new Entities.Zone()
                {
                    ZoneId = Convert.ToString(Guid.NewGuid()),
                    Name = createZoneRequest.Name,
                    AreaId = createZoneRequest.AreaId,
                    IsActive = createZoneRequest.IsActive
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateZoneResponse(zone.Entity.ZoneId, zone.Entity.Name, zone.Entity.AreaId, zone.Entity.IsActive, true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateZoneResponse(errors, false, null); ;
            }
        }

        public async Task<DeleteZoneResponse> DeleteZoneById(DeleteZoneRequest deleteZoneRequest)
        {
            Task<DeleteZoneResponse> response = null;
            try
            {

                var zone = await DbContext.Zone.FirstOrDefaultAsync(x => x.ZoneId == deleteZoneRequest.ZoneId);

                if (zone != null && zone.ZoneId != null)
                {
                    DbContext.Remove(zone);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteZoneResponse(deleteZoneRequest.ZoneId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteZoneResponse(null, true, "Nenhuma zona encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteZoneResponse(errors, ex.Message, false); ;
            }
        }

        public async Task<GetListZoneResponse> GetListZones(GetListZoneRequest getListZoneRequest)
        {
            Task<GetListZoneResponse> response = null;

            try
            {
                var zoneList = DbContext.Zone.Where(x => x.AreaId == getListZoneRequest.AreaId).Skip(getListZoneRequest.Skip).Take(getListZoneRequest.Quantity).ToList();

                if (zoneList != null && zoneList.Any())
                {
                    var zoneEntity = new List<OrusIOT.Core.Entities.Zone>();

                    foreach (var item in zoneList)
                    {
                        var zone = new OrusIOT.Core.Entities.Zone(item.ZoneId, item.Name, item.AreaId, item.IsActive);

                        zoneEntity.Add(zone);
                    }

                    response = Task.FromResult(new GetListZoneResponse(zoneEntity, zoneList.Count(), getListZoneRequest.Page, true));
                }
                else
                {
                    response = Task.FromResult(new GetListZoneResponse(null, true, "Nenhuma zona encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetListZoneResponse(errors, false, null); ;
            }
        }

        public async Task<GetZoneByIdResponse> GetZoneById(GetZoneByIdRequest getZoneByIdRequest)
        {
            Task<GetZoneByIdResponse> response = null;

            try
            {
                var zone = await DbContext.Zone.FirstOrDefaultAsync(x => x.ZoneId == getZoneByIdRequest.ZoneId);

                if (zone != null && zone.ZoneId != null)
                {
                    response = Task.FromResult(new GetZoneByIdResponse(zone.ZoneId, zone.Name, zone.AreaId, zone.IsActive, true));
                }
                else
                {
                    response = Task.FromResult(new GetZoneByIdResponse(null, true, "Nenhuma zona encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetZoneByIdResponse(errors, false, null); ;
            }
        }

        public async Task<UpdateZoneResponse> UpdateZone(UpdateZoneRequest updateZoneRequest)
        {
            Task<UpdateZoneResponse> response = null;
            try
            {

                var zone = await DbContext.Zone.FirstOrDefaultAsync(x => x.ZoneId == updateZoneRequest.ZoneId);

                if (zone != null && zone.ZoneId != null)
                {
                    zone.Name = updateZoneRequest.Name;
                    zone.AreaId = updateZoneRequest.AreaId;
                    zone.IsActive = updateZoneRequest.IsActive;

                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new UpdateZoneResponse(
                                                         updateZoneRequest.ZoneId,
                                                         updateZoneRequest.Name,
                                                         updateZoneRequest.AreaId,
                                                         updateZoneRequest.IsActive, true));
                }
                else
                {
                    response = Task.FromResult(new UpdateZoneResponse(null, true, "Nenhuma zona encontrada"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateZoneResponse(errors, false, null); ;
            }
        }
    }
}
