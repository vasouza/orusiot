﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Core.Dto;
using OrusIOT.Core.Dto.Device;
using OrusIOT.Core.Entities;
using OrusIOT.Core.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrusIOT.Infrastructure.EntityFramework.Repositories
{
    public class DeviceRepository : IDeviceRepository
    {
        private DatabaseContext DbContext { get; }

        public DeviceRepository(IConfiguration configuration)
        {
            DbContext = new DatabaseContext(configuration);
        }

        public async Task<GetDeviceResponse> GetDevices(GetDeviceRequest getDeviceRequest)
        {
            try
            {
                var dbDevices = DbContext.Device.Where(x => x.CompanyId == getDeviceRequest.CompanyId).Skip(getDeviceRequest.Skip).Take(getDeviceRequest.Quantity);

                Task<GetDeviceResponse> getDeviceResponse;
                if (dbDevices != null && dbDevices.Any())
                {
                    var devices = new List<Device>();

                    foreach (var item in dbDevices)
                    {
                        devices.Add(new Device(item.DeviceId, item.MacAddress, item.Name));
                    }

                    getDeviceResponse = Task.FromResult(new GetDeviceResponse(devices, devices.Count, getDeviceRequest.Page, true, string.Empty));
                }
                else
                {
                    getDeviceResponse = Task.FromResult(new GetDeviceResponse(null, true, "Nenhum aparelho encontrado."));
                }

                return await getDeviceResponse;
            }
            catch (Exception ex)
            {
                var errors = new List<string>();
                errors.Add(ex.Message);
                return new GetDeviceResponse(errors, false);
            }
        }

        public async Task<GetDeviceByIdResponse> GetDeviceById(GetDeviceByIdRequest getDeviceRequest)
        {
            try
            {
                var device = await DbContext.Device.FirstOrDefaultAsync(x => x.DeviceId == getDeviceRequest.DeviceId);

                Task<GetDeviceByIdResponse> deviceResponse = null;
                if (device != null && device.DeviceId != null)
                {
                    deviceResponse = Task.FromResult(new GetDeviceByIdResponse(device.DeviceId, device.Name, device.MacAddress, true));
                }
                else
                {
                    deviceResponse = Task.FromResult(new GetDeviceByIdResponse(null, true, "Nenhum aparelho encontrada"));
                }

                return await deviceResponse;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new GetDeviceByIdResponse(errors, false, null); ;
            }
        }

        public async Task<CreateDeviceResponse> CreateDevice(CreateDeviceRequest createDeviceRequest)
        {
            Task<CreateDeviceResponse> response = null;
            try
            {
                var device = await DbContext.Device.AddAsync(new Entities.Device()
                {
                    DeviceId = Guid.NewGuid().ToString(),
                    Name = createDeviceRequest.Name,
                    MacAddress = createDeviceRequest.MacAddress
                });

                await DbContext.SaveChangesAsync();

                response = Task.FromResult(new CreateDeviceResponse(device.Entity.DeviceId, device.Entity.Name, device.Entity.MacAddress, true));
                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new CreateDeviceResponse(errors, false, null); ;
            }
        }

        public async Task<UpdateDeviceResponse> UpdateDevice(UpdateDeviceRequest updateDeviceRequest)
        {
            Task<UpdateDeviceResponse> response = null;
            try
            {
                var device = await DbContext.Device.FirstOrDefaultAsync(x => x.DeviceId == updateDeviceRequest.DeviceId);

                if (device != null && device.DeviceId != null)
                {
                    device.Name = updateDeviceRequest.Name ?? device.Name;
                    device.MacAddress = updateDeviceRequest.MacAddress ?? device.MacAddress;

                    await DbContext.SaveChangesAsync();
                    response = Task.FromResult(new UpdateDeviceResponse(device.DeviceId, device.Name, device.MacAddress, true));
                }
                else
                {
                    response = Task.FromResult(new UpdateDeviceResponse(null, true, "Nenhum aparelho encontrado"));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new UpdateDeviceResponse(errors, false, null); ;
            }
        }

        public async Task<DeleteDeviceResponse> DeleteDevice(string deviceId)
        {
            Task<DeleteDeviceResponse> response = null;
            try
            {

                var device = await DbContext.Device.FirstOrDefaultAsync(x => x.DeviceId == deviceId);

                if (device != null && device.DeviceId != null)
                {
                    DbContext.Remove(device);
                    await DbContext.SaveChangesAsync();

                    response = Task.FromResult(new DeleteDeviceResponse(deviceId, true));
                }
                else
                {
                    response = Task.FromResult(new DeleteDeviceResponse(null, "Nenhum aparelho encontrado", true));
                }

                return await response;
            }
            catch (Exception ex)
            {
                List<string> errors = new List<string>();
                errors.Add(ex.Message);
                return new DeleteDeviceResponse(errors, ex.Message, false); ;
            }
        }
    }
}
