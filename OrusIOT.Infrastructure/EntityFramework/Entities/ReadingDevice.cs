﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class ReadingDevice
    {
        public string ReadingDeviceId { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public bool IsActive { get; set; }
        public string AssociatedUserId { get; set; }
        public string SharedUserId { get; set; }
        public string MacAddress { get; set; }
        public int Latitude { get; set; }
        public int Longitude { get; set; }
        public string CompanyId { get; set; }

        public MachineUser MachineUserNavigation { get; set; }
        public Company CompanyNavigation { get; set; }
    }
}
