﻿using System;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public partial class User
    {
        public string UserId { get; set; }
        public string Login { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsActive { get; set; }
        public string Password { get; set; }
        public string DepartmentId { get; set; }
        public string CompanyId { get; set; }

        public Department DepartmentNavigation { get; set; }
        public Company CompanyNavigation { get; set; }

    }
}
