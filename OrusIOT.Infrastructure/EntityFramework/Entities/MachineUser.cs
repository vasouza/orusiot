﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class MachineUser
    {
        public MachineUser()
        {
            ReadingDevice = new HashSet<ReadingDevice>();
        }

        public string MachineUserId { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string CompanyId { get; set; }

        public Company CompanyNavigation { get; set; }

        public ICollection<ReadingDevice> ReadingDevice { get; set; }
    }
}
