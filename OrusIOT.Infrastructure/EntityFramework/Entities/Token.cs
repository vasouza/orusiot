﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class Token
    {
        public string TokenId { get; set; }
        public DateTime CreateDateUTC { get; set; }
        public DateTime ExpireDateUTC { get; set; }
        public bool HasUse { get; set; }
    }
}
