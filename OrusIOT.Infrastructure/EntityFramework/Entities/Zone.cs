﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class Zone
    {
        public string ZoneId { get; set; }
        public string Name { get; set; }
        public string AreaId { get; set; }
        public bool IsActive { get; set; }

        public Area AreaNavigation { get; set; }

    }
}
