﻿using System.Collections.Generic;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class Company
    {
        public Company()
        {
            User = new HashSet<User>();
            Department = new HashSet<Department>();
            Custody = new HashSet<Custody>();
            Locale = new HashSet<Locale>();
            Area = new HashSet<Area>();
            MachineUser = new HashSet<MachineUser>();
            ReadingDevice = new HashSet<ReadingDevice>();
            Item = new HashSet<Item>();
            FloorMap = new HashSet<FloorMap>();
        }

        public string CompanyId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public string Logo { get; set; }
        public bool IsActive { get; set; }

        public ICollection<User> User { get; set; }
        public ICollection<Department> Department { get; set; }
        public ICollection<Custody> Custody { get; set; }
        public ICollection<Locale> Locale { get; set; }
        public ICollection<Area> Area { get; set; }
        public ICollection<MachineUser> MachineUser { get; set; }
        public ICollection<ReadingDevice> ReadingDevice { get; set; }
        public ICollection<Item> Item { get; set; }
        public ICollection<FloorMap> FloorMap { get; set; }
    }
}
