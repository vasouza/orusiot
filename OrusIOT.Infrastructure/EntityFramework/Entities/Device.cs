﻿using System;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class Device
    {
        public string DeviceId { get; set; }
        public string MacAddress { get; set; }
        public string Name { get; set; }
        public string CompanyId { get; set; }
    }
}
