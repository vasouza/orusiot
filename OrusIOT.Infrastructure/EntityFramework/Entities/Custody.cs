﻿using System.Collections.Generic;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class Custody
    {
        public Custody()
        {
            Item = new HashSet<Item>();
        }

        public string CustodyId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string CompanyId { get; set; }
        public Company CompanyNavigation { get; set; }

        public ICollection<Item> Item { get; set; }

    }
}
