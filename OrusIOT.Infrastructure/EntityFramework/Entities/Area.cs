﻿using System.Collections.Generic;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class Area
    {
        public Area()
        {
            Zone = new HashSet<Zone>();
        }

        public string AreaId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Floor { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public bool IsActive { get; set; }
        public string LocaleId { get; set; }
        public string CompanyId { get; set; }

        public Company CompanyNavigation { get; set; }
        public Locale LocaleNavigation { get; set; }

        public ICollection<Zone> Zone { get; set; }
        public ICollection<FloorMap> FloorMap { get; set; }
    }
}
