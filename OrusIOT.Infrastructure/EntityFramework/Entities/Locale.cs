﻿using OrusIOT.Infrastructure.EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class Locale
    {
        public Locale()
        {
            Area = new HashSet<Area>();
            Item = new HashSet<Item>();
        }

        public string LocaleId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string CompanyId { get; set; }

        public Company CompanyNavigation { get; set; }
        public ICollection<Area> Area { get; set; }
        public ICollection<Item> Item { get; set; }
    }
}
