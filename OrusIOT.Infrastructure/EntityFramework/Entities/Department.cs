﻿using System.Collections.Generic;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class Department
    {
        public Department()
        {
            User = new HashSet<User>();
        }

        public string DepartmentId { get; set; }
        public string Name { get; set; }
        public string CompanyId { get; set; }

        public Company CompanyNavigation { get; set; }

        public ICollection<User> User { get; set; }
    }
}
