﻿namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class FloorMap
    {
        public string FloorMapId { get; set; }
        public string Name { get; set; }
        public string FloorMapZones { get; set; }
        public string AreaId { get; set; }
        public string Image { get; set; }
        public string CompanyId { get; set; }


        public Area AreaNavigation { get; set; }
        public Company CompanyNavigation { get; set; }

    }
}
