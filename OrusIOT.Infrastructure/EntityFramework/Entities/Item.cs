﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrusIOT.Infrastructure.EntityFramework.Entities
{
    public class Item
    {
        public string ItemId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string SerialNumber { get; set; }
        public bool IsActive { get; set; }
        public string CustodyId { get; set; }
        public string CompanyId { get; set; }
        public string LocaleId { get; set; }

        public Company CompanyNavigation { get; set; }
        public Locale LocaleNavigation { get; set; }
        public Custody CustodyNavigation { get; set; }
    }
}
