﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OrusIOT.Infrastructure.EntityFramework.Entities;

namespace OrusIOT.Infrastructure
{
    public partial class DatabaseContext : DbContext
    {
        public IConfiguration Configuration { get; }

        public DatabaseContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Device> Device { get; set; }
        public virtual DbSet<Custody> Custody { get; set; }
        public virtual DbSet<Locale> Locale { get; set; }
        public virtual DbSet<Area> Area { get; set; }
        public virtual DbSet<Zone> Zone { get; set; }
        public virtual DbSet<MachineUser> MachineUser { get; set; }
        public virtual DbSet<ReadingDevice> ReadingDevice { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<FloorMap> FloorMap { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("Default"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("USER");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnName("UserId");

                entity.Property(e => e.Login)
                   .IsRequired()
                   .HasColumnName("Login")
                   .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("Name")
                    .IsUnicode(false);

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("Address")
                    .IsUnicode(false);


                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("Email")
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                   .IsRequired()
                   .HasColumnName("Phone")
                   .IsUnicode(false);

                entity.Property(e => e.IsActive).HasColumnName("IsActive");

                entity.Property(e => e.Password).HasColumnName("Password");

                entity.HasOne(d => d.DepartmentNavigation)
                   .WithMany(p => p.User)
                   .HasForeignKey(d => d.DepartmentId)
                   .HasConstraintName("FK_User_Department");

                entity.HasOne(d => d.CompanyNavigation)
                   .WithMany(p => p.User)
                   .HasForeignKey(d => d.CompanyId)
                   .HasConstraintName("FK_User_Company");
            });

            modelBuilder.Entity<Custody>(entity =>
            {
                entity.ToTable("CUSTODY");

                entity.Property(e => e.CustodyId)
                  .IsRequired()
                  .HasColumnName("CustodyId");

                entity.Property(e => e.Phone)
                     .IsRequired()
                     .HasColumnName("Phone")
                     .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("Name")
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("Email")
                    .IsUnicode(false);

                entity.Property(e => e.Address)
                 .IsRequired()
                 .HasColumnName("Address")
                 .IsUnicode(false);

                entity.HasOne(d => d.CompanyNavigation)
                      .WithMany(p => p.Custody)
                      .HasForeignKey(d => d.CompanyId)
                      .HasConstraintName("FK_Custody_Company");

            });

            modelBuilder.Entity<Locale>(entity =>
            {
                entity.ToTable("LOCALE");

                entity.Property(e => e.LocaleId)
                   .IsRequired()
                   .HasColumnName("LocaleId");

                entity.Property(e => e.Name)
                   .IsRequired()
                   .HasColumnName("Name")
                   .IsUnicode(false);

                entity.Property(e => e.IsActive)
                   .HasColumnName("IsActive")
                   .IsUnicode(false);

                entity.HasOne(d => d.CompanyNavigation)
                    .WithMany(p => p.Locale)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_Locale_Company");
            });

            modelBuilder.Entity<Area>(entity =>
            {
                entity.ToTable("AREA");


                entity.Property(e => e.AreaId)
                   .IsRequired()
                   .HasColumnName("AreaId");


                entity.Property(e => e.Name)
                   .IsRequired()
                   .HasColumnName("Name");


                entity.Property(e => e.Address)
                   .IsRequired()
                   .HasColumnName("Address");


                entity.Property(e => e.Floor)
                   .IsRequired()
                   .HasColumnName("Floor");

                entity.Property(e => e.City)
                   .IsRequired()
                   .HasColumnName("City");

                entity.Property(e => e.Country)
                 .IsRequired()
                 .HasColumnName("Country");

                entity.Property(e => e.ZipCode)
                 .IsRequired()
                 .HasColumnName("ZipCode");

                entity.Property(e => e.IsActive)
                  .HasColumnName("IsActive")
                  .IsUnicode(false);

                entity.HasOne(d => d.CompanyNavigation)
                 .WithMany(p => p.Area)
                 .HasForeignKey(d => d.CompanyId)
                 .HasConstraintName("FK_Area_Company");

                entity.HasOne(d => d.LocaleNavigation)
                 .WithMany(p => p.Area)
                 .HasForeignKey(d => d.LocaleId)
                 .HasConstraintName("FK_Area_Locale");
            });

            modelBuilder.Entity<Zone>(entity =>
            {
                entity.ToTable("ZONE");

                entity.Property(e => e.ZoneId)
                   .IsRequired()
                   .HasColumnName("ZoneId");

                entity.Property(e => e.Name)
                   .IsRequired()
                   .HasColumnName("Name")
                   .IsUnicode(false);

                entity.Property(e => e.IsActive)
                   .HasColumnName("IsActive")
                   .IsUnicode(false);

                entity.HasOne(d => d.AreaNavigation)
                    .WithMany(p => p.Zone)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK_Zone_Area");
            });

            modelBuilder.Entity<MachineUser>(entity =>
            {
                entity.ToTable("MachineUser");

                entity.Property(e => e.MachineUserId)
                   .IsRequired()
                   .HasColumnName("MachineUserId");

                entity.Property(e => e.Name)
                   .IsRequired()
                   .HasColumnName("Name")
                   .IsUnicode(false);

                entity.Property(e => e.Login)
                   .IsRequired()
                   .HasColumnName("Login")
                   .IsUnicode(false);

                entity.HasOne(d => d.CompanyNavigation)
                    .WithMany(p => p.MachineUser)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_MachineUser_Company");
            });

            modelBuilder.Entity<ReadingDevice>(entity =>
            {
                entity.ToTable("ReadingDevice");

                entity.Property(e => e.ReadingDeviceId)
                   .IsRequired()
                   .HasColumnName("ReadingDeviceId");

                entity.Property(e => e.Key)
                  .IsRequired()
                  .HasColumnName("ReadingDeviceKey");

                entity.Property(e => e.Name)
                   .IsRequired()
                   .HasColumnName("Name")
                   .IsUnicode(false);

                entity.Property(e => e.Manufacturer)
                   .HasColumnName("Manufacturer")
                   .IsUnicode(false);

                entity.Property(e => e.Model)
                   .HasColumnName("Model")
                   .IsUnicode(false);

                entity.Property(e => e.SerialNumber)
                   .HasColumnName("SerialNumber")
                   .IsUnicode(false);

                entity.Property(e => e.IsActive)
                   .HasColumnName("IsActive")
                   .IsUnicode(false);

                entity.HasOne(d => d.MachineUserNavigation)
                    .WithMany(p => p.ReadingDevice)
                    .HasForeignKey(d => d.AssociatedUserId)
                    .HasConstraintName("FK_ReadingDevice_MachineUser");

                entity.HasOne(d => d.MachineUserNavigation)
                    .WithMany(p => p.ReadingDevice)
                    .HasForeignKey(d => d.SharedUserId)
                    .HasConstraintName("FK_ReadingDevice_MachineUser");

                entity.Property(e => e.MacAddress)
                   .HasColumnName("MacAddress")
                   .IsUnicode(false);

                entity.Property(e => e.Latitude)
                   .HasColumnName("Latitude")
                   .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .HasColumnName("Longitude")
                    .IsUnicode(false);

                entity.HasOne(d => d.CompanyNavigation)
                   .WithMany(p => p.ReadingDevice)
                   .HasForeignKey(d => d.CompanyId)
                   .HasConstraintName("FK_ReadingDevice_Company");
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.ToTable("Item");

                entity.Property(e => e.ItemId)
                   .IsRequired()
                   .HasColumnName("ItemId");

                entity.Property(e => e.Name)
                  .IsRequired()
                  .HasColumnName("Name")
                  .IsUnicode(false);

                entity.Property(e => e.Code)
                  .IsRequired()
                  .HasColumnName("Code")
                  .IsUnicode(false);

                entity.Property(e => e.SerialNumber)
                  .IsRequired()
                  .HasColumnName("SerialNumber")
                  .IsUnicode(false);

                entity.Property(e => e.IsActive)
                   .HasColumnName("IsActive")
                   .IsUnicode(false);

                entity.HasOne(d => d.CompanyNavigation)
                   .WithMany(p => p.Item)
                   .HasForeignKey(d => d.CompanyId)
                   .HasConstraintName("FK_Item_Company");

                entity.HasOne(d => d.CustodyNavigation)
                   .WithMany(p => p.Item)
                   .HasForeignKey(d => d.CustodyId)
                   .HasConstraintName("FK_Item_Custody");

                entity.HasOne(d => d.LocaleNavigation)
                   .WithMany(p => p.Item)
                   .HasForeignKey(d => d.LocaleId)
                   .HasConstraintName("FK_Item_Local");
            });

            modelBuilder.Entity<FloorMap>(entity =>
            {
                entity.ToTable("FLOORMAP");

                entity.Property(e => e.FloorMapId)
                .IsRequired()
                .HasColumnName("FloorMapId");

                entity.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("Name");

                entity.Property(e => e.Image)
                    .HasColumnName("Image");

                entity.Property(e => e.FloorMapZones)
                .IsRequired()
                .HasColumnName("FloorMapZones");

                entity.HasOne(d => d.AreaNavigation)
                 .WithMany(p => p.FloorMap)
                 .HasForeignKey(d => d.AreaId)
                 .HasConstraintName("FK_FloorMap_Area");

                entity.HasOne(d => d.CompanyNavigation)
                 .WithMany(p => p.FloorMap)
                 .HasForeignKey(d => d.CompanyId)
                 .HasConstraintName("FK_FloorMap_Company");
            });
        }
    }
}
